<?php

namespace App\Repositories\Perusahaan;

use App\Models\Perusahaan;
use App\Models\PerusahaanJenis;

class PerusahaanRepository
{
    protected $model;

    public function __construct(Perusahaan $model)
    {
        $this->model                = $model;
    }

    public function store($data)
    {
        $model              = auth()->user()->perusahaan;

        if (empty($model)) {
            $data["user_id"]    = auth()->user()->id;
            $model              = $this->model->create($data);   
        }else {
            $model->update($data);
        }

        if ($model->perusahaanJenis->isNotEmpty()) {
            $model->perusahaanJenis()->delete();
        }

        foreach ($data["jenis"] as $key => $jenis) {
            $model->perusahaanJenis()->create([
                "jenis"         => $jenis,
            ]);
        }
    }

    public function upload($data, $perusahaanId)
    {
        $model              = $this->model->findOrFail($perusahaanId);
        $jenisFile          = str_replace("_file", "", $data["field"]);
        $fileName           = saveFile($data["file"], $jenisFile);

        $model->update([
            $data["field"]  => $fileName,
        ]);

        $items      = [
            "fileUrl"           => $model->{$data["field"] . "_storage"},
            "fileName"          => $data["field"] == "nib_file" ? "NIB" : "IUP",
            "removeFileUrl"     => route("user-perusahaan.perusahaan.remove-file", [$model->id, request()->field]),
        ];

        return $items;
    }

    public function removeFile($data, $perusahaanId, $field)
    {
        $model              = $this->model->findOrFail($perusahaanId);
        $jenisFile          = str_replace("_file", "", $field);
        deleteFile($model->{$field}, $jenisFile);

        $model->update([
            $field  => null,
        ]);
    }

    /**
     * getIndexItems
     * Untuk melempar data yang akan ditampilkan ke halaman index
     * @return array
     */
    public function getIndexItems(): array
    {
        return [
            "list_jenis"                => PerusahaanJenis::listJenis(),
            "list_files"                => [
                [
                    "name"                  => "nib_file",
                    "label"                 => "NIB",
                ],
                [
                    "name"                  => "iup_file",
                    "label"                 => "IUP",
                ]
            ],
        ];
    }
    // 
}