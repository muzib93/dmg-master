<?php

namespace App\Repositories;

class AjaxRepository
{
    protected $stateRepository;

    public function __construct()
    {

    }

    public function markAllAsRead(): array
    {
        auth()->user()->unreadNotifications->markAsRead();

        return makeResponse(true, "success");
    }
}