<?php

namespace App\Repositories\Dashboard;

use App\Models\Jetty;
use App\Models\Tugboat;
use App\Models\Tongkang;

class DashboardRepository
{
    public function __construct()
    {
    }

    public function getIndexItems()
    {
        $tugboat            = Tugboat::filter(request())->count();
        $tongkang           = Tongkang::filter(request())->count();
        $jetty              = Jetty::filter(request())->count();

        return [
            "counters"          => [
                "tugboat"               => $tugboat,
                "tongkang"              => $tongkang,
                "jetty"                 => $jetty,
            ],
        ];
    }
}