<?php

namespace App\Repositories\Dashboard;

use App\Models\UserLog;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Yajra\DataTables\EloquentDataTable as DataTables;

class UserLogRepository
{
    protected $model;

    public function __construct(UserLog $model)
    {
        $this->model                = $model;
    }

    /**
     * getInstanceModel
     * Untuk melakukan query model
     * @return Builder
     */
    public function getInstanceModel(array $relations = []): Builder
    {   
        if (empty($relations)) {
            $relations          = ["user"];
        }

        return $this->model->with($relations)->orderBy('id','DESC')->filterDatatable(request());
    }
    
    /**
     * getDatatable
     * Untuk memanipulasi data yang ingin ditampilkan ke dalam datatable
     * @param  mixed $dataTable
     * @param  mixed $routes
     * @param  mixed $optionPath
     * @return DataTables
     */
    public function getDatatable(DataTables $dataTable, Collection $routes, String $optionPath): DataTables
    {
        return $dataTable->addColumn('options',  function ($model) use ($routes, $optionPath)
                         {
                            return view($optionPath, compact("model", "routes"));
                         })
                         ->addColumn("user_name", function ($model)
                         {
                            return ucwords($model->user->name);
                         })
                         ->addColumn("tag_formatted", function ($model)
                         {
                            return $model->tag_formatted;
                         })
                         ->addColumn("created_at_formatted", function ($model)
                         {
                            return $model->created_at_formatted;
                         })
                         ->addColumn("file_formatted", function ($model)
                         {
                            return !empty($model->file_url) ? "<a target='_blank' href='". $model->file_url ."' class='btn btn-primary btn-xs'><i class='fa fa-file'></a>" : "";
                         })
                         ->rawColumns(["log","file_formatted","tag_formatted"])
                         ->filter(function ($model)  {
                            
                         }, true);
    }

    /**
     * storeItem
     * Untuk melakukan penambahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function storeItem(array $data)
    {
        $model          = $this->model->fill($data);
        $model->save();

        return $model;
    }
    
    /**
     * findItem
     * Untuk melakukan pencarian satu data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model
     */
    public function findItem(String $id, array $relations = []): Model
    {
        if (empty($relations)) {
            $relations          = [];
        }
        
        return $this->model->with($relations)->findOrFail($id);
    }
    
    /**
     * updateItem
     * Untuk melakukan perubahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function updateItem(String $id, array $data)
    {
        $model          = $this->findItem($id);
        $model->fill($data);
        $model->save();

        return $model;
    }
    
    /**
     * deleteItem
     * Untuk melakukan hapus data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model | @return RedirectResponse
     */
    public function deleteItem(String $id)
    {
        $model          = $this->findItem($id);
        
        $model->delete();

        return $model;
    }

    /**
     * getIndexItems
     * Untuk melempar data yang akan ditampilkan ke halaman index
     * @return array
     */
    public function getIndexItems(): array
    {
        return [];
    }
    
    /**
     * getFormItems
     * Untuk melempar data yang akan ditampilkan ke halaman form
     * @param String $function
     * @param Model|null $item
     * @return array
     */
    public function getFormItems(String $function, $item = null): array
    {
        return [];
    }
    // 

    // request & redirect    
    /**
     * customRequest
     * Untuk merubah data request
     * @param  mixed $request
     * @return array
     */
    public function customRequest($request): array
    {
        $data           = $request->all();

        return $data;
    }
    
    /**
     * customRedirect
     * Untuk memanipulasi redirect setelah data tersimpan
     * @param  array $attributes
     * @param  String $attributes["function"] = store|update|destroy
     * @param  String $attributes["route"] = default route to index
     * @param  String $attributes["message"] = default success message
     * @param  Model $response = response after actions
     * @return array
     */
    public function customRedirect(array $attributes, Model $response): array
    {
        return $attributes;
    }
}