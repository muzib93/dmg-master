<?php

namespace App\Repositories\Dashboard;

use App\Models\Role;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Eloquent\Builder;

class RoleRepository
{
    protected $model;
    protected $permissionRepository;

    public function __construct(Role $model, PermissionRepository $permissionRepository)
    {
        $this->model                                = $model;
        $this->permissionRepository                 = $permissionRepository;
    }

    /**
     * getInstanceModel
     * Untuk melakukan query model
     * @return Builder
     */
    public function getInstanceModel(): Builder
    {
        return $this->model->with([])->orderBy('id','DESC');
    }

    /**
     * storeItem
     * Untuk melakukan penambahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function storeItem(array $data)
    {
        $data["permission_id"]  = $this->extractPermissions($data["permission_id"]);
        $model                  = $this->model->fill($data);
        $model->save();

        $model->givePermissionTo($data["permission_id"]);

        return $model;
    }
    
    public function extractPermissions($permissionIds)
    {
        $decodes            = json_decode($permissionIds, true);
        $merged             = array_merge(...$decodes);
        $ids                = array_column($merged, "value");
        return $ids;
    }
    
    /**
     * findItem
     * Untuk melakukan pencarian satu data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model
     */
    public function findItem(String $id): Model
    {
        return $this->model->with(["permissions"])->findOrFail($id);
    }
    
    /**
     * updateItem
     * Untuk melakukan perubahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function updateItem(String $id, array $data)
    {
        $model          = $this->findItem($id);
        $model->fill($data);
        $model->save();

        $data["permission_id"]  = $this->extractPermissions($data["permission_id"]);

        $model->syncPermissions($data["permission_id"]);

        return $model;
    }
    
    /**
     * deleteItem
     * Untuk melakukan hapus data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model | @return RedirectResponse
     */
    public function deleteItem(String $id)
    {
        $model          = $this->findItem($id);
        
        $model->permissions()->detach();

        $model->delete();

        return $model;
    }

    /**
     * getIndexItems
     * Untuk melempar data yang akan ditampilkan ke halaman index
     * @return array
     */
    public function getIndexItems(): array
    {
        return [
            'items'                 => $this->getInstanceModel()->get(),
        ];
    }
    
    /**
     * getFormItems
     * Untuk melempar data yang akan ditampilkan ke halaman form
     * @param String $function
     * @param Model|null $item
     * @return array
     */
    public function getFormItems(String $function, $item = null): array
    {   
        $selected       = [];

        if (!empty($item)) {
            $selected   = $item->permissions->pluck("id")->toArray();
        }

        $items  = [
            "list_roles"            => $this->generateTreeviewData(Permission::get()->groupBy("parent_type")->toArray(), $selected),
        ];

        return $items;
    }
    // 

    // request & redirect    
    /**
     * customRequest
     * Untuk merubah data request
     * @param  mixed $request
     * @return array
     */
    public function customRequest($request): array
    {
        $data           = $request->all();

        return $data;
    }
    
    /**
     * customRedirect
     * Untuk memanipulasi redirect setelah data tersimpan
     * @param  array $attributes
     * @param  String $attributes["function"] = store|update|destroy
     * @param  String $attributes["route"] = default route to index
     * @param  String $attributes["message"] = default success message
     * @param  Model $response = response after actions
     * @return array
     */
    public function customRedirect(array $attributes, Model $response): array
    {
        return $attributes;
    }

    public function listRole(): Collection
    {
        return $this->model->pluck("name","id");
    }

    public function editForm(String $viewPath, array $data): array
    {
        $role                       = $this->findItem($data["role_id"]);
        $items                      = $this->getIndexItems();

        $view                       = view($viewPath . "form_edit", ["item" => $role, "items" => $items])->render();

        return makeResponse(true, 'success', ["view" => $view]);
    }

    public function generateTreeviewData($array, $selectedItems = [])
    {
        $jstreeData     = [];
        $parentType     = $this->parentType();
        $selected       = empty($selectedItems) ? true : false;

        foreach ($array as $key => $childrens) {
            $node = [
                'id'        => $key,
                'text'      => $parentType[$key],
                'state'     => ['opened' => true],
                'type'      => $key,
                'icon'      => 'fa fa-key text-success',
                'children'  => [],
            ];

            foreach ($childrens as $child) {

                if (!empty($selectedItems)) {
                    $selected           = in_array($child["id"], $selectedItems) ? true : false;
                }


                $childNode = [
                    'id'        => $key . '_' . $child['name'],
                    'value'     => $child['id'],
                    'text'      => $child['name'],
                    'type'      => $key,
                    'icon'      => 'fa fa-lock text-secondar',
                    'state'     => [
                        "selected"          => $selected
                    ],
                ];
                $node['children'][] = $childNode;
            }

            $jstreeData[] = $node;
        }

        return $jstreeData;
    }

    public function parentType()
    {
        $parentType             = [
            0               => "Auth",
        ];

        return $parentType;
    }
}