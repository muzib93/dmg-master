<?php

namespace App\Repositories\Dashboard;

use App\Models\Jetty;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Yajra\DataTables\EloquentDataTable as DataTables;

class JettyRepository
{
    protected $model;

    public function __construct(Jetty $model)
    {
        $this->model                = $model;
    }

    /**
     * getInstanceModel
     * Untuk melakukan query model
     * @return Builder
     */
    public function getInstanceModel(array $relations = []): Builder
    {   
        if (empty($relations)) {
            $relations          = ["perusahaan"];
        }

        return $this->model->with($relations)->filter(request())->orderBy('id','DESC');
    }
    
    /**
     * getDatatable
     * Untuk memanipulasi data yang ingin ditampilkan ke dalam datatable
     * @param  mixed $dataTable
     * @param  mixed $routes
     * @param  mixed $optionPath
     * @return DataTables
     */
    public function getDatatable(DataTables $dataTable, Collection $routes, String $optionPath): DataTables
    {
        return $dataTable->addColumn('options',  function ($model) use ($routes, $optionPath)
                         {
                            return view($optionPath, compact("model", "routes"));
                         })
                         ->addColumn("nama_perusahaan", function ($model)
                         {
                            return $model->perusahaan->nama_perusahaan ?? "-";
                         })
                         ->filter(function ($model)  {
                            
                         }, true);
    }

    /**
     * storeItem
     * Untuk melakukan penambahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function storeItem(array $data)
    {
        if (auth()->user()->roles->first()->name == "perusahaan") {
            $data["perusahaan_id"]  = auth()->user()->perusahaan->id;
        }

        $model          = $this->model->fill($data);
        $model->save();

        return $model;
    }
    
    /**
     * findItem
     * Untuk melakukan pencarian satu data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model
     */
    public function findItem(String $id, array $relations = []): Model
    {
        if (empty($relations)) {
            $relations          = ["perusahaan"];
        }
        
        return $this->model->with($relations)->findOrFail($id);
    }
    
    /**
     * updateItem
     * Untuk melakukan perubahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function updateItem(String $id, array $data)
    {
        if (auth()->user()->roles->first()->name == "perusahaan") {
            $data["perusahaan_id"]  = auth()->user()->perusahaan->id;
        }

        $model          = $this->findItem($id);
        $model->fill($data);
        $model->save();

        return $model;
    }
    
    /**
     * deleteItem
     * Untuk melakukan hapus data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model | @return RedirectResponse
     */
    public function deleteItem(String $id)
    {
        $model          = $this->findItem($id);
        
        $listFiles      = $this->model::listFiles();

        foreach ($listFiles as $key => $listFile) {
            if (!empty($model->{$listFile["name"]})) {
                deleteFile($model->{$listFile["name"]}, "jetty_" . $listFile["name"]);
            }
        }

        $model->delete();

        return $model;
    }

    /**
     * getIndexItems
     * Untuk melempar data yang akan ditampilkan ke halaman index
     * @return array
     */
    public function getIndexItems(): array
    {
        return [];
    }
    
    /**
     * getFormItems
     * Untuk melempar data yang akan ditampilkan ke halaman form
     * @param String $function
     * @param Model|null $item
     * @return array
     */
    public function getFormItems(String $function, $item = null): array
    {
        return [
            "list_files"        => $this->model::listFiles(),
        ];
    }
    // 

    // request & redirect    
    /**
     * customRequest
     * Untuk merubah data request
     * @param  mixed $request
     * @return array
     */
    public function customRequest($request): array
    {
        $data           = $request->all();

        return $data;
    }
    
    /**
     * customRedirect
     * Untuk memanipulasi redirect setelah data tersimpan
     * @param  array $attributes
     * @param  String $attributes["function"] = store|update|destroy
     * @param  String $attributes["route"] = default route to index
     * @param  String $attributes["message"] = default success message
     * @param  Model $response = response after actions
     * @return array
     */
    public function customRedirect(array $attributes, Model $response): array
    {
        return $attributes;
    }

    public function upload($data, $id)
    {
        $model              = $this->model->findOrFail($id);
        $jenisFile          = "jetty_" . $data["field"];
        $fileName           = saveFile($data["file"], $jenisFile);

        $model->update([
            $data["field"]  => $fileName,
        ]);

        $items      = [
            "fileUrl"           => $model->{$data["field"] . "_storage"},
            "fileName"          => collect($this->model::listFiles())->where("name", $data["field"])->first()["label"] ?? "",
            "removeFileUrl"     => route("jetty.remove-file", [$model->id, request()->field]),
        ];

        return $items;
    }

    public function removeFile($data, $id, $field)
    {
        $model              = $this->model->findOrFail($id);
        $jenisFile          = "jetty_" . $field;
        deleteFile($model->{$field}, $jenisFile);

        $model->update([
            $field  => null,
        ]);
    }
}