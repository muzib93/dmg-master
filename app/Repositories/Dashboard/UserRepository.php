<?php

namespace App\Repositories\Dashboard;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable as DataTables;

class UserRepository
{
    protected $model;
    protected $roleRepository;

    public function __construct(User $model, RoleRepository $roleRepository)
    {
        $this->model                        = $model;
        $this->roleRepository               = $roleRepository;
    }

    /**
     * getInstanceModel
     * Untuk melakukan query model
     * @return Builder
     */
    public function getInstanceModel(): Builder
    {
        return $this->model->with(["roles"])
                           ->whereHas("roles", function ($query)
                           {
                            $query->whereIn("name", ["admin"]);
                           })
                           ->orderDatatable(request());
    }
    
    /**
     * getDatatable
     * Untuk memanipulasi data yang ingin ditampilkan ke dalam datatable
     * @param  mixed $dataTable
     * @param  mixed $routes
     * @param  mixed $optionPath
     * @return DataTables
     */
    public function getDatatable(DataTables $dataTable, Collection $routes, String $optionPath): DataTables
    {
        return $dataTable->addColumn('options',  function ($model) use ($routes, $optionPath)
                         {
                            return view($optionPath, compact("model", "routes"));
                         })
                         ->addColumn('role_name', function ($model)
                         {
                            return $model->roles->first() ? ucwords($model->roles->first()->name) : '';
                         })
                         ->filterColumn('role_name', function ($query, $keyword)
                         {
                            $query->whereHas("roles", function ($query) use ($keyword)
                            {
                                $query->where("name","LIKE", "%" . $keyword . "%");
                            });
                         });
    }

    /**
     * storeItem
     * Untuk melakukan penambahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function storeItem(array $data)
    {
        $data["password"]   = Hash::make($data["password"]);
        $model              = $this->model->fill($data);
        $model->save();

        $model->assignRole($data["role_id"]);

        return $model;
    }
    
    /**
     * findItem
     * Untuk melakukan pencarian satu data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model
     */
    public function findItem(String $id): Model
    {
        return $this->model->with([])->findOrFail($id);
    }
    
    /**
     * updateItem
     * Untuk melakukan perubahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function updateItem(String $id, array $data)
    {
        if (array_key_exists("password", $data) && !empty($data["password"])) {
            $data["password"]       = Hash::make($data["password"]);
        }

        $model          = $this->findItem($id);
        $model->fill($data);
        $model->save();

        $model->syncRoles($data['role_id']);

        return $model;
    }
    
    /**
     * deleteItem
     * Untuk melakukan hapus data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model | @return RedirectResponse
     */
    public function deleteItem(String $id)
    {
        $model          = $this->findItem($id);
        
        $model->delete();

        return $model;
    }

    /**
     * getIndexItems
     * Untuk melempar data yang akan ditampilkan ke halaman index
     * @return array
     */
    public function getIndexItems(): array
    {
        return [];
    }
    
    /**
     * getFormItems
     * Untuk melempar data yang akan ditampilkan ke halaman form
     * @param String $function
     * @param Model|null $item
     * @return array
     */
    public function getFormItems(String $function, $item = null): array
    {
        return [
            'list_role'             => Role::whereIn("name", ["admin"])->pluck("name","id"),
        ];
    }
    // 

    // request & redirect    
    /**
     * customRequest
     * Untuk merubah data request
     * @param  mixed $request
     * @return array
     */
    public function customRequest($request): array
    {

        $data           = $request->all();

        if (empty($request->password) && $request->method() == "PUT") {
            unset($data["password"]);
        }

        return $data;
    }
    
    /**
     * customRedirect
     * Untuk memanipulasi redirect setelah data tersimpan
     * @param  array $attributes
     * @param  String $attributes["function"] = store|update|destroy
     * @param  String $attributes["route"] = default route to index
     * @param  String $attributes["message"] = default success message
     * @param  Model $response = response after actions
     * @return array
     */
    public function customRedirect(array $attributes, Model $response): array
    {
        return $attributes;
    }

    public function updateProfile(array $data): void
    {
        if (array_key_exists("password", $data) && !empty($data["password"])) {
            $data["password"]       = Hash::make($data["password"]);
        }else {
            unset($data["password"]);
        }

        auth()->user()->update($data);
    }
}