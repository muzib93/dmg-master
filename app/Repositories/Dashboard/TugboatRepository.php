<?php

namespace App\Repositories\Dashboard;

use App\Models\Nahkoda;
use App\Models\Tugboat;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable as DataTables;

class TugboatRepository
{
    protected $model;

    public function __construct(Tugboat $model)
    {
        $this->model                = $model;
    }

    /**
     * getInstanceModel
     * Untuk melakukan query model
     * @return Builder
     */
    public function getInstanceModel(array $relations = []): Builder
    {   
        if (empty($relations)) {
            $relations          = ["perusahaan"];
        }

        return $this->model->with($relations)->filter(request())->orderBy('id','DESC');
    }
    
    /**
     * getDatatable
     * Untuk memanipulasi data yang ingin ditampilkan ke dalam datatable
     * @param  mixed $dataTable
     * @param  mixed $routes
     * @param  mixed $optionPath
     * @return DataTables
     */
    public function getDatatable(DataTables $dataTable, Collection $routes, String $optionPath): DataTables
    {
        return $dataTable->addColumn('options',  function ($model) use ($routes, $optionPath)
                         {
                            return view($optionPath, compact("model", "routes"));
                         })
                         ->addColumn("nama_perusahaan", function ($model)
                         {
                            return $model->perusahaan->nama_perusahaan ?? "-";
                         })
                         ->addColumn("jenis_kapal_formatted", function ($model)
                         {
                            return $model->jenis_kapal_formatted ?? "-";
                         })
                         ->rawColumns(["jenis_kapal_formatted"])
                         ->filter(function ($model)  {
                            
                         }, true);
    }

    /**
     * storeItem
     * Untuk melakukan penambahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function storeItem(array $data)
    {
        if (auth()->user()->roles->first()->name == "perusahaan") {
            $data["perusahaan_id"]  = auth()->user()->perusahaan->id;
        }
        
        $model                  = $this->model->fill($data);
        $model->save();

        return $model;
    }
    
    /**
     * findItem
     * Untuk melakukan pencarian satu data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model
     */
    public function findItem(String $id, array $relations = []): Model
    {
        if (empty($relations)) {
            $relations          = ["perusahaan","nahkoda"];
        }
        
        return $this->model->with($relations)->findOrFail($id);
    }
    
    /**
     * updateItem
     * Untuk melakukan perubahan data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @param  mixed $data
     * @return Model | @return RedirectResponse
     */
    public function updateItem(String $id, array $data)
    {
        if (auth()->user()->roles->first()->name == "perusahaan") {
            $data["perusahaan_id"]  = auth()->user()->perusahaan->id;
        }
        
        $model          = $this->findItem($id);
        $model->fill($data);
        $model->save();

        return $model;
    }
    
    /**
     * deleteItem
     * Untuk melakukan hapus data | Melakukan validasi sebelum menyimpan data
     * @param  mixed $id
     * @return Model | @return RedirectResponse
     */
    public function deleteItem(String $id)
    {
        $model          = $this->findItem($id);
        
        $listFiles      = $this->model::listFiles();

        foreach ($listFiles as $key => $listFile) {
            if (!empty($model->{$listFile["name"]})) {
                deleteFile($model->{$listFile["name"]}, "tugboat_" . $listFile["name"]);
            }
        }

        deleteFile($model->nahkoda->skak_file, "nahkoda_skak_file");
        deleteFile($model->nahkoda->ijazah_laut_file, "nahkoda_ijazah_laut_file");
        $model->nahkoda()->delete();

        $model->delete();

        return $model;
    }

    /**
     * getIndexItems
     * Untuk melempar data yang akan ditampilkan ke halaman index
     * @return array
     */
    public function getIndexItems(): array
    {
        return [];
    }
    
    /**
     * getFormItems
     * Untuk melempar data yang akan ditampilkan ke halaman form
     * @param String $function
     * @param Model|null $item
     * @return array
     */
    public function getFormItems(String $function, $item = null): array
    {
        return [
            "list_jenis_kapal"              => $this->model::listJenisKapal(),
            "list_files"                    => $this->model::listFiles(),
        ];
    }
    // 

    // request & redirect    
    /**
     * customRequest
     * Untuk merubah data request
     * @param  mixed $request
     * @return array
     */
    public function customRequest($request): array
    {
        $data           = $request->all();

        return $data;
    }
    
    /**
     * customRedirect
     * Untuk memanipulasi redirect setelah data tersimpan
     * @param  array $attributes
     * @param  String $attributes["function"] = store|update|destroy
     * @param  String $attributes["route"] = default route to index
     * @param  String $attributes["message"] = default success message
     * @param  Model $response = response after actions
     * @return array
     */
    public function customRedirect(array $attributes, Model $response): array
    {
        return $attributes;
    }

    public function upload($data, $perusahaanId)
    {
        $model              = $this->model->findOrFail($perusahaanId);
        $jenisFile          = "tugboat_" . $data["field"];
        $fileName           = saveFile($data["file"], $jenisFile);

        $model->update([
            $data["field"]  => $fileName,
        ]);

        $items      = [
            "fileUrl"           => $model->{$data["field"] . "_storage"},
            "fileName"          => collect($this->model::listFiles())->where("name", $data["field"])->first()["label"] ?? "",
            "removeFileUrl"     => route("tugboat.remove-file", [$model->id, request()->field]),
        ];

        return $items;
    }

    public function removeFile($data, $perusahaanId, $field)
    {
        $model              = $this->model->findOrFail($perusahaanId);
        $jenisFile          = "tugboat_" . $field;
        deleteFile($model->{$field}, $jenisFile);

        $model->update([
            $field  => null,
        ]);
    }

    public function nahkodaStore($data, $tugboatId)
    {
        $model                      = $this->model->findOrFail($tugboatId);

        if (array_key_exists("skak_file", $data) && !empty($data["skak_file"])) {
            $data["skak_file"]              = saveFile($data["skak_file"], "nahkoda_skak_file");
        }

        if (array_key_exists("ijazah_laut_file", $data) && !empty($data["ijazah_laut_file"])) {
            $data["ijazah_laut_file"]      = saveFile($data["ijazah_laut_file"], "nahkoda_ijazah_laut_file");
        }

        $model->nahkoda()->create($data);
    }

    public function nahkodaDelete($nahkodaId)
    {
        $model                      = Nahkoda::findOrFail($nahkodaId);

        if (!empty($model->skak_file)) {
            deleteFile($model->skak_file, "nahkoda_skak_file");
        }

        if (!empty($model->ijazah_laut_file)) {
            deleteFile($model->ijazah_laut_file, "nahkoda_ijazah_laut_file");
        }

        $model->delete();
    }
}