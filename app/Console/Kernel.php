<?php

namespace App\Console;

use App\Console\Commands\BillingServiceJobCommand;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\CurrencyRateCommand;
use App\Console\Commands\MailGunServiceWorker;
use App\Console\Commands\StatusWatcherCommand;
use App\Console\Commands\SetScheduledMessageCommand;
use App\Console\Commands\SetScheduledCampaignCommand;
use App\Console\Commands\SetScheduledMailCampaignCommand;
use App\Console\Commands\SmppWatcherCommand;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
