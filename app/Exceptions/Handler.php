<?php

namespace App\Exceptions;

use Throwable;
use PDOException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        if ($request->header('Content-type') == "application/json" || $request->is("api/*")) {

            if (!empty($exception)) {
                
                $response           = makeResponse(false, "Internal error.", [], 500);
                
                if (config("app.debug")) {
                    $response["message"]             = $exception->getMessage();
                    $response["error"]               = $exception->getTrace();
                }

                if ($exception instanceof ValidationException) {
                    $response           = makeResponse(false, collect($exception->errors())->first()[0], [], 422);
                }else if ($exception instanceof AuthenticationException) {
                    $response           = makeResponse(false, "Username atau password salah.", [], 401);
                }else if ($exception instanceof NotFoundHttpException) {
                    $response           = makeResponse(false, "Not Found", [], 404);
                }else if ($exception instanceof PDOException) {
                    $response           = makeResponse(false, "Can not finish your query request!", [], 500);
                }else if($this->isHttpException($exception)) {
                    $response           = makeResponse(false, "Request Error", [], $exception->getStatusCode());
                }else {
                    $status             = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500;
                    $response["code"]   = $status;
                }

                return response()->json($response,  $response["code"]);
            }

        }

        return parent::render($request, $exception);
    }
}
