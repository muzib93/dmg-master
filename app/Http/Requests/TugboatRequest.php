<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TugboatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nama_pemilik"              => ["required"],
            "nib"                       => ["required"],
            "nama"                      => ["required"],
            "ukuran"                    => ["required"],
            "tonase_kotor"              => ["required"],
            "registrasi_pas"            => ["required"],
            "masa_berlaku_skksd"        => ["required"],
            "surat_ukur_berlaku"        => ["required"],
            "tanggal_pas"               => ["required"],
            "masa_berlaku_izin_operasi" => ["required"],
            "masa_berlaku_izin_trayek"  => ["required"],
            "jenis_kapal"               => ["required"],
        ];
    }
}
