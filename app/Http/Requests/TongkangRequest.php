<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TongkangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nama"                      => ["required"],
            "nama_pemilik"              => ["required"],
            "ukuran"                    => ["required"],
            "tonase_kotor"              => ["required"],
            "tanda_pendaftaran"         => ["required"],
            "tanggal_surat_laut"        => ["required"],
            "tanggal_ukur"              => ["required"],
            "masa_berlaku_konstruksi"   => ["required"],
            "masa_berlaku_lambung"      => ["required"],
            "masa_berlaku_garis_muat"   => ["required"],
        ];
    }
}
