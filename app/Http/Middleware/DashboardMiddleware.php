<?php

namespace App\Http\Middleware;

use Closure;
use Helpers\SidebarHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class DashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            $role               = $this->getRole();

            // if ($role->name != "admin") {
            //     $this->handlePermission($role, $request);
            // }

            $topBar             = new SidebarHelper("sidebar.json");
            $menuByRole         = $topBar->getMenuByRole($role);
            View::share("topBarMenu", $menuByRole);

            return $next($request);
        }
    }

    public function getRole()
    {
        return auth()->user()->roles->first();
    }

    public function handlePermission($role, $request)
    {
        $permissions            = $role->permissions->pluck('route_name','name')->toArray();
        $route_name             = $request->route()->getName();
        $allowed_role           = ['admin'];
        $checkIfSubmitRoute     = $this->checkIfSubmitRoute($route_name, $permissions);
        $allowedRoute           = $this->allowedRoute($route_name, $permissions);

// dd(!in_array($route_name, $permissions), (!in_array($role->name, $allowed_role)), $checkIfSubmitRoute, !$allowedRoute, !$request->ajax());

        if (!in_array($route_name, $permissions) && 
            (!in_array($role->name, $allowed_role)) && 
            $checkIfSubmitRoute && 
            !$allowedRoute &&
            !$request->ajax()
        ) {
            abort(401, 'Unauthorized');
        }
    }

    protected function checkIfSubmitRoute(String $routeName, $permissions): bool
    {
        $explode                = explode(".", $routeName);
        $status                 = true;
        $prefixRoute            = count($explode) > 2 ? $explode[0] . "." . $explode[1] : $explode[0];

        if (end($explode) == "update" && in_array(($prefixRoute . ".edit"), $permissions)) {
            $status             = false;
        }

        if (end($explode) == "store" && in_array(($prefixRoute . ".create"), $permissions)) {
            $status             = false;
        }

        return $status;
    }

    public function allowedRoute($routeName, $permissions)
    {
        $status                 = false;

        $allowedRoutes          = [];

        foreach ($allowedRoutes as $permissionParent => $allowedRoute) {
            if (array_key_exists($permissionParent, $permissions)) {
                if (in_array($routeName, $permissions)) {
                    $status             = true;
                }

                foreach ($allowedRoute as $key => $route) {
                    if (fnmatch($route, $routeName)) {
                        $status         = true;
                    }
                }
            }
        }

        return $status;
    }
}
