<?php

namespace App\Http\Controllers;

use App\Repositories\AjaxRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AjaxController extends Controller
{   
    protected $repository;

    public function __construct(AjaxRepository $repository)
    {
        $this->repository               = $repository;
    }

    public function markAllAsRead(Request $request): JsonResponse
    {
        $response                   = $this->repository->markAllAsRead();

        return response()->json($response, $response["code"]);
    }
}
