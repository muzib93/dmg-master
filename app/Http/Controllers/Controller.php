<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CRUDTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\Traits\CRUDHelperTrait;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, CRUDTrait, CRUDHelperTrait;

    public function __construct() {
    }
}
