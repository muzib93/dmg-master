<?php

namespace App\Http\Controllers\Traits;

use Illuminate\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;
use Yajra\DataTables\Facades\DataTables;

trait CRUDTrait
{
    public function index(): View
    {
        $view                   = [
            'title'                 => $this->title,
            'sub_title'             => $this->makeSubTitle(__FUNCTION__),
            'routes'                => $this->completeRoutes(),
            'items'                 => method_exists($this->repository, "getIndexItems") ? $this->repository->getIndexItems() : [],
        ];

        return view($this->makeViewPath(__FUNCTION__), $view);
    }

    public function data(): JsonResponse
    {
        $datatable                  = $this->repository->getDatatable(DataTables::of($this->repository->getInstanceModel())->addIndexColumn(), 
                                                                      $this->completeRoutes(['edit','destroy','show']), 
                                                                      $this->makeViewPath("button"));

	    return $datatable->toJson();
    }

    public function create()
    {
        $view                   = [
            'title'                 => $this->title,
            'sub_title'             => $this->makeSubTitle(__FUNCTION__),
            'routes'                => $this->completeRoutes(["store"]),
            'form'                  => $this->makeViewPath("form"),
            'items'                 => method_exists($this->repository, "getFormItems") ? $this->repository->getFormItems(__FUNCTION__) : []
        ];

        return view($this->makeViewPath(__FUNCTION__), $view);
    }

    public function store(): RedirectResponse
    {
        try {
			
			DB::beginTransaction();
			
			$data       = $this->getRequest();

            $response   = $this->repository->storeItem($data);

            if ($response instanceof RedirectResponse) {
                return $response;   
            }

			storeLog(
                'add', 
                $response->id, 
                makeTag($response, property_exists($this, 'tag') ? $this->tag : []), 
                $this->title
            );

			DB::commit();

            if (method_exists($this->repository, "callbackAfterDbCommit")) {
                $this->repository->callbackAfterDbCommit(__FUNCTION__, $response);
            }

			return $this->redirectSuccessToIndex(__FUNCTION__, $response);

		} catch (Exception $e) {
			
			DB::rollback();

            throw $e;
		}           
    }

    public function edit(String $id): View
    {
        $item               = $this->repository->findItem($id);

        $view               = [
            'title'                 => $this->title,
            'sub_title'             => $this->makeSubTitle(__FUNCTION__),
            'routes'                => $this->completeRoutes(["update"]),
            'form'                  => $this->makeViewPath("form"),
            'item'                  => $item,
            'items'                 => method_exists($this->repository, "getFormItems") ? $this->repository->getFormItems(__FUNCTION__, $item) : []
        ];

        return view($this->makeViewPath(__FUNCTION__), $view);
    }

    public function update(String $id): RedirectResponse
    {
        try {
			
			DB::beginTransaction();
			
			$data  = $this->getRequest();

            $response   = $this->repository->updateItem($id, $data);

            if ($response instanceof RedirectResponse) {
                return $response;   
            }

			storeLog(
                'edit', 
                $response->id, 
                makeTag($response, property_exists($this, 'tag') ? $this->tag : []), 
                $this->title
            );

			DB::commit();
			
            if (method_exists($this->repository, "callbackAfterDbCommit")) {
                $this->repository->callbackAfterDbCommit(__FUNCTION__, $response);
            }

			return $this->redirectSuccessToIndex(__FUNCTION__, $response);

		} catch (Exception $e) {
			
			DB::rollback();
			
            throw $e;
		}
    }

    public function show($id)
    {
        $item               = $this->repository->findItem($id);
        
        $view               = [
            'title'                 => $this->title,
            'sub_title'             => $this->makeSubTitle(__FUNCTION__),
            'form'                  => $this->makeViewPath("form"),
            'item'                  => $item,
            'items'                 => method_exists($this->repository, "getFormItems") ? $this->repository->getFormItems(__FUNCTION__, $item) : []
        ];

        return view($this->makeViewPath(__FUNCTION__), $view);        
    }

    public function destroy(String $id): RedirectResponse
    {
        try {
			
			DB::beginTransaction();

            $response   = $this->repository->deleteItem($id);

            if ($response instanceof RedirectResponse) {
                return $response;   
            }

            if (method_exists($this->repository, "callbackAfterDbCommit")) {
                $this->repository->callbackAfterDbCommit(__FUNCTION__, $response);
            }

			storeLog(
                'delete', 
                $response->id, 
                makeTag($response, property_exists($this, 'tag') ? $this->tag : []), 
                $this->title
            );

			DB::commit();
			
            return $this->redirectSuccessToIndex(__FUNCTION__, $response);

		} catch (Exception $e) {
			
			DB::rollback();
			
            throw $e;
		}
    }
}