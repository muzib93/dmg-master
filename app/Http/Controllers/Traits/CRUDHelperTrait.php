<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Support\Collection;
use Illuminate\Http\RedirectResponse;

trait CRUDHelperTrait
{
    public function makeViewPath(String $function): String
    {
        return $this->viewPath . '' . $this->generateRoute($function);
    }

    public function completeRoutes($only = []): Collection
    {
        $routes     = collect([
            'index'		=> $this->generateRoute('index'),
            'destroy'	=> $this->generateRoute('destroy'),
            'create'	=> $this->generateRoute('create'),
            'store'     => $this->generateRoute('store'),
            'edit'		=> $this->generateRoute('edit'),
            'update'    => $this->generateRoute('update'),
            'show'		=> $this->generateRoute('show'),
            'data'      => $this->generateRoute('data'),
        ]);

        return !empty($only) ? $routes->only($only) : $routes;
    }

    public function generateRoute(String $name): String
    {
        return $this->titleDashCase() . '.' . $name;
    }

    public function titleDashCase(): String
    {
        return strtolower(str_replace(' ', '-', $this->title));
    }

    public function redirectSuccessToIndex(String $function, $response): RedirectResponse
    {
        $route      = $this->generateRoute('index');
        $message    = $this->generateMessage($function);
        $params     = [];

        if (method_exists($this->repository, "customRedirect")) {
            $customRedirect         = $this->repository->customRedirect(["route" => $route, "message" => $message, "function" => $function], $response);
            $route                  = $customRedirect["route"];
            $message                = array_key_exists("message", $customRedirect) ? $customRedirect["message"] : [];
            $params                 = array_key_exists("params", $customRedirect) ? $customRedirect["params"] : [];
        }

        return redirect()->route($route, $params)
						 ->withSuccess($message);
    }

    public function generateMessage(String $function): String
    {
        $message            = 'Successfully';   

        switch ($function) {
            case 'store':
                $message    = $message . ' Added';
                break;
            case 'update':
                $message    = $message . ' Changed';
                break;
            case 'destroy':
                $message    = $message . ' Deleted';
                break;
        }

        return $message . ' Data';
    }

    public function makeSubTitle(String $function): String
    {
        switch ($function) {
            case 'index':
                return "List " . $this->title;
                break;
            case 'create':
                return "Add " . $this->title;
                break;
            case 'edit':
                return "Edit " . $this->title;
                break;
            case 'show':
                return "Detail " . $this->title;
                break;
        }
    }

    public function getRequest()
    {
		return method_exists($this->repository, 'customRequest') ? 
                $this->repository->customRequest(app($this->modelRequest)) : 
                app($this->modelRequest)->all();
    }
}