<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\TugboatRequest;
use App\Repositories\Dashboard\TugboatRepository;

class TugboatController extends Controller
{
    protected $repository;

    public function __construct(TugboatRepository $repository)
    {
        $this->title            = 'Tugboat';
        $this->viewPath         = 'dashboard.pages.';
        $this->repository       = $repository;
        $this->modelRequest     = TugboatRequest::class;
    }

    public function upload(Request $request, $id)
    {
        try {
            
            DB::beginTransaction();

            $items      = $this->repository->upload(request()->all(), $id);

            DB::commit();

            return response()->json(makeResponse(true, 'success', [
                "view"          => view("components.files.after_upload", $items)->render(),
                "parent_div"    => request()->field,
            ]));

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }

    public function removeFile(Request $request, $id, $jenisFile)
    {
        try {
            
            DB::beginTransaction();

            $this->repository->removeFile(request()->all(), $id, $jenisFile);

            DB::commit();

            return redirect()->back()->withSuccess("Berhasil menghapus file");

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }

    public function nahkodaStore(Request $request, $tugboatId)
    {
        try {
            
            DB::beginTransaction();

            $this->repository->nahkodaStore(request()->all(), $tugboatId);

            DB::commit();

            return redirect()->back()->withSuccess("Berhasil menambahkan data Nahkoda");

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }

    public function nahkodaDelete($nahkodaId)
    {
        try {
            
            DB::beginTransaction();

            $this->repository->nahkodaDelete($nahkodaId);

            DB::commit();

            return redirect()->back()->withSuccess("Berhasil menghapus data Nahkoda");

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }
}
