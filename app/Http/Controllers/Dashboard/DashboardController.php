<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use App\Repositories\Dashboard\DashboardRepository;

class DashboardController extends Controller
{
    protected $repository;

    public function __construct(DashboardRepository $repository)
    {
        $this->title            = 'Dashboard';
        $this->viewPath         = "dashboard";
        $this->repository       = $repository;
        $this->modelRequest     = [];
    }

    public function index(): View
    {
        $items          = $this->repository->getIndexItems();

        return view("dashboard.pages.dashboard.index")->with(["items" => $items]);
    }
}
