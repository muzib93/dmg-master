<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Dashboard\PermissionRepository;

class PermissionController extends Controller
{
    protected $repository;

    public function __construct(PermissionRepository $repository)
    {
        $this->title            = 'Permission';
        $this->viewPath         = 'dashboard.pages.';
        $this->repository       = $repository;
        $this->modelRequest     = [];
    }
}
