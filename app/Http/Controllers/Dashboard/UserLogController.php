<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\UserLog;
use App\Repositories\Dashboard\UserLogRepository;

class UserLogController extends Controller
{
    protected $repository;

    public function __construct(UserLogRepository $repository)
    {
        $this->title            = 'User Log';
        $this->viewPath         = 'dashboard.pages.';
        $this->repository       = $repository;
        $this->modelRequest     = null;
    }

    public function userLogUpload($userLogId)
    {
        try {
            
            DB::beginTransaction();

            $userLog    = UserLog::find($userLogId);

            if (!empty($userLog->file)) {
                deleteFile($userLog->file, "user_log");
            }

            $userLog->update([
                "file"          => saveFile(request()->file, "user_log"),
            ]);

            DB::commit();

            return redirect()->back()->withSuccess("Berhasil upload file");

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }
}
