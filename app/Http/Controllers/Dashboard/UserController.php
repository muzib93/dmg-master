<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\UpdateProfileRequest;
use App\Repositories\Dashboard\UserRepository;

class UserController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->title            = 'User';
        $this->viewPath         = 'dashboard.pages.';
        $this->repository       = $repository;
        $this->modelRequest     = UserRequest::class;
    }

    public function editProfile(): View
    {
        $view                   = [
            'title'                 => "Profile",
            'sub_title'             => "Edit Profile",
            'routes'                => [
                "store"             => "profile.update"
            ],
            'form'                  => $this->viewPath . "profile.form",
        ];

        return view($this->viewPath . "profile.edit")->with($view);
    }

    public function notifikasiIndex(): View
    {
        $view                   = [
            'title'                 => "Notifikasi",
            'sub_title'             => "List Notifikasi",
            'routes'                => [],
        ];

        return view($this->viewPath . "user.notifikasi.index")->with($view);
    }

    public function updateProfile(UpdateProfileRequest $request): RedirectResponse
    {
        try {
			
			DB::beginTransaction();
			
			$data       = $request->all();

            $this->repository->updateProfile($data);

			DB::commit();

			return redirect()->back()->withSuccess("Successfully Update Profile");

		} catch (Exception $e) {
			
			DB::rollback();

            throw $e;
		}   
    }
}
