<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\JettyRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Dashboard\JettyRepository;

class JettyController extends Controller
{
    protected $repository;

    public function __construct(JettyRepository $repository)
    {
        $this->title            = 'Jetty';
        $this->viewPath         = 'dashboard.pages.';
        $this->repository       = $repository;
        $this->modelRequest     = JettyRequest::class;
    }

    public function upload(Request $request, $id)
    {
        try {
            
            DB::beginTransaction();

            $items      = $this->repository->upload(request()->all(), $id);

            DB::commit();

            return response()->json(makeResponse(true, 'success', [
                "view"          => view("components.files.after_upload", $items)->render(),
                "parent_div"    => request()->field,
            ]));

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }

    public function removeFile(Request $request, $id, $jenisFile)
    {
        try {
            
            DB::beginTransaction();

            $this->repository->removeFile(request()->all(), $id, $jenisFile);

            DB::commit();

            return redirect()->back()->withSuccess("Berhasil menghapus file");

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }
}
