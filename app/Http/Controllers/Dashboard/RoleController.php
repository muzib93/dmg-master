<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\User;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Dashboard\RoleRepository;

class RoleController extends Controller
{
    protected $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->title            = 'Role';
        $this->viewPath         = 'dashboard.pages.';
        $this->repository       = $repository;
        $this->modelRequest     = RoleRequest::class;
    }

    public function addUserIndex(String $id): View
    {
        $item               = $this->repository->findItem($id);

        $view               = [
            'title'                 => $this->title,
            'sub_title'             => "Role - Add User",
            'routes'                => [
                "update"                => 'role.add-user.store'
            ],
            'item'                  => $item,
            'items'                 => []
        ];

        return view("dashboard.pages.role.add_user", $view);
    }

    public function addUserStore(Request $request, $roleId)
    {
        try {
        
            DB::beginTransaction();

            $data                   = $request->all();
            $userIds                = json_decode($data["user_id"]);
            $item                   = $this->repository->findItem($roleId);

            $item->users()->detach();

            foreach ($userIds as $key => $userId) {
                $user               = User::find($userId);

                $user->syncRoles($roleId);
            }
        
            DB::commit();

            return redirect()->route("role.index")->withSuccess("Berhasil menambah user role");

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }
}
