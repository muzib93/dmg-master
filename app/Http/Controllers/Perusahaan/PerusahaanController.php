<?php

namespace App\Http\Controllers\Perusahaan;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Repositories\Perusahaan\PerusahaanRepository;

class PerusahaanController extends Controller
{
    protected $repository;

    public function __construct(PerusahaanRepository $repository)
    {
        $this->title            = 'Perusahaan';
        $this->viewPath         = "perusahaan";
        $this->repository       = $repository;
        $this->modelRequest     = [];
    }

    public function index(): View
    {
        $view                   = [
            'title'                 => $this->title,
            'sub_title'             => "Data " . $this->title,
            'routes'                => [
                "store"                 => "user-perusahaan.perusahaan.store",
            ],
            'items'                 => $this->repository->getIndexItems(),
        ];

        return view("perusahaan.pages.perusahaan.index", $view);   
    }    

    public function store() : RedirectResponse
    {
        try {
            
            DB::beginTransaction();

            $this->repository->store(request()->all());

            DB::commit();

            return redirect()->back()->withSuccess("Berhasil merubah data");

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }

    public function upload(Request $request, $perusahaanId)
    {
        try {
            
            DB::beginTransaction();

            $items      = $this->repository->upload(request()->all(), $perusahaanId);

            DB::commit();

            return response()->json(makeResponse(true, 'success', [
                "view"          => view("components.files.after_upload", $items)->render(),
                "parent_div"    => request()->field,
            ]));

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }

    public function removeFile(Request $request, $perusahaanId, $jenisFile)
    {
        try {
            
            DB::beginTransaction();

            $this->repository->removeFile(request()->all(), $perusahaanId, $jenisFile);

            DB::commit();

            return redirect()->back()->withSuccess("Berhasil menghapus file");

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }
}
