<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Nahkoda extends Model
{
    use HasFactory, SoftDeletes;

    protected $table    = "nahkoda";
    protected $fillable = [
        "tugboat_id",
        "nama",
        "tanggal_skak",
        "tanggal_aktif",
        "skak_file",
        "ijazah_laut_file",
    ];

    // const
    // 

    // data
    // 

    // relation
    public function tugboat()
    {
        return $this->belongsTo(Tugboat::class, "tugboat_id");
    }
    // 

    // attributes
    public function getTanggalSkakFormattedAttribute()
    {
        return formatDate("Y-m-d","d-m-Y", $this->tanggal_skak);
    }

    public function getTanggalAktifFormattedAttribute()
    {
        return formatDate("Y-m-d","d-m-Y", $this->tanggal_aktif);
    }

    public function setTanggalSkakAttribute($val)
    {
        $this->attributes["tanggal_skak"] = formatDate("d-m-Y","Y-m-d", $val);
    }

    public function setTanggalAktifAttribute($val)
    {
        $this->attributes["tanggal_aktif"] = formatDate("d-m-Y","Y-m-d", $val);
    }

    public function getSkakFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->skak_file)) {
            $str   = Storage::url("nahkoda_skak_file/" . $this->skak_file);
        }

        return $str;
    }

    public function getIjazahLautFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->ijazah_laut_file)) {
            $str   = Storage::url("nahkoda_ijazah_laut_file/" . $this->ijazah_laut_file);
        }

        return $str;
    }
    // 

    // scopes
    // 
}
