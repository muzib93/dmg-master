<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Perusahaan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "perusahaan";
    protected $fillable = [
        "nama_perusahaan",
        "nama_pemilik",
        "nib",
        "iup",
        "nib_file",
        "iup_file",
        "user_id",
    ];

    // const
    // 

    // data
    // 

    // relations
    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function perusahaanJenis()
    {
        return $this->hasMany(PerusahaanJenis::class);
    }

    public function tugboat()
    {
        return $this->hasMany(Tugboat::class);
    }

    public function tongkang()
    {
        return $this->hasMany(Tongkang::class);
    }

    public function jetty()
    {
        return $this->hasMany(Jetty::class);
    }
    // 

    // attributes
    public function getNibFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->nib_file)) {
            $str   = Storage::url("nib/" . $this->nib_file);
        }

        return $str;
    }

    public function getIupFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->iup_file)) {
            $str   = Storage::url("iup/" . $this->iup_file);
        }

        return $str;
    }
    // 

    // scopes
    // 
}
