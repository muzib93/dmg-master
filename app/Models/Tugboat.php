<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tugboat extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "tugboat";
    protected $fillable = [
        "nama_pemilik",
        "nib",
        "nama",
        "ukuran",
        "tonase_kotor",
        "registrasi_pas",
        "masa_berlaku_skksd",
        "surat_ukur_berlaku",
        "tanggal_pas",
        "masa_berlaku_izin_operasi",
        "masa_berlaku_izin_trayek",
        "jenis_kapal",
        "skksd_file",
        "surat_ukur_file",
        "pas_file",
        "izin_operasi_file",
        "izin_trayek_file",
        "nib_file",
        "perusahaan_id",
    ];

    // const
    const JENIS_KAPAL_KAPAL_BARANG                  = 1;
    const JENIS_KAPAL_KAPAL_PENUMPANG               = 2;
    const JENIS_KAPAL_KAPAL_BARANG_PENUMPANG        = 3;
    // 

    // data
    public  static function listJenisKapal()
    {
        return [
            self::JENIS_KAPAL_KAPAL_BARANG             => "Kapal Barang",
            self::JENIS_KAPAL_KAPAL_PENUMPANG          => "Kapal Penumpang",
            self::JENIS_KAPAL_KAPAL_BARANG_PENUMPANG   => "Kapal Barang & Penumpang",
        ];
    }

    public  static function listFiles()
    {
        return [
            [
                "name"                  => "skksd_file",
                "label"                 => "SKKSD",
            ],
            [
                "name"                  => "surat_ukur_file",
                "label"                 => "Surat Ukur",
            ],
            [
                "name"                  => "pas_file",
                "label"                 => "PAS Sungai Danau & Endorse",
            ],
            [
                "name"                  => "izin_operasi_file",
                "label"                 => "Izin Operasi",
            ],
            [
                "name"                  => "izin_trayek_file",
                "label"                 => "Izin Trayek",
            ],
            [
                "name"                  => "nib_file",
                "label"                 => "NIB",
            ],
        ];
    }
    // 

    // relations
    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, "perusahaan_id");
    }

    public function nahkoda()
    {
        return $this->hasMany(Nahkoda::class);
    }
    // 

    // attributes
    public function getJenisKapalStrAttribute()
    {
        return self::listJenisKapal()[$this->jenis_kapal] ?? "-";
    }

    public function getJenisKapalFormattedAttribute()
    {
        return labelHelper($this->jenis_kapal_str, "badge bg-primary");
    }

    public function getMasaBerlakuSkksdFormattedAttribute()
    {
        return formatDate("Y-m-d", "d-m-Y", $this->masa_berlaku_skksd);
    }

    public function getSuratUkurBerlakuFormattedAttribute()
    {
        return formatDate("Y-m-d", "d-m-Y", $this->surat_ukur_berlaku);
    }

    public function getTanggalPasFormattedAttribute()
    {
        return formatDate("Y-m-d", "d-m-Y", $this->tanggal_pas);
    }

    public function getMasaBerlakuIzinOperasiFormattedAttribute()
    {
        return formatDate("Y-m-d", "d-m-Y", $this->masa_berlaku_izin_operasi);
    }

    public function getMasaBerlakuIzinTrayekFormattedAttribute()
    {
        return formatDate("Y-m-d", "d-m-Y", $this->masa_berlaku_izin_trayek);
    }

    public function setMasaBerlakuSkksdAttribute($val)
    {
        $this->attributes["masa_berlaku_skksd"]     = formatDate("d-m-Y", "Y-m-d", $val);
    }

    public function setSuratUkurBerlakuAttribute($val)
    {
        $this->attributes["surat_ukur_berlaku"]     = formatDate("d-m-Y", "Y-m-d", $val);
    }

    public function setTanggalPasAttribute($val)
    {
        $this->attributes["tanggal_pas"]     = formatDate("d-m-Y", "Y-m-d", $val);
    }

    public function setMasaBerlakuIzinOperasiAttribute($val)
    {
        $this->attributes["masa_berlaku_izin_operasi"]     = formatDate("d-m-Y", "Y-m-d", $val);
    }

    public function setMasaBerlakuIzinTrayekAttribute($val)
    {
        $this->attributes["masa_berlaku_izin_trayek"]     = formatDate("d-m-Y", "Y-m-d", $val);
    }

    public function getSkksdFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->skksd_file)) {
            $str   = Storage::url("tugboat_skksd_file/" . $this->skksd_file);
        }

        return $str;
    }

    public function getSuratUkurFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->surat_ukur_file)) {
            $str   = Storage::url("tugboat_surat_ukur_file/" . $this->surat_ukur_file);
        }

        return $str;
    }

    public function getPasFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->pas_file)) {
            $str   = Storage::url("tugboat_pas_file/" . $this->pas_file);
        }

        return $str;
    }

    public function getIzinOperasiFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->izin_operasi_file)) {
            $str   = Storage::url("tugboat_izin_operasi_file/" . $this->izin_operasi_file);
        }

        return $str;
    }

    public function getIzinTrayekFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->izin_trayek_file)) {
            $str   = Storage::url("tugboat_izin_trayek_file/" . $this->izin_trayek_file);
        }

        return $str;
    }

    public function getNibFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->nib_file)) {
            $str   = Storage::url("tugboat_nib_file/" . $this->nib_file);
        }

        return $str;
    }
    // 

    // scopes
    public function scopeFilter($query, $request)
    {
        if (auth()->user()->roles->first()->name == "perusahaan") {
            empty(auth()->user()->perusahaan) ? $query->whereNull("id") : $query->where("perusahaan_id", auth()->user()->perusahaan->id);
        }

        return $query;
    }
    // 
}
