<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerusahaanJenis extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "perusahaan_jenis";
    protected $fillable = [
        "jenis",
        "perusahaan_id",
    ];

    // const
    const JENIS_PEMILIK_TUGBOAT         = 1;
    const JENIS_PEMILIK_TONGKANG        = 2;
    const JENIS_PEMILIK_JETTY           = 3;
    const JENIS_SHIPPER                 = 4;
    const JENIS_AGENT_SIUPKK            = 5;
    // 

    // data
    public  static function listJenis()
    {
        return [
            self::JENIS_PEMILIK_TUGBOAT         => "Pemilik Tugboat",
            self::JENIS_PEMILIK_TONGKANG        => "Pemilik Tongkang",
            self::JENIS_PEMILIK_JETTY           => "Pemilik Jetty",
            self::JENIS_SHIPPER                 => "Shipper",
            self::JENIS_AGENT_SIUPKK            => "Agent/SIUPKK",
        ];
    }
    // 

    // relations
    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, "perusahaan_id");
    }
    // 

    // attributes
    public function getJenisStrAttribute()
    {
        return self::listJenis()[$this->jenis] ?? "-";
    }

    public function getJenisFormattedAttribute()
    {
        return labelHelper($this->jenis_str, "badge bg-primary");
    }
    // 

    // scopes
    // 
}
