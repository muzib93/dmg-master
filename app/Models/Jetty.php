<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Jetty extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "jetty";
    protected $fillable = [
        "nama",
        "lokasi",
        "titik_kordinat",
        "ijin_jetty",
        "ijin_jetty_file",
        "perusahaan_id",
    ];

    // const
    // 

    // data
    public  static function listFiles()
    {
        return [
            [
                "name"                  => "ijin_jetty_file",
                "label"                 => "Ijin Jetty",
            ]
        ];
    }
    // 

    // relations
    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, "perusahaan_id");
    }
    // 

    // attributes
    public function getIjinJettyFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->ijin_jetty_file)) {
            $str   = Storage::url("jetty_ijin_jetty_file/" . $this->ijin_jetty_file);
        }

        return $str;
    }
    // 

    // scopes
    public function scopeFilter($query, $request)
    {
        if (auth()->user()->roles->first()->name == "perusahaan") {
            empty(auth()->user()->perusahaan) ? $query->whereNull("id") : $query->where("perusahaan_id", auth()->user()->perusahaan->id);
        }

        return $query;
    }
    // 
}
