<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'fcm_token',
        "is_active",
        "status_logout",
    ];

    const IS_ACTIVE         = 1;
    const IS_NOT_ACTIVE     = 2;

    const IS_STATUS_LOGGEDOUT   = 1;
    const IS_STATUS_LOGGEDIN    = 2;
    const IS_STATUS_PENDING     = 3;

    public function statusLoginData()
    {
        return [
            self::IS_STATUS_LOGGEDOUT           => "Logged Out",
            self::IS_STATUS_LOGGEDIN            => "Logged In",
            self::IS_STATUS_PENDING             => "Pending",
        ];
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    // attributes
    public function getStatusLogoutFormattedAttribute()
    {
        return labelHelper($this->statusLoginData()[$this->status_logout] ?? "-", "badge bg-success");
    }
    // 

    // relations
    public function perusahaan()
    {
        return $this->hasOne(Perusahaan::class);
    }
    // 

    // scopes
    public function scopeFilter($query, $request)
    {
        if ($request->has("search") && !empty($request->search["value"])) {
            $query->where("name","LIKE","%" . $request->search["value"] . "%")
                  ->orWhere("email","LIKE","%" . $request->search["value"] . "%")
                  ->orWhereHas("roles", function ($query) use ($request)
                  {
                    $query->where("name","LIKE","%" . $request->search["value"] . "%");
                  });
        }
    }
    // 

    // DATATABLE
    public function orderColumn($key) : String
    {
        $columns                = [
            1               => "name",
            2               => "email",
        ];

        return $columns[$key];
    }

    public function scopeOrderDatatable($query, $request) : void
    {
        if ($request->has("order") && !empty($request->order)) {
            $orderAttr  = $request->order[0];
            $query->orderBy($this->orderColumn($orderAttr["column"]), $orderAttr["dir"]);
            return;
        }

        $query->orderBy("id","DESC");
    }
    // 
}
