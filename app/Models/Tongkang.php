<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tongkang extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "tongkang";
    protected $fillable = [
        "nama",
        "nama_pemilik",
        "ukuran",
        "tonase_kotor",
        "tanda_pendaftaran",
        "tanggal_surat_laut",
        "tanggal_ukur",
        "masa_berlaku_konstruksi",
        "masa_berlaku_lambung",
        "masa_berlaku_garis_muat",
        "surat_laut_file",
        "surat_ukur_file",
        "konstruksi_file",
        "lambung_file",
        "garis_muat_file",
        "perusahaan_id",
    ];

    // const
    // 

    // data
    public  static function listFiles()
    {
        return [
            [
                "name"                  => "surat_laut_file",
                "label"                 => "Surat Laut",
            ],
            [
                "name"                  => "surat_ukur_file",
                "label"                 => "Surat Ukur",
            ],
            [
                "name"                  => "konstruksi_file",
                "label"                 => "Konstruksi",
            ],
            [
                "name"                  => "lambung_file",
                "label"                 => "Lambung",
            ],
            [
                "name"                  => "garis_muat_file",
                "label"                 => "Garis Muat",
            ],
        ];
    }
    // 

    // relations
    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, "perusahaan_id");
    }
    // 

    // attributes
    public function getTanggalSuratLautFormattedAttribute()
    {
        return formatDate("Y-m-d","d-m-Y", $this->tanggal_surat_laut);
    }

    public function getTanggalUkurFormattedAttribute()
    {
        return formatDate("Y-m-d","d-m-Y", $this->tanggal_ukur);
    }

    public function getMasaBerlakuKonstruksiFormattedAttribute()
    {
        return formatDate("Y-m-d","d-m-Y", $this->masa_berlaku_konstruksi);
    }

    public function getMasaBerlakuLambungFormattedAttribute()
    {
        return formatDate("Y-m-d","d-m-Y", $this->masa_berlaku_lambung);
    }

    public function getMasaBerlakuGarisMuatFormattedAttribute()
    {
        return formatDate("Y-m-d","d-m-Y", $this->masa_berlaku_garis_muat);
    }

    public function setTanggalSuratLautAttribute($val)
    {
        $this->attributes["tanggal_surat_laut"] = formatDate("d-m-Y","Y-m-d", $val);
    }

    public function setTanggalUkurAttribute($val)
    {
        $this->attributes["tanggal_ukur"] = formatDate("d-m-Y","Y-m-d", $val);
    }

    public function setMasaBerlakuKonstruksiAttribute($val)
    {
        $this->attributes["masa_berlaku_konstruksi"] = formatDate("d-m-Y","Y-m-d", $val);
    }

    public function setMasaBerlakuLambungAttribute($val)
    {
        $this->attributes["masa_berlaku_lambung"] = formatDate("d-m-Y","Y-m-d", $val);
    }

    public function setMasaBerlakuGarisMuatAttribute($val)
    {
        $this->attributes["masa_berlaku_garis_muat"] = formatDate("d-m-Y","Y-m-d", $val);
    }

    public function getSuratLautFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->surat_laut_file)) {
            $str   = Storage::url("tongkang_surat_laut_file/" . $this->surat_laut_file);
        }

        return $str;
    }

    public function getSuratUkurFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->surat_ukur_file)) {
            $str   = Storage::url("tongkang_surat_ukur_file/" . $this->surat_ukur_file);
        }

        return $str;
    }

    public function getKonstruksiFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->konstruksi_file)) {
            $str   = Storage::url("tongkang_konstruksi_file/" . $this->konstruksi_file);
        }

        return $str;
    }

    public function getLambungFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->lambung_file)) {
            $str   = Storage::url("tongkang_lambung_file/" . $this->lambung_file);
        }

        return $str;
    }

    public function getGarisMuatFileStorageAttribute()
    {
        $str       = asset('assets/dashboard/images/noimage.png');

        if (!empty($this->garis_muat_file)) {
            $str   = Storage::url("tongkang_garis_muat_file/" . $this->garis_muat_file);
        }

        return $str;
    }
    // 

    // scopes
    public function scopeFilter($query, $request)
    {
        if (auth()->user()->roles->first()->name == "perusahaan") {
            empty(auth()->user()->perusahaan) ? $query->whereNull("id") : $query->where("perusahaan_id", auth()->user()->perusahaan->id);
        }

        return $query;
    }
    // 
}
