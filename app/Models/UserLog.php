<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserLog extends Model
{
    use HasFactory, SoftDeletes;

    protected $table    = "user_log";
    protected $fillable = [
        "user_id",
        "menu",
        "log",
        "tag",
        "action",
        "file",
    ];

    protected $casts 	= [
    	'tag'		=> 'array',
    ];

    // relations
    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }
    // 

    // attributes
    public function getTagFormattedAttribute()
    {
        $html           = "";
        if (!empty($this->tag)) {
            $html       = "<ul>";

            foreach ($this->tag as $key => $value) {
                $html   .= "<li>";
                $html   .= ucwords(str_replace("_", " ", $key)) . " = " . $value;
                $html   .= "</li>";
            }

            $html       .= "</ul>";
        }

        return $html;
    }

    public function getCreatedAtFormattedAttribute()
    {
        return formatDate("Y-m-d H:i:s","d-m-Y H:i:s", $this->created_at);
    }

    public function getFileUrlAttribute() : String
    {
        $str       = "";

        if (!empty($this->file)) {
            $str   = Storage::url("user_log/" . $this->file);
        }

        return $str;
    }

    public function scopeFilterDatatable($query, $request)
    {
        if ($request->has("tanggal_mulai_akhir") && !empty($request->tanggal_mulai_akhir)) {
            $explode    = explode(" to ", $request->tanggal_mulai_akhir);
            if (count($explode) > 1) {
                $query->whereDate("created_at",'>=', formatDate("d-m-Y","Y-m-d", $explode[0]))
                      ->whereDate("created_at",'<=', formatDate("d-m-Y","Y-m-d", $explode[1]));
            }
        }
    }
    // 
}
