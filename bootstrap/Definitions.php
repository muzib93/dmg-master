<?php

//This is the place where you would place all your constants

abstract class JenisKelamin {
    const Laki      = 1;
    const Perempuan = 2;

    public static function listData() : array
    {
        return [
            self::Laki              => "Laki-laki",
            self::Perempuan         => "Perempuan",
        ];
    }
}

abstract class KelasJabatan {

    public static function listData() : array
    {
        $arr    = range(1, 17);

        array_unshift($arr,"");
        unset($arr[0]);

        return $arr;
    }
}

abstract class Etakah {

    const STATUS_PENDING        = 0;
    const STATUS_VERIFIED       = 2;
    const STATUS_NOT_VERIFIED   = 3;

    public static function listStatus()
    {
        return [
            self::STATUS_PENDING            => "PENDING",
            self::STATUS_VERIFIED           => "VERIFIKASI",
            self::STATUS_NOT_VERIFIED       => "BELUM UPLOAD",
        ];
    }

    public static function statusFormatted($verif)
    {
        $verif      = $verif === null ? self::STATUS_NOT_VERIFIED : $verif;

        switch ($verif) {
            case self::STATUS_PENDING:
                $color  = "bg-warning";
                break;
            case self::STATUS_VERIFIED:
                $color  = "bg-success";
                break;
            default:
                $color  = "bg-danger";
                break;
        }

        return labelHelper(self::listStatus()[$verif], "badge " . $color);
    }

    public static function berkasFormatted($berkasUrl)
    {
        if ($berkasUrl == null) {
            return " - ";
        }

        $html   = '<a href="' . $berkasUrl . '" class="btn btn-primary btn-sm" target="_blank">
            <i class="fa fa-file"></i>
        </a>';

        return $html;
    }
}

abstract class Siasn {

    const SYNC_SIASN_SYNCED         = 1;
    const SYNC_SIASN_NOT_SYNCED     = 2;

    public  static function listSyncSiasn()
    {
        return [
            self::SYNC_SIASN_SYNCED         => "Sudah Sinkronisasi",
            self::SYNC_SIASN_NOT_SYNCED     => "Belum Sinkronisasi",
        ];
    }

    public  static function syncSiasnFormatted($syncSiasn)
    {
        switch ($syncSiasn) {
            case self::SYNC_SIASN_SYNCED:
                $color  = "bg-success";
                break;
            case self::SYNC_SIASN_NOT_SYNCED:
                $color  = "bg-warning";
                break;
            default:
                $color  = "bg-secondary";
                break;
        }

        return labelHelper(self::listSyncSiasn()[$syncSiasn] ?? "-", "badge " . $color);   
    }

}