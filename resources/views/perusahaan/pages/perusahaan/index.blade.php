@extends('dashboard.layouts.app')

@push('dashboard.css')
    
@endpush


@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{ $sub_title }}</h4>
                    </div>
                    <div class="card-body mt-2 pb-2">
                        @include('perusahaan.pages.perusahaan.form')
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">File Attachment</h4>
                    </div>
                    <div class="card-body mt-2 pb-2">
                        @include('perusahaan.pages.perusahaan.file_attachment')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
@endpush