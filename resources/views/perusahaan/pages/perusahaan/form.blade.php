{!! Form::open(['route' => $routes['store'], 'method' => 'POST','class' => 'form form-horizontal','enctype' => 'multipart/form-data']) !!}
                        
    <div class="row">
        <div class="col-12">
            <div class="mb-1 row">
                <div class="col-sm-3">
                    <label class="col-form-label">Nama Perusahaan</label>
                </div>
                <div class="col-sm-9">
                    {!! Form::text('nama_perusahaan', auth()->user()->perusahaan->nama_perusahaan ?? "", ['class' => 'form-control', 'placeholder' => 'Nama Perusahaan']) !!}
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="mb-1 row">
                <div class="col-sm-3">
                    <label class="col-form-label">Nama Pemilik</label>
                </div>
                <div class="col-sm-9">
                    {!! Form::text('nama_pemilik', auth()->user()->perusahaan->nama_pemilik ?? "", ['class' => 'form-control', 'placeholder' => 'Nama Pemilik']) !!}
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="mb-1 row">
                <div class="col-sm-3">
                    <label class="col-form-label">NIB</label>
                </div>
                <div class="col-sm-9">
                    {!! Form::number('nib', auth()->user()->perusahaan->nib ?? "", ['class' => 'form-control', 'placeholder' => 'NIB']) !!}
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="mb-1 row">
                <div class="col-sm-3">
                    <label class="col-form-label">IUP</label>
                </div>
                <div class="col-sm-9">
                    {!! Form::text('iup', auth()->user()->perusahaan->iup ?? "", ['class' => 'form-control', 'placeholder' => 'IUP']) !!}
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="mb-1 row">
                <div class="col-sm-3">
                    <label class="col-form-label">Jenis</label>
                </div>
                <div class="col-sm-9">
                    {!! Form::select('jenis[]', $items["list_jenis"] ?? [], !empty(auth()->user()->perusahaan) && auth()->user()->perusahaan->perusahaanJenis->isNotEmpty() ? auth()->user()->perusahaan->perusahaanJenis->pluck("jenis") : [], ['class' => 'form-control select2', 'multiple']) !!}
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-9 offset-sm-3">
            <button type="submit" class="btn btn-primary me-1">Submit</button>
        </div>
    </div>
{!! Form::close() !!}