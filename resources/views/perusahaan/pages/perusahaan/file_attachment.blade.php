<div class="row">
    @if (empty(auth()->user()->perusahaan))
        <div class="alert alert-danger" role="alert">
            <div class="alert-body">
                <i data-feather="info" class="me-50"></i>
                <span>Harap Lengkapi Data Perusahaan Terlebih Dahulu</span>
            </div>
        </div>
    @else
        @foreach ($items["list_files"] as $listFiles)
            <div class="col-md-4" id="{{ $listFiles["name"] }}">
                @if (!empty(auth()->user()->perusahaan) && !empty(auth()->user()->perusahaan->{$listFiles["name"]}))
                    @include('components.files.after_upload', ["fileUrl" => auth()->user()->perusahaan->{$listFiles["name"] . "_storage"}, "fileName" => $listFiles["label"], "removeFileUrl" => route("user-perusahaan.perusahaan.remove-file", [auth()->user()->perusahaan->id, $listFiles["name"]]), ""])
                @else
                    @include("components.files.before_upload", ["fileName" => $listFiles["label"], "fileField" => $listFiles["name"], "uploadUrl" => route("user-perusahaan.perusahaan.upload", auth()->user()->perusahaan->id)])
                @endif
            </div>
        @endforeach
    @endif
</div>