<div class="card h-100">
    <div class="card-body d-flex justify-content-center text-center flex-column p-8 box-shadow-file">
        <a href="{{ $fileUrl }}" target="_blank" class="text-gray-800 text-hover-primary d-flex flex-column">
            <div class="symbol symbol-60px mb-1">
                <img src="https://efile.trilliun.com/assets/media/svg/files/file.svg" alt="">
            </div>
            <div class="fs-5 fw-bolder mb-2">{{ $fileName }}</div>
        </a>
    </div>
    <div class="image-input image-input-outline" data-kt-image-input="true">
        <span style="background-color: white !important;" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow remove-button" data-bs-toggle="tooltip" title="" onclick="RemoveFile('{{ $removeFileUrl }}')" data-bs-original-title="Remove file">
            <i class="fa fa-x fs-5"></i>
        </span>
    </div>
</div>