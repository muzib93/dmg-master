<div class="card h-100 flex-center bg-light-primary border-primary border border-dashed p-8 box-file">
    <img src="https://efile.trilliun.com/assets/media/svg/files/upload.svg" alt="">
    <a href="#" class="text-hover-primary fs-5 fw-bolder mb-2">{{ $fileName }}</a>
    <div class="fs-7 fw-bold text-gray-400 file-name" style="text-align: center;">Upload File</div>
    <label for="doc-2">
        <input type="file" class="file-upload" name="dokumen_ktp" onchange="UploadFile(this)" data-url="{{ $uploadUrl }}" data-file="{{ $fileField }}">
    </label>
</div>