@extends('dashboard.layouts.app')

@push('dashboard.css')
    <style>
    </style>    
@endpush

@section('dashboard.content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $sub_title }}</h2>
                    <h2 class="float-start mb-0">&nbsp; {{ $item->nama }} ({{ $item->perusahaan->nama_perusahaan }})</h2>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <div class="btn-group" role="group" aria-label="">
                    <div class="dropdown">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-5 order-1 order-md-0 sidebar-pmb-application">
                <div class="card">
                    <div class="card-body">
                        <div class="user-avatar-section mb-2">
                            <div class="d-flex align-items-center flex-column">
                                <div class="mb-1 mt-1">
                                    <i class="fa fa-anchor" style="font-size: 100px;"></i>
                                </div>
                                <div class="user-info text-center">
                                    <h4>{{ $item->nama }}</h4>
                                    <span class="badge bg-light-secondary">{{ $item->perusahaan->nama_perusahaan ?? "-" }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="info-container">
                            <div class="sidebar-menu-list ps ps--active-y">
                                <div class="list-group list-group-messages">
                                    <div class="fw-bolder mt-1">Lokasi</div>
                                    <div class="text-gray-600">{{ $item->lokasi ?? "-" }}</div>
                                    <div class="fw-bolder mt-1">Titik Kordinat</div>
                                    <div class="text-gray-600">{{ $item->titik_kordinat ?? "-" }}</div>
                                    <div class="fw-bolder mt-1">Ijin Jetty</div>
                                    <div class="text-gray-600">{{ $item->ijin_jetty }}</div>
                                </div>
                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                </div>
                                <div class="ps__rail-y" style="top: 0px; right: 0px;">
                                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-9 col-lg-9 col-md-7 order-0 order-md-1">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-pills mb-2">
                            <li class="nav-item">
                                <a class="nav-link active" id="file-attachment-tab" data-bs-toggle="tab" href="#file-attachment" aria-controls="file-attachment" role="tab" aria-selected="true">
                                    <i class="fa fa-box-archive"></i>
                                    <span class="fw-bold">File Attachment</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="tab-content">
                            @include('dashboard.pages.jetty.components.file_attachment')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
@endpush