<div class="row">
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Nama Jetty</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Jetty']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Lokasi</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('lokasi', null, ['class' => 'form-control', 'placeholder' => 'Lokasi']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Titik Koordinat</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('titik_kordinat', null, ['class' => 'form-control', 'placeholder' => 'Titik Koordinat']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Ijin Jetty</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('ijin_jetty', null, ['class' => 'form-control', 'placeholder' => 'Ijin Jetty']) !!}
            </div>
        </div>
    </div>
</div>