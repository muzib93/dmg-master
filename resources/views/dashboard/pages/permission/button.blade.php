<div class="btn-group">
    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
        Options
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="{{ route($routes['edit'], $model->id) }}">Edit</a>
        {!! Form::open(['route' => [$routes['destroy'], $model->id], 'method' => 'DELETE', 'class' => 'delete','style' => 'display: contents']) !!}
            <a class="dropdown-item" href="#" onclick="SwalDelete($(this).closest('form'))">Delete</a>
        {!! Form::close() !!}
    </div>
</div>