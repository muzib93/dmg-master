@extends('dashboard.layouts.app')

@push('dashboard.css')
    
@endpush


@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{ $sub_title }}</h4>
                    </div>
                    {{-- <div class="card-body mt-2">
                        <form class="dt_adv_search" method="POST">
                            <div class="row g-1 mb-md-1">
                                <div class="col-md-4">
                                    <label class="form-label">Name:</label>
                                    <input type="text" class="form-control dt-input dt-full-name" data-column="1" placeholder="Alaric Beslier" data-column-index="0" />
                                </div>
                                <div class="col-md-4">
                                    <label class="form-label">Email:</label>
                                    <input type="text" class="form-control dt-input" data-column="2" placeholder="demo@example.com" data-column-index="1" />
                                </div>
                                <div class="col-md-4">
                                    <label class="form-label">Post:</label>
                                    <input type="text" class="form-control dt-input" data-column="3" placeholder="Web designer" data-column-index="2" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <hr class="my-0" /> --}}
                    <div class="card-datatable pb-2">
                        <table class="dt-general table">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th>Name</th>
                                    <th>Guard</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
    <script>
        const datatableUrl       = InitDatatable("{{ route($routes['data']) }}", 
                                                ["name", "guard_name"]);
    </script>
@endpush