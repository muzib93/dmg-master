<div class="row">
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Nama Pemilik</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('nama_pemilik', null, ['class' => 'form-control', 'placeholder' => 'Nama Pemilik']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">NIB</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('nib', null, ['class' => 'form-control', 'placeholder' => 'NIB']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Nama Tugboat</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Tugboat']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Ukuran (PxLxD)</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('ukuran', null, ['class' => 'form-control', 'placeholder' => 'Ukuran (PxLxD)']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Tonase Kotor (GT)</label>
            </div>
            <div class="col-sm-9">
                {!! Form::number('tonase_kotor', null, ['class' => 'form-control', 'placeholder' => 'Tonase Kotor (GT)']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Registrasi PAS / Tanda PAS</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('registrasi_pas', null, ['class' => 'form-control', 'placeholder' => 'Registrasi PAS / Tanda PAS']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Masa Berlaku SKKSD (s/d Expired Dicekal)</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('masa_berlaku_skksd', isset($item) ? $item->masa_berlaku_skksd_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Masa Berlaku SKKSD (s/d Expired Dicekal)']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Surat Ukur Masa Berlaku s/d</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('surat_ukur_berlaku', isset($item) ? $item->surat_ukur_berlaku_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Surat Ukur Masa Berlaku s/d']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Tanggal PAS Sungai Danau & Endorse</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('tanggal_pas', isset($item) ? $item->tanggal_pas_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Tanggal PAS Sungai Danau & Endorse']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Masa Berlaku Izin Operasi</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('masa_berlaku_izin_operasi', isset($item) ? $item->masa_berlaku_izin_operasi_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Masa Berlaku Izin Operasi']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Masa Berlaku Izin Trayek</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('masa_berlaku_izin_trayek', isset($item) ? $item->masa_berlaku_izin_trayek_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Masa Berlaku Izin Trayek']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Jenis Kapal</label>
            </div>
            <div class="col-sm-9">
                {!! Form::select('jenis_kapal', $items["list_jenis_kapal"], null, ['class' => 'form-control select2', 'placeholder' => 'Pilih Jenis Kapal']) !!}
            </div>
        </div>
    </div>
</div>