<div class="tab-pane" id="nahkoda" aria-labelledby="nahkoda-tab" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><i class="fa fa-users"></i> Nahkoda</h4>
            <div class="float-end">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-nahkoda">
                    <i class="fa fa-plus"></i> Tambah Nahkoda
                </button>
            </div>
        </div>
        <div class="card-body mt-2">
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="dt-client-side table">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th>Nama</th>
                                    <th>SKAK Nahkoda</th>
                                    <th>Tanggal Aktif</th>
                                    <th class="text-center">File Attachment</th>
                                    <th class="text-center" width="2%">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($item->nahkoda as $nahkoda)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $nahkoda->nama }}</td>
                                        <td>{{ $nahkoda->tanggal_skak_formatted }}</td>
                                        <td>{{ $nahkoda->tanggal_aktif_formatted }}</td>
                                        <td class="text-center">
                                            <a href="{{ $nahkoda->skak_file_storage }}" target="_blank" class="btn btn-primary btn-xs me-1">
                                                <i class="fa fa-file"></i> SKAK
                                            </a>
                                            <a href="{{ $nahkoda->ijazah_laut_file_storage }}" target="_blank" class="btn btn-primary btn-xs me-1">
                                                <i class="fa fa-file"></i> Ijazah Laut
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            {!! Form::open(['route' => ["tugboat.nahkoda.delete", $nahkoda->id], 'method' => 'DELETE', 'class' => 'delete','style' => 'display: contents']) !!}
                                                <a class="btn btn-danger btn-sm" href="#" onclick="SwalDelete($(this).closest('form'))">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-start" id="modal-nahkoda" tabindex="-1" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="">Tambah Nahkoda</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            {!! Form::open(['route' => ["tugboat.nahkoda.store", $item->id], 'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                <div class="modal-body">
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label class="form-label" for="">Nama</label>
                                <input type="text" name="nama" class="form-control" id="" placeholder="Nama">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label class="form-label" for="">Tanggal SKAK</label>
                                <input type="text" name="tanggal_skak" class="form-control flatpicker" id="" placeholder="Tanggal SKAK">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label class="form-label" for="">Tanggal Aktif Sementara Dalam Proses</label>
                                <input type="text" name="tanggal_aktif" class="form-control flatpicker" id="" placeholder="Tanggal Aktif Sementara Dalam Proses">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label class="form-label" for="">SKAK File</label>
                                <input type="file" name="skak_file" class="form-control">
                                <small>Tipe file : jpg, png, pdf. Ukuran file maksimal 5mb</small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-2">
                                <label class="form-label" for="">Ijazah Laut File</label>
                                <input type="file" name="ijazah_laut_file" class="form-control">
                                <small>Tipe file : jpg, png, pdf. Ukuran file maksimal 5mb</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('dashboard.js')
    <script>
        $(document).ready(function () {
            $('.dt-client-side').DataTable({
                ordering: false,
                language: {
                    paginate: {
                        previous:'&nbsp;',
                        next: '&nbsp;'
                    },
                    processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>'
                },
            });
        });
    </script>
@endpush