<div class="tab-pane active" id="file-attachment" aria-labelledby="file-attachment-tab" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><i class="fa fa-box-archive"></i> File Attachment</h4>
        </div>
        <div class="card-body mt-2">
            <div class="row mb-2">
                @foreach ($items["list_files"] as $listFiles)
                    <div class="col-md-4 mb-2" id="{{ $listFiles["name"] }}">
                        @if (!empty($item) && !empty($item->{$listFiles["name"]}))
                            @include('components.files.after_upload', ["fileUrl" => $item->{$listFiles["name"] . "_storage"}, "fileName" => $listFiles["label"], "removeFileUrl" => route("tugboat.remove-file", [$item->id, $listFiles["name"]]), ""])
                        @else
                            @include("components.files.before_upload", ["fileName" => $listFiles["label"], "fileField" => $listFiles["name"], "uploadUrl" => route("tugboat.upload", $item->id)])
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>