@extends('dashboard.layouts.app')

@push('dashboard.css')
    
@endpush


@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{ $sub_title }}</h4>
                        <a href="{{ route($routes['create']) }}" class="btn btn-primary">
                            <i data-feather="plus"></i> Add
                        </a>
                    </div>
                    <div class="card-datatable pb-2">
                        <table class="dt-general table">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th>Nama</th>
                                    <th>Perusahaan</th>
                                    <th>Pemilik</th>
                                    <th>Jenis Kapal</th>
                                    <th class="text-center" width="2%">Options</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
    <script>
        const datatableUrl       = InitDatatable("{{ route($routes['data']) }}", 
                                                  ["nama", "nama_perusahaan", "nama_pemilik","jenis_kapal_formatted","options"],
                                                  []);
    </script>
@endpush