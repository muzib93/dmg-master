<div class="card cost-chart-card">
    <div class="card-header border-bottom mb-2" style="display: block;">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group input-group-merge">
                    <span class="input-group-text" id="grafik-karyawan-type">Tampilkan By :</span>
                    {!! Form::select('', $dataTypeGrafik, null, ['class' => 'form-control', 'wire:model' => 'type', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group input-group-merge">
                    <span class="input-group-text">Perusahaan :</span>
                    {!! Form::select('', $dataPerusahaan, null, ['class' => 'form-control', 'wire:model' => 'perusahaan', 'readonly']) !!}
                    <span class="input-group-text" wire:click="$set('perusahaan', 'semua')">
                        <i class="fa fa-undo"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-primary text-center" role="alert" id="{{ $namaChart }}-empty"
                    style="display: none">
                    <h4 class="alert-heading"><i class="fa fa-circle-exclamation"></i> Data Kosong</h4>
                </div>
                <div class="chart-container">
                    <canvas class="full-width" id="{{ $namaChart }}"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

@push('dashboard.js')
    <script>
        $(document).ready(function() {
            function optionsGrafikKaryawan(title) {
                return {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: title
                    },
                    onClick: function(evt, i) {
                        let activePoint = chart.getElementAtEvent(evt)[0];
                        let data = activePoint._chart.data;
                        e = i[0];
                        let label = this.data.labels[e._index];
                        $('#modalDetailKaryawan').modal('show', {label: label, type : @this.type, perusahaan: @this.perusahaan});
                    },
                }
            };

            let chart = new Chart(document.getElementById(@this.namaChart), {
                type: 'bar',
                data: {
                    labels: @json($labels),
                    datasets: @json($dataset),
                },
                options: optionsGrafikKaryawan(@this.title),
            });

            Livewire.on('updateChart-{{ $namaChart }}', function(data, status) {
                let scroll = $(document).scrollTop();
                chart.destroy();
                if (status) {
                    $('#{{ $namaChart }}-empty').hide();
                    let ctx = document.getElementById(@this.namaChart).getContext("2d");
                    chart = myChart = new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: optionsGrafikKaryawan(@this.title)
                    });
                    $(document).scrollTop(scroll);
                } else {
                    $('#{{ $namaChart }}-empty').show();
                }
            });
        });
    </script>
@endpush
