@foreach ($labels as $key => $label)
    <div class="col-md-12 mb-1">
        <span class="fw-bolder">{{ $label }}</span>
        <span class="fw-bolder float-end">{{ $data[$key] ?? 0 }}</span>
    </div>
@endforeach