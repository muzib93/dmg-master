<div class="card cost-chart-card">
    <div class="card-header border-bottom mb-2" style="display: block;">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group input-group-merge">
                    <span class="input-group-text">Tampilkan :</span>
                    {!! Form::select('', $dataTypeGrafik, null, ['class' => 'form-control', 'wire:model' => 'type', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group input-group-merge">
                    <span class="input-group-text">Tanggal :</span>
                    <input type="text" class="form-control flatpicker" wire:model='tanggal' readonly />
                    <span class="input-group-text" wire:click="$set('tanggal', null)">
                        <i class="fa fa-undo"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="alert alert-primary text-center" role="alert" id="{{ $namaChart }}-empty"
                style="display: none">
                <h4 class="alert-heading"><i class="fa fa-circle-exclamation"></i>Data Kosong</h4>
            </div>
            <div class="col-md-{{ $type == 'semua' ? '12' : '8' }}">
                <div class="chart-container">
                    <canvas class="full-width" id="{{ $namaChart }}"></canvas>
                </div>
            </div>
            @if ($type != 'semua' && $dataTipeJangkauan['total'] > 0)
                @php $data = json_decode(json_encode($dataTipeJangkauan)); @endphp
                <div class="col-md-4">
                    <b>Tipe Jangkauan</b>
                    <p>Didalam : {{ $data->didalam->count }}</p>
                    <div class="progress mb-2">
                        <div class="progress-bar bg-success" role="progressbar"
                            style="width: {{ $data->didalam->percent }}%;" aria-valuenow="{{ $data->didalam->count }}"
                            aria-valuemax="{{ $data->total }}" aria-valuemin='0'>
                            {{ $data->didalam->count }}
                        </div>
                    </div>
                    <p>Diluar : {{ $data->diluar->count }}</p>
                    <div class="progress">
                        <div class="progress-bar bg-warning" role="progressbar"
                            style="width: {{ $data->diluar->percent }}%;" aria-valuenow="{{ $data->diluar->count }}"
                            aria-valuemin='0' aria-valuemax="{{ $data->total }}">
                            {{ $data->diluar->count }}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

@push('dashboard.js')
    <script>
        $(document).ready(function() {
            let chart = new Chart(document.getElementById(@this.namaChart), {
                type: 'pie',
                data: {
                    labels: @json($labels),
                    datasets: @json($dataset),
                },
                options: @json($options),
            });

            Livewire.on('updateChart-{{ $namaChart }}', function(data, status) {
                let scroll = $(document).scrollTop();
                chart.destroy();
                if (status) {
                    $('#{{ $namaChart }}-empty').hide();
                    let ctx = document.getElementById(@this.namaChart).getContext("2d");
                    chart = myChart = new Chart(ctx, {
                        type: 'pie',
                        data: data,
                        options: @this.options
                    });
                    $(document).scrollTop(scroll);
                } else {
                    $('#{{ $namaChart }}-empty').show();
                }
            });
        });
    </script>
@endpush
