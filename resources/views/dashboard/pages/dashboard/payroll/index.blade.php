@extends('dashboard.layouts.app')

@push('dashboard.css')
    <link rel="stylesheet" href="{{ asset('assets/dashboard/vendors/css/charts/apexcharts.css') }}">
@endpush


@section('dashboard.content')
    <section id="dashboard-analytics">

        <div class="row">
            <div class="col-md-8">
                <div class="card cost-chart-card" id="card-grafik-thp-karyawan">
                    <div class="card-header border-bottom mb-2" style="display: block;">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12">
                                <span class="card-title">Grafik THP Karyawan</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <div class="input-group input-group-merge">
                                    <span class="input-group-text" id="grafik-karyawan-type">Tampilkan By :</span>
                                    {!! Form::select('type_grafik', $items['typeGrafik'], null, ['class' => 'form-control ps-1', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group input-group-merge">
                                    <span class="input-group-text" id="grafik-karyawan-type">Tahun :</span>
                                    {!! Form::select('tahun', $items['listTahun'], null, ['class' => 'form-control ps-1', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group input-group-merge">
                                    <span class="input-group-text">Perusahaan :</span>
                                    {!! Form::select('perusahaan_id', $items['dataPerusahaan'], null, ['class' => 'form-control ps-1', 'readonly']) !!}
                                    <span class="input-group-text btn btn-primary" style="cursor: pointer" onclick="ChangeGrafik()">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div id="chart"></div>
                    </div>
                </div>
            </div>
            <div class="row col-md-4">
                <div class="card cost-chart-card">
                    <div class="card-header border-bottom mb-2" style="display: block;">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12">
                                <span class="card-title">Reminder</span>
                                <div class="float-end">
                                    <button class="btn btn-primary btn-sm" data-bs-toggle="modal"
                                        data-bs-target="#modal-kontrak-expired">
                                        <i class="fa fa-list"></i> Lihat
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($items['reminder']['kontrak_perusahaan']->isEmpty() && $items['reminder']['kontrak_karyawan']->isEmpty())
                            <div class="row" style="font-size: 12px;">
                                <div class="alert alert-success text-center" role="alert">
                                    <div class="alert-body"><strong>Reminder Kosong.</div>
                                </div>
                            </div>
                        @else
                            @foreach ($items['reminder']['kontrak_karyawan'] as $kontrakKaryawan)
                                <div class="row" style="font-size: 12px;">
                                    <div class="col-12">
                                        <div class="alert alert-danger" role="alert">
                                            <h4 class="alert-heading"><i class="fa fa-circle-exclamation"></i> Kontrak
                                                Karyawan Akan/Sudah Berakhir.</h4>
                                            <div class="alert-body">
                                                <span><b>{{ $kontrakKaryawan->penempatanAktif->statusKaryawan->nama }}</b>
                                                    Karyawan <a href="#"
                                                        class="fw-bolder">{{ $kontrakKaryawan->nama }}</a> akan segera
                                                    berakhir pada
                                                    <b>{{ $kontrakKaryawan->penempatanAktif->tanggal_akhir_formatted }}</b>,
                                                    segera lakukan pembayaran uang pesangon jika tidak diperpanjang.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('dashboard.pages.dashboard.components.modal_kontrak_expired')

@endsection

@push('dashboard.js')
    <script src="{{ asset('assets/dashboard/vendors/js/charts/apexcharts.js') }}"></script>
    <script>
        $(document).on({
            ajaxStart: function() {
                $(".overlay-spinner-class").fadeIn(300)
            },
            ajaxStop: function() {
                setTimeout(function() {
                    $(".overlay-spinner-class").fadeOut(300);
                }, 500);
            }
        });

        function FormatRupiah(angka) {
            return new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR"
            }).format(angka);
        }

        ChangeGrafik();


        function ChangeGrafik() {
            type_grafik = $("#card-grafik-thp-karyawan [name=type_grafik]").val();
            perusahaan_id = $("#card-grafik-thp-karyawan [name=perusahaan_id]").val();
            tahun = $("#card-grafik-thp-karyawan [name=tahun]").val();
            $.ajax({
                url:"{{route('dashboard-payroll.payroll-perusahaan')}}",
                type:'POST',
                data:{
                    _token: "{{ csrf_token() }}",
                    type_grafik: type_grafik,
                    perusahaan_id: perusahaan_id,
                    tahun: tahun,
                },
                success: function(data){
                    if (data.error == 0) {
                        $("#chart").html(data.html);
                    }else{
                        toastr.warning(data.message);
                    }
                },
                error:function(data){
                    if (data.message) {
                        toastr.error(data.message);
                    }else{
                        toastr.error("");
                    }
                }
            });
        }
    </script>
@endpush
