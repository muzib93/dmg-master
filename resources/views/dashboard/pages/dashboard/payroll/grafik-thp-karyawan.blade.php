<div id="grafik"></div>
<script>
    var options = {
        series: [{
            name: "Desktops",
            data: @json($seriesData) // Mengambil data dari PHP
        }],
        chart: {
            height: 350,
            type: 'area',
            toolbar: {
                show: true,
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'straight'
        },
        title: {
            text: "",
            align: 'left'
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        
        xaxis: {
            categories: @json($categories) // Mengambil data dari PHP
        },
        yaxis: {
            labels: {
                padding: 4,
                formatter: function (value) {
                    return FormatRupiah(value);
                }
            },
        },
    };

    var chart = new ApexCharts(document.querySelector("#grafik"), options);
    chart.render();
</script>
