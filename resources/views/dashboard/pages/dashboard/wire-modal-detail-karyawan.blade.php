<div id="modalDetailKaryawan" class="modal fade" tabindex="-1" wire:ignore.self>
    <div class="modal-dialog modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="col-12 modal-title text-center">Detail Grafik Karyawan</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body bg-white">
                <p style="text-align: center">
                    By : {{ ucwords(str_replace('-', ' ', $type)) }} ({{ $label }})
                    | Perusahaan : {{ ucwords($stringPerusahaan) }}
                </p>
                <div class="dataTables_wrapper container-fluid dt-bootstrap4">
                    <div class="col-sm-12">
                        <table class="table table-striped table-hover" id="tabelRacikan" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nik</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data as $key => $item)
                                    <tr>
                                        <td>{{ $data->firstItem() + $key }}</td>
                                        <td>{{ $item->nik ?? '-' }}</td>
                                        <td>{{ $item->nama ?? '-' }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="100%">
                                            <div class="alert alert-warning text-center" role="alert">
                                                Data Kosong!
                                            </div>
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">{{ $data->links() }}</div>
            </div>
        </div>
    </div>
</div>

@push('dashboard.js')
    <script>
        $('#modalDetailKaryawan').on('shown.bs.modal', function(e) {
            @this.type = e.relatedTarget.type;
            @this.label = e.relatedTarget.label;
            @this.perusahaan = e.relatedTarget.perusahaan;
        });
    </script>
@endpush
