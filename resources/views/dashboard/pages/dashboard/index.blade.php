@extends('dashboard.layouts.app')

@push('dashboard.css')
@endpush


@section('dashboard.content')
    <section id="dashboard-analytics">
        <div class="row match-height">
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5 col-5"
                                style="border-right: 1px solid lightgray; height: 79px; text-align: center;">
                                <i class="fa fa-ship" style="font-size: 75px; color: #0171f3"></i>
                            </div>
                            <div class="col-md-7 col-7" style="margin-top: 8px;">
                                <div class="row">
                                    <div class="col-md-12" style="font-size: 13px;">
                                        <span>TUGBOAT</span>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 5px; font-size: 22px;">
                                        <span>{{ $items["counters"]["tugboat"] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5 col-5"
                                style="border-right: 1px solid lightgray; height: 79px; text-align: center;">
                                <i class="fa fa-inbox" style="font-size: 75px; color: #0171f3"></i>
                            </div>
                            <div class="col-md-7 col-7" style="margin-top: 8px;">
                                <div class="row">
                                    <div class="col-md-12" style="font-size: 13px;">
                                        <span>TONGKANG</span>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 5px; font-size: 22px;">
                                        <span>{{ $items["counters"]["tongkang"] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5 col-5"
                                style="border-right: 1px solid lightgray; height: 79px; text-align: center;">
                                <i class="fa fa-anchor" style="font-size: 75px; color: #0171f3"></i>
                            </div>
                            <div class="col-md-7 col-7" style="margin-top: 8px;">
                                <div class="row">
                                    <div class="col-md-12" style="font-size: 13px;">
                                        <span>JETTY</span>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 5px; font-size: 22px;">
                                        <span>{{ $items["counters"]["jetty"] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('dashboard.js')
    <script src="{{ asset('assets/dashboard/vendors/js/charts/chart.min.js') }}"></script>
    <script>
        $(document).on({
            ajaxStart: function() {
                $(".overlay-spinner-class").fadeIn(300)
            },
            ajaxStop: function() {
                setTimeout(function() {
                    $(".overlay-spinner-class").fadeOut(300);
                }, 500);
            }
        });
    </script>
@endpush
