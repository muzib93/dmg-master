<div class="modal fade text-start" id="modal-kontrak-expired" tabindex="-1" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="">List Kontrak Akan Berakhir</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row mt-2">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-pills mb-2">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="kontrak-karyawan-tab" data-bs-toggle="tab" href="#kontrak-karyawan" aria-controls="kontrak-karyawan" role="tab" aria-selected="true">
                                            <i data-feather="user" class="font-medium-3 me-50"></i>
                                            <span class="fw-bold">Kontrak Karyawan</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="kontrak-perusahaan-tab" data-bs-toggle="tab" href="#kontrak-perusahaan" aria-controls="kontrak-perusahaan" role="tab" aria-selected="false">
                                            <i data-feather="lock" class="font-medium-3 me-50"></i>
                                            <span class="fw-bold">Kontrak Perusahaan</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                <div class="tab-content">

                                    <div class="tab-pane active" id="kontrak-karyawan" aria-labelledby="kontrak-karyawan-tab" role="tabpanel">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Kontrak Karyawan</h4>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-bordered table-striped dt-general" style="font-size: 11px;">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Npk</th>
                                                            <th>Nama</th>
                                                            <th>Perusahaan</th>
                                                            <th>Departemen</th>
                                                            <th>Jabatan</th>
                                                            <th>Tanggal Mulai</th>
                                                            <th>Tanggal Akhir</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse ($items['reminder']["kontrak_karyawan_all"] as $itemKontrakKaryawan)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $itemKontrakKaryawan->penempatanAktif->npk }}</td>
                                                                <td>{{ $itemKontrakKaryawan->nama }}</td>
                                                                <td>{{ $itemKontrakKaryawan->penempatanAktif->perusahaan->nama }}</td>
                                                                <td>{{ $itemKontrakKaryawan->penempatanAktif->departemen->nama }}</td>
                                                                <td>{{ $itemKontrakKaryawan->penempatanAktif->jabatan->nama }}</td>
                                                                <td>{{ formatDate("Y-m-d","d-m-Y", $itemKontrakKaryawan->penempatanAktif->tanggal_mulai) }}</td>
                                                                <td>{{ formatDate("Y-m-d","d-m-Y", $itemKontrakKaryawan->penempatanAktif->tanggal_akhir) }}</td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td colspan="8" class="text-center">
                                                                    <i>Data Kosong.</i>
                                                                </td>
                                                            </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="kontrak-perusahaan" aria-labelledby="kontrak-perusahaan-tab" role="tabpanel">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Kontrak Perusahaan</h4>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-bordered table-striped dt-general" style="font-size: 11px;">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Perusahaan</th>
                                                            <th>No Kontrak</th>
                                                            <th>Tipe Kontrak Kontrak</th>
                                                            <th>Tanggal Mulai</th>
                                                            <th>Tanggal Akhir</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse ($items['reminder']["kontrak_perusahaan_all"] as $itemKontrakPerusahaan)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $itemKontrakPerusahaan->perusahaan->nama }}</td>
                                                                <td>{{ $itemKontrakPerusahaan->no_kontrak }}</td>
                                                                <td>{{ $itemKontrakPerusahaan->tipe_kontrak_str }}</td>
                                                                <td>{{ formatDate("Y-m-d","d-m-Y", $itemKontrakPerusahaan->tanggal_mulai) }}</td>
                                                                <td>{{ formatDate("Y-m-d","d-m-Y", $itemKontrakPerusahaan->tanggal_akhir) }}</td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td colspan="6" class="text-center">
                                                                    <i>Data Kosong.</i>
                                                                </td>
                                                            </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>