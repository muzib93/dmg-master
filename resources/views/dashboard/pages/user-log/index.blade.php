@extends('dashboard.layouts.app')

@push('dashboard.css')
    
@endpush


@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{ $sub_title }}</h4>
                    </div>
                    <div class="card-body mt-2 pb-0">
                        <form class="dt_adv_search" method="POST">
                            <div class="row g-1 mb-md-1">
                                <div class="col-xxxl-2 col-md-3">
                                    <label class="form-label">Tanggal Mulai - Akhir</label>
                                    {!! Form::text('tanggal_mulai_akhir', null, ['class' => 'form-control date-range-picker-filter', 'placeholder' => 'dd-mm-yyyy to dd-mm-yyyy','autocomplete' => 'off']) !!}
                                </div>
                                <div class="col-md-2">
                                    <label class="form-label">&nbsp;</label>
                                    <br>
                                    <button class="btn btn-primary" onclick="ClearFilterInput(this, event)">
                                        <i class="fa fa-refresh"></i> Clear
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-datatable pb-2">
                        <table class="dt-general table">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th>Tanggal</th>
                                    <th>User</th>
                                    <th>Menu</th>
                                    <th>Action</th>
                                    <th>Log</th>
                                    <th>Tag</th>
                                    <th>File</th>
                                    <th class="text-center" width="2%">Options</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade text-start" id="modal-upload" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="">Upload File</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <label class="form-label" for="">File</label>
                                    <input type="file" name="file" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
    <script>
        const datatableUrl       = InitDatatable("{{ route($routes['data']) }}", 
                                                  ["created_at_formatted","user_name","menu", "action", "log", "tag_formatted",
                                                   "file_formatted","options"],
                                                  [{ targets: 1, orderable: false }, { targets: 2, orderable: false },
                                                   { targets: 2, orderable: false }, { targets: 3, orderable: false },
                                                   { targets: 4, orderable: false }, { targets: 5, orderable: false },
                                                   { targets: 5, orderable: false }, { targets: 6, orderable: false },
                                                   { targets: 7, orderable: false }],
                                                   ["tanggal_mulai_akhir"]);
        const OpenModalUpload    = (url) => {
            $("#modal-upload").find("form").attr("action", url);
            $("#modal-upload").modal("show");
        };
    </script>
@endpush