<div class="btn-group">
    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
        Options
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        @can('Upload User Log')
            <a class="dropdown-item" href="#" onclick="OpenModalUpload('{{ route('user-log.upload', $model->id) }}')">Uplad File</a>
        @endcan
    </div>
</div>