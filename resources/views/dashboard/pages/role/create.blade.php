@extends('dashboard.layouts.app')

@push('dashboard.css')
<link rel="stylesheet" href="{{ asset('assets/dashboard/vendors/css/extensions/jstree.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/dashboard/css/plugins/extensions/ext-component-tree.min.css') }}">
@endpush

@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{ $sub_title }}</h4>
                    </div>
                    <div class="card-body mt-2">
                        {!! Form::open(['route' => $routes['store'], 'method' => 'POST','class' => 'form form-horizontal','enctype' => 'multipart/form-data']) !!}
                            @include($form)                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary me-1 float-end" onclick="SubmitForm(this, event)">Submit</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
    <script src="{{ asset('assets/dashboard/vendors/js/extensions/jstree.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            @foreach (collect($items["list_roles"])->chunk(3) as $key => $itemChunk)
                $("#events-{{ $key }}").jstree({
                    core: {
                        "themes" : {
                            "variant" : "large"
                        },
                        data: {!! json_encode($itemChunk->values()) !!},
                    },
                    plugins: ['types', 'checkbox', 'wholerow'],
                    types: {
                        default: { icon: 'fas fa-bolt' },
                        @foreach ($itemChunk->values() as $node)
                            {{ $node['type'] }}: { icon: '{{ $node["icon"] }}' },
                        @endforeach
                    }
                });
            @endforeach
        });

        function extractDataAndCreateJSON(selectedNodes) {
            const jsonData = [];

            selectedNodes.forEach(node => {
                if (node.original) {
                    const data = {
                        value: node.original.value,
                        parent: node.parent,
                        label: node.text,
                    };

                    jsonData.push(data);
                }
            });
            jsonAll     = jsonData;
            return jsonData;
        }

        const SubmitForm                    = (el, e) => {
            e.preventDefault();            
            var jsonData                    = [];
            @foreach (collect($items["list_roles"])->chunk(3) as $key => $itemChunk)
                var selectedNodes = $('#events-{{ $key }}').jstree("get_bottom_checked", true);
                jsonData.push(extractDataAndCreateJSON(selectedNodes));
            @endforeach

            $("input[name='permission_id']").val(JSON.stringify(jsonData));
            $(el).closest("form").submit();
        };
    </script>
@endpush