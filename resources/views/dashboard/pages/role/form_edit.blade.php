<form id="" class="row" method="POST" action="{{ route('role.update', $item->id) }}">
    @method("PUT")
    @csrf
    <div class="col-12 mb-2">
        <label class="form-label" for="modalRoleName">Role Name</label>
        <input type="text" id="modalRoleName" name="name" class="form-control" placeholder="Enter role name" tabindex="-1" data-msg="Please enter role name" value="{{ $item->name }}" />
    </div>
    <div class="col-12 mb-2">
        <label class="form-label" for="modalRoleName">Perusahaan</label>
        {!! Form::select("perusahaan_id", $items["list_perusahaan"], $item->roleRelation->perusahaan_id ?? null, ["class" => "form-control select2 select-perusahaan", "placeholder" => "Pilih Perusahaan","onchange" => "ChangePerusahaan(this)"]) !!}
    </div>
    <div class="col-12 mb-2">
        <label class="form-label" for="modalRoleName">Departemen</label>
        {!! Form::select("departemen_id", $items["list_departemen"] ?? [], $item->roleRelation->departemen_id ?? null, ["class" => "form-control select2 select-departemen", "placeholder" => "Pilih Departemen"]) !!}
    </div>
    <div class="col-12">
        <h4 class="mt-2 pt-50">Role Permissions</h4>
        <div class="table-responsive">
            <table class="table table-flush-spacing">
                <tbody>
                    @foreach ($items["list_permission"]->chunk(4) as $permissions)
                        <tr>
                            @foreach ($permissions as $permission_id => $permission_name)
                                <td nowrap>
                                    <input class="form-check-input" type="checkbox" {{ $item->permissions->pluck("name","id")->has($permission_id) ? "checked" : "" }} id="{{ $permission_name }}" name="permission_id[]" value="{{ $permission_id }}" />
                                    <label class="form-check-label" for="{{ $permission_name }}"> {{ $permission_name }} </label>
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-12 text-center mt-2">
        <button type="submit" class="btn btn-primary me-1">Submit</button>
        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
            Discard
        </button>
    </div>
</form>