<div class="row">
    <div class="col-12 mb-2">
        <label class="form-label" for="modalRoleName">Nama</label>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama']) !!}
    </div>

    <div class="col-12 mb-2">
        <label class="form-label mb-2" for="modalRoleName">Permissions</label>
        <div class="row">
            @foreach (collect($items["list_roles"])->chunk(3) as $key => $itemChunk)
                <div class="col-4 mb-2">
                    <div class="mb-1 row">
                        <div class="col-sm-12">
                            <div id="events-{{ $key }}"></div>
                        </div>
                    </div>
                </div>
            @endforeach
            {!! Form::hidden('permission_id', null, ['class' => 'form-control', 'placeholder' => 'Events']) !!}
        </div>
    </div>
</div>
