@extends('dashboard.layouts.app')

@push('dashboard.css')
    
@endpush


@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            @foreach ($items["items"] as $item)
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <span>Total {{ $item->users->count() }} users 
                                </span>
                                <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                    @foreach ($item->users->take(5) as $user)
                                        <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top" title="{{ $user->name }}" class="avatar avatar-sm pull-up">
                                            <img class="rounded-circle" src="{{ asset('assets/dashboard/images/avatars/no_profile.png') }}" alt="Avatar" />
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="d-flex justify-content-between align-items-end mt-1 pt-25">
                                <div class="role-heading">
                                    <h4 class="fw-bolder">{{ ucwords($item->name) }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="d-flex align-items-end justify-content-center h-100">
                                <img src="{{ asset('assets/dashboard/images/illustration/role-view.png') }}" class="img-fluid mt-2" alt="Image" width="85" />
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body text-sm-end text-center ps-sm-0">
                                <a href="{{ route($routes['create']) }}" class="stretched-link text-nowrap add-new-role">
                                    <span class="btn btn-primary mb-1">Add New Role</span>
                                </a>
                                <p class="mb-0">Add role, if it does not exist</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')

@endpush