@extends('dashboard.layouts.app')

@push('dashboard.css')
    
@endpush


@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{ $sub_title }}</h4>
                    </div>
                    <div class="card-body mt-2">
                        {!! Form::open(['route' => $routes['store'], 'method' => 'POST','class' => 'form form-horizontal','enctype' => 'multipart/form-data']) !!}
                        
                            @include($form)
                            
                            <div class="row">
                                <div class="col-sm-9 offset-sm-3">
                                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
    
@endpush