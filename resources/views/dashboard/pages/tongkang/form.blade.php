<div class="row">
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Nama Pemilik</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('nama_pemilik', null, ['class' => 'form-control', 'placeholder' => 'Nama Pemilik']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Nama Tongkang</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Tongkang']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Ukuran (PxLxD)</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('ukuran', null, ['class' => 'form-control', 'placeholder' => 'Ukuran (PxLxD)']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Tonase Kotor (GT)</label>
            </div>
            <div class="col-sm-9">
                {!! Form::number('tonase_kotor', null, ['class' => 'form-control', 'placeholder' => 'Tonase Kotor (GT)']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Tanda Pendaftaran</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('tanda_pendaftaran', null, ['class' => 'form-control', 'placeholder' => 'Tanda Pendaftaran']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Tanggal Surat Laut</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('tanggal_surat_laut', isset($item) ? $item->tanggal_surat_laut_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Tanggal Surat Laut']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Tanggal Ukur</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('tanggal_ukur', isset($item) ? $item->tanggal_ukur_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Tanggal Ukur']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Masa Berlaku Konstruksi</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('masa_berlaku_konstruksi', isset($item) ? $item->masa_berlaku_konstruksi_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Masa Berlaku Konstruksi']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Masa Berlaku Lambung</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('masa_berlaku_lambung', isset($item) ? $item->masa_berlaku_lambung_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Masa Berlaku Lambung']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Masa Berlaku Garis Muat</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('masa_berlaku_garis_muat', isset($item) ? $item->masa_berlaku_garis_muat_formatted : null, ['class' => 'form-control flatpicker', 'placeholder' => 'Masa Berlaku Garis Muat']) !!}
            </div>
        </div>
    </div>
</div>