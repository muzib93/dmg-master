@extends('dashboard.layouts.app')

@push('dashboard.css')
    
@endpush


@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{ $sub_title }}</h4>
                        <a href="{{ route($routes['create']) }}" class="btn btn-primary">
                            <i data-feather="plus"></i> Add
                        </a>
                    </div>
                    <div class="card-datatable pb-2">
                        <table class="dt-general table">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th class="text-center" width="2%">Options</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
    <script>
        const datatableUrl       = InitDatatable("{{ route($routes['data']) }}", 
                                                  ["name", "email", "role_name","options"],
                                                  [{ targets: 3, orderable: false }]);
    </script>
@endpush