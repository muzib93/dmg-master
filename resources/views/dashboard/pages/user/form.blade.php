<div class="row">
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Name</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Company</label>
            </div>
            <div class="col-sm-9">
                {!! Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'Company']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Email</label>
            </div>
            <div class="col-sm-9">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Role</label>
            </div>
            <div class="col-sm-9">
                {!! Form::select('role_id', $items["list_role"], isset($item) ? $item->roles->first()->id : null, ['class' => 'form-control', 'placeholder' => 'Choose Role']) !!}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-1 row">
            <div class="col-sm-3">
                <label class="col-form-label">Password</label>
            </div>
            <div class="col-sm-9">
                {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => 'Password']) !!}
            </div>
        </div>
    </div>
</div>