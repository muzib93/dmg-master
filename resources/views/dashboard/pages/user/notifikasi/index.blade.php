@extends('dashboard.layouts.app')

@push('dashboard.css')
    
@endpush


@section('dashboard.content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{ $sub_title }}</h4>
                    </div>
                    <div class="card-datatable pb-2">
                        <table class="dt-general table">
                            <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th>Title</th>
                                    <th>Sub Title</th>
                                    <th>Tanggal Dibaca</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse (auth()->user()->notifications as $notification)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $notification->data["title"] }}</td>
                                        <td>{{ $notification->data["sub_title"] }}</td>
                                        <td>{{ $notification->read_at }}</td>
                                        <td>
                                            <a href="{{ $notification->data["action_url"] }}" target="_blank" class="btn btn-sm btn-primary">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="5">Notifikasi Kosong.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.js')
    <script>
        $(document).ready(function () {
            $('.dt-general').DataTable({
                paging: true,
                searching: true,
                ordering: false,
            });
        });
    </script>
@endpush