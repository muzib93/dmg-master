<div class="overlay-spinner-class">
    <div class="cv-spinner">
        <span class="spinner"></span>
    </div>
</div>

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT &copy; {{ date("Y") }}<a class="ms-25" href="#" target="_blank">{{ env("APP_NAME") }}</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span><span class="float-md-end d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span></p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>

<script src="{{ asset('assets/dashboard/vendors/js/vendors.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/extensions/toastr.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/extensions/moment.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/responsive.bootstrap5.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/core/app-menu.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/core/app.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/scripts/pages/app-invoice-list.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{ asset('assets/dashboard/js/custom/datatable.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/custom/swal.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/forms/cleave/cleave.min.js') }}"></script>

<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/dashboard/vendors/js/tables/datatable/jszip.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    });

    const SUCCESS_MESSAGE = "{{ session()->get('success') ?? "undefined"  }}";
    const baseUrl         = "{{ url('/') }}";
    const _csrf_token     = "{{ csrf_token() }}";
    
    if (SUCCESS_MESSAGE != "undefined") {
        SwalPopUp("Berhasil", SUCCESS_MESSAGE, "success");
    }


    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    });

    $(document).ready(function () {
        $(".dataTables_scrollBody").css("min-height", "500px");
    });

    $(".select2").each(function () {
        var $this = $(this);
        $this.wrap('<div class="position-relative"></div>');
        $this.select2({
            dropdownAutoWidth: true,
            width: '100%',
            dropdownParent: $this.parent()
        });
    });
    // $(".select2-selection").find(".select2-selection__arrow").remove();

    $(".flatpicker-time").flatpickr({
        dateFormat: 'd-m-Y H:i',
        enableTime: true,
        time_24hr: true,
    });

    $(".flatpicker").flatpickr({
        dateFormat: 'd-m-Y'
    });

    $(".flatpicker-multiple").flatpickr({
        dateFormat: 'd-m-Y',
        mode: "multiple",
    });

    $(".flatpicker-range").flatpickr({
        dateFormat: 'd-m-Y',
        mode: 'range',
    });

    $(".flatpicker-only-time").flatpickr({
        dateFormat: 'H:i',
        enableTime: true,
        time_24hr: true,
        noCalendar: true
    });

    if ($(".numeral-mask").length  > 0) {
        new Cleave($(".numeral-mask"), {
            numeral: true,
            numeralDecimalMark: '.',
            delimiter: ','
        });
    }

    const ClickNotificationBell             = () => {
        $.ajax({
            type: "GET",
            url: "{{ route('ajax.mark-all-as-read') }}",
            data: {},
            dataType: "json",
            success: function (response) {
                
            }
        });
    }

    const InitSelect2                       = (el) => {
        $(el).each(function () {
            var $this = $(this);
            $this.wrap('<div class="position-relative"></div>');
            $this.select2({
                dropdownAutoWidth: true,
                width: '100%',
                dropdownParent: $this.parent()
            });
        });
    }

    const formattedInputs = document.querySelectorAll('.input-nominal');
    formattedInputs.forEach(function(input) {
        new Cleave(input, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
    });

    const AjaxReload            = () => {
        $(document).on({
            ajaxStart: function() {
                $(".overlay-spinner-class").fadeIn(300)
            },
            ajaxStop: function() {
                setTimeout(function() {
                    $(".overlay-spinner-class").fadeOut(300);
                }, 500);
            }
        });
    };

    const UploadFile = (el) => {
        const file = $(el).prop('files')[0];
        const tipeFile = $(el).data('file');
        const url = $(el).data('url');

        let formData = new FormData();
        formData.append('file', file);
        formData.append('field', tipeFile);
        formData.append('_token', "{{ csrf_token() }}");

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            data: formData,
        })
        .done(function(response) {
            if (response.status) {
                $(el).closest(`#${response.data.parent_div}`).html(response.data.view);
                SwalPopUp('Berhasil', 'Berhasil Upload File', "success");
            }
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    };

    const RemoveFile        = (url) => {
        Swal.fire({
            title: 'Hapus File?',
            text: "Apakah anda yakin untuk hapus file ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ms-1'
            },
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                window.location.href = url;
            }
        });
    };

</script>