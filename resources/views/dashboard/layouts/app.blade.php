<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

    <head>

        @include('dashboard.layouts.header')
        
        @livewireStyles()
        @stack('dashboard.css')

    </head>

    <body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">

        @include('dashboard.layouts.navbar')

        @include('dashboard.layouts.sidebar')

        <div class="app-content content">
            <div class="content-overlay"></div>
            
            <div class="header-navbar-shadow"></div>

            <div class="content-wrapper container-xxl p-0">

                {{-- @include('dashboard.layouts.title') --}}

                @include('dashboard.layouts.alert')

                @yield('dashboard.content')

            </div>
        </div>

        @include('dashboard.layouts.footer')

        @stack('dashboard.js')

    </body>

</html>