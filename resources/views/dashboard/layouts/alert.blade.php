@if (isset($errors) && $errors->isNotEmpty())
    <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Failed</h4>
        <div class="alert-body">
            @foreach ($errors->all() as $error) 
                <i data-feather="info" class="me-50"></i><span>{{ $error }}</span>
                <br/>
            @endforeach
        </div>
    </div>
@endif