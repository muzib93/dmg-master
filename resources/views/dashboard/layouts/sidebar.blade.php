<div class="horizontal-menu-wrapper">
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-shadow menu-border container-xxl" role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
        <div class="navbar-header mb-1">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item me-auto text-center">
                    {{-- <a class="navbar-brand" href="{{ route('dashboard.index') }}">
                        <img src="https://muzib93.vercel.app/static/media/me1.6b5b5853a4e253b18c40.png" alt="" style="width: 26%;">
                    </a> --}}
                </li>
            </ul>
        </div>    
        <div class="shadow-bottom"></div>
        
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                @foreach ($topBarMenu as $keySection => $sectionItems)
                    @foreach ($sectionItems["menu"] as $menuItem)
                        @if (!array_key_exists("has-child", $menuItem))
                            <li class="dropdown nav-item {{ isTopBarParentActive($menuItem, false) }}" data-menu="dropdown">
                                <a class="dropdown-item d-flex align-items-center" href="{{ $menuItem["route"] != null ? route($menuItem["route"]) : "#" }}">
                                    <i class="fa {{ $menuItem["icon"] }}"></i>
                                    <span data-i18n="{{ $menuItem["text"] }}">{{ $menuItem["text"] }}</span>
                                </a>
                            </li>
                        @else
                            <li class="dropdown nav-item {{ isTopBarParentActive($menuItem, true) }}" data-menu="dropdown">
                                <a class="dropdown-toggle nav-link d-flex align-items-center" href="#" data-bs-toggle="dropdown">
                                    <i class="fa {{ $menuItem["icon"] }}"></i>
                                    <span data-i18n="{{ $menuItem["text"] }}">{{ $menuItem["text"] }}</span>
                                </a>
                                <ul class="dropdown-menu" data-bs-popper="none">
                                    @foreach ($menuItem["child"] as $menuItemChild)
                                        @if (array_key_exists("has-child", $menuItemChild) && $menuItemChild["has-child"])
                                            <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                                                <a class="dropdown-item d-flex align-items-center dropdown-toggle" href="#" data-bs-toggle="dropdown" data-i18n="circle">
                                                    <i class="fa {{ $menuItemChild['icon'] }}"></i>
                                                    <span data-i18n="{{ $menuItemChild['text'] }}">{{ $menuItemChild['text'] }}</span>
                                                </a>
                                                <ul class="dropdown-menu" data-bs-popper="none">
                                                    @foreach ($menuItemChild['child'] as $menuItemGrandChild)
                                                        <li data-menu="" class="{{ isTopBarChildActive($menuItemGrandChild["route"]) }}">
                                                            <a class="dropdown-item d-flex align-items-center" href="{{ $menuItemGrandChild["route"] != null ? route($menuItemGrandChild["route"]) : "#" }}" data-bs-toggle="" data-i18n="{{ $menuItemGrandChild["text"] }}">
                                                                <i class="fa {{ $menuItemGrandChild["icon"] }}"></i>
                                                                <span data-i18n="{{ $menuItemGrandChild["text"] }}">{{ $menuItemGrandChild["text"] }}</span>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @else
                                            <li data-menu="" class="{{ isTopBarChildActive($menuItemChild["route"]) }}">
                                                <a class="dropdown-item d-flex align-items-center" href="{{ $menuItemChild["route"] != null ? route($menuItemChild["route"]) : "#" }}" data-bs-toggle="" data-i18n="{{ $menuItemChild["text"] }}">
                                                    <i class="fa {{ $menuItemChild["icon"] }}"></i>
                                                    <span data-i18n="{{ $menuItemChild["text"] }}">{{ $menuItemChild["text"] }}</span>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endforeach
                @endforeach
            </ul>
        </div>
    </div>
</div>