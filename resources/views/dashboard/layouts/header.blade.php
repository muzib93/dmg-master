<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<title>{{ env("APP_NAME") }}</title>
<link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">
<link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/vendors/css/charts/apexcharts.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/vendors/css/extensions/toastr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/vendors/css/tables/datatable/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/vendors/css/tables/datatable/responsive.bootstrap5.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/vendors/css/forms/select/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/colors.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/components.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/themes/dark-layout.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/themes/bordered-layout.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/themes/semi-dark-layout.css') }}">

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/core/menu/menu-types/horizontal-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/plugins/charts/chart-apex.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/plugins/extensions/ext-component-toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/vendors/css/datatables-buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/pages/app-invoice-list.css') }}">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!-- END: Custom CSS-->

<style>
    .mz-select {
        padding: 0.6rem 0.5rem; 
        line-height: 1.45;
        color: #6e6b7b; 
        background-color: #fff; 
        background-clip: padding-box; 
        border: 1px solid #d8d6de; 
        border-radius: 0.357rem;
    }

    .overlay-spinner-class {	
        position: fixed;
        top: 0;
        z-index: 1063;
        width: 100%;
        height:100%;
        display: none;
        background: rgba(0,0,0,0.6);
    }
    .cv-spinner {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;  
    }
    .spinner {
        width: 40px;
        height: 40px;
        border: 4px #ddd solid;
        border-top: 4px #2e93e6 solid;
        border-radius: 50%;
        animation: sp-anime 0.8s infinite linear;
    }
    @keyframes sp-anime {
        100% { 
            transform: rotate(360deg); 
        }
    }
    .is-hide{
        display:none;
    }

    .btn-group-xs>.btn, .btn-xs {
        padding: 1px 5px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;
    }
    
    .file-upload {
        position: absolute;
        left: 0;
        opacity: 0;
        top: 0;
        bottom: 0;
        width: 100%;
        cursor: pointer;
    }

    .box-file {
        min-height: 180px;
    }
    .flex-center {
        justify-content: center;
        align-items: center;
    }

    .bg-light-primary {
        background-color: #f1faff!important;
    }
    .p-8 {
        padding: 2rem!important;
    }
    .h-100 {
        height: 100%!important;
    }

    .border-dashed {
        border-style: dashed!important;
        border-color: #e4e6ef;
    }

    .text-gray-400 {
        color: #b5b5c3!important;
    }

    .box-shadow-file {
        border: 3px solid #fff;
        box-shadow: 0 0.5rem 1.5rem 0.5rem rgb(0 0 0 / 8%);
        min-height: 180px;
    }
    .text-center {
        text-align: center!important;
    }
    .p-8 {
        padding: 2rem!important;
    }
    .justify-content-center {
        justify-content: center!important;
    }
    .flex-column {
        flex-direction: column!important;
    }
    .d-flex {
        display: flex!important;
    }

    .image-input:not(.image-input-empty) {
        background-image: none!important;
    }

    .image-input {
        position: relative;
        display: inline-block;
        border-radius: 0.475rem;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .btn.btn-icon:not(.btn-outline):not(.btn-dashed):not(.border-hover):not(.border-active):not(.btn-flush) {
        border: 0;
    }

    .btn.btn-icon.btn-circle {
        border-radius: 50%;
    }
    .image-input .remove-button {
        position: absolute;
        right: -10px;
        bottom: -5px;
    }
    .btn.btn-icon {
        display: inline-flex;
        align-items: center;
        justify-content: center;
        padding: 0;
        height: calc(1.5em + 1.5rem + 2px);
        width: calc(1.5em + 1.5rem + 2px);
    }
</style>