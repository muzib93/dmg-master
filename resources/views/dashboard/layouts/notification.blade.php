<li class="nav-item dropdown dropdown-notification me-25">
    <a class="nav-link" href="#" data-bs-toggle="dropdown" onclick="ClickNotificationBell()">
        <i class="ficon" data-feather="bell"></i>
        <span class="badge rounded-pill bg-danger badge-up">{{ auth()->user()->unreadNotifications->count() }}</span>
    </a>
    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
        <li class="dropdown-menu-header">
            <div class="dropdown-header d-flex">
                <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                <div class="badge rounded-pill badge-light-primary">{{ auth()->user()->unreadNotifications->count() }} new</div>
            </div>
        </li>
        <li class="scrollable-container media-list">
            @forelse (auth()->user()->unreadNotifications as $unreadNotification)
                <a class="d-flex" href="{{ $unreadNotification->data["action_url"] ?? "#" }}">
                    <div class="list-item d-flex align-items-start">
                        <div class="me-1">
                            <div class="avatar bg-light-success">
                                <div class="avatar-content"><i class="fa fa-bell"></i></div>
                            </div>
                        </div>
                        <div class="list-item-body flex-grow-1">
                            <p class="media-heading">
                                <span class="fw-bolder">{{ $unreadNotification->data["title"] }}</span>
                            </p>
                            <small class="notification-text"> {{ $unreadNotification->data["sub_title"] }}</small>
                        </div>
                    </div>
                </a>
            @empty
                <div class="list-item">
                    <span class="w-100 mb-0 text-center">
                        <i>Notification is empty.</i>
                    </span>
                </div>
            @endforelse
            @can('Read Notifikasi')
                <div class="list-item">
                    <span class="w-100 mb-0 text-center">
                        <a href="{{ route("notifikasi.index") }}" target="_blank" class="btn btn-primary btn-sm">Lihat semua notifikasi</a>
                    </span>
                </div>
            @endcan
        </li>
    </ul>
</li>