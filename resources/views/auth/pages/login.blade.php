@extends('auth.layouts.app')

@section('auth.content')
    <div class="card mb-0">
        <div class="card-body">
            {{-- <a href="#" class="brand-logo">
                <img src="https://muzib93.vercel.app/static/media/me1.6b5b5853a4e253b18c40.png" alt="" style="width: 70%;">
            </a> --}}

            <h4 class="card-title mb-1">Welcome to {{ env("APP_NAME") }}! 👋</h4>
            <p class="card-text mb-2">Please sign-in to your account and start the adventure</p>

            <form class="auth-login-form mt-2" action="{{ route('login') }}" method="POST">
                @csrf
                <div class="mb-1">
                    <label for="login-email" class="form-label">Email</label>
                    <input type="text" class="form-control" id="login-email" name="email" placeholder="john@example.com" aria-describedby="login-email" tabindex="1" autofocus />
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-1">
                    <div class="d-flex justify-content-between">
                        <label class="form-label" for="login-password">Password</label>
                    </div>
                    <div class="input-group input-group-merge form-password-toggle">
                        <input type="password" class="form-control form-control-merge" id="login-password" name="password" tabindex="2" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="login-password" />
                        <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                    </div>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mb-1">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="remember-me" tabindex="3" />
                    </div>
                </div>
                <button class="btn btn-primary w-100" tabindex="4">Sign in</button>
            </form>
            <p class="text-center mt-2">
                <span>New on our platform?</span>
                <a href="{{ route('register') }}">
                    <span>Create an account</span>
                </a>
            </p>
        </div>
    </div>
@endsection