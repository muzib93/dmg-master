<?php

namespace Helpers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;

class ClientHelper {

    protected $client;
    protected $url;
    protected $data     = [];

    public function __construct(Client $client)
    {
        $this->client               = $client;
    }

    protected function setUrl($url): void
    {
        $this->url                  = $url;
    }

    protected function setHeaders(array $headers): void
    {
        $this->data["headers"]  = array_merge(["accept" => "application/json"], $headers);
    }

    protected function setBody(array $attributes): void
    {
        if (array_key_exists("body", $attributes) && !empty($attributes["body"])) {
            $this->data["body"]             = $attributes["body"];
        }
    }

    protected function setParams(array $attributes): void
    {
        if (array_key_exists("params", $attributes) && !empty($attributes["params"])) {
            $this->url              = $this->url . "?" . http_build_query($attributes["params"]);   
        }
    }

    protected function sendRequest(String $type)
    {
        try {
            
            $response   = $this->client->request($type, $this->url, $this->data);
            $parsed     = json_decode($response->getBody()->getContents(), true);

            Log::debug('ClientHelper Success', [
                'url'           => $this->url,
                'data'          => $this->data, 
                'response'      => $parsed,
            ]);

            return $parsed;

        } catch (ClientException $th) {
            
            Log::warning('ClientHelper Fail', [
                "url"               => $this->url,
                "data"              => $this->data,
                "response"          => $th->getMessage(),
            ]);
        }

    }

    public function post(String $url, array $headers, array $attributes)
    {
        $this->setUrl($url);
        $this->setHeaders($headers);
        $this->setBody($attributes);
        $this->setParams($attributes);

        $response           = $this->sendRequest("POST");

        return $response;
    }

    public function get(String $url, array $headers, array $attributes)
    {
        $this->setUrl($url);
        $this->setHeaders($headers);
        $this->setBody($attributes);
        $this->setParams($attributes);
        
        $response           = $this->sendRequest("GET");

        return $response;
    }
}