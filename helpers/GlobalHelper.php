<?php

use Carbon\Carbon;
use Helpers\LogHelper;
use App\Models\UserLog;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

if (!function_exists('isTopBarParentActive')) {

    function isTopBarParentActive(array $menuItem, bool $hasChild): String
    {
        if ($hasChild) {
            $routeName      = transformRouteName(request()->route()->getName());
            return in_array($routeName, array_column($menuItem["child"], "route")) ? "sidebar-group-active open" : "";
        }

        return $menuItem["route"] == request()->route()->getName() ? "active" : "";
    }

}

if (!function_exists('isTopBarChildActive')) {

    function isTopBarChildActive($menuRouteName): String
    {
        return $menuRouteName == transformRouteName(request()->route()->getName()) ? "active" : "";
    }

}

if (!function_exists('transformRouteName')) {

    function transformRouteName(String $routeName): String
    {
        $explode        = explode(".", $routeName);

        return $explode[0] . '.index';
    }

}

if (!function_exists('formatDate')) {

    function formatDate($from, $to, $date)
    {
        if (!empty($date)) {
            Carbon::setLocale('id');
            return Carbon::createFromFormat($from, $date)->translatedFormat($to);
        }
    }

}

if (!function_exists('makeResponse')) {

    function makeResponse($status = true, $message = 'success', $data = [], $code = 200)
    {
        return [
            'code'              => $code,
            'status'            => $status,
            'message'           => $message,
            'data'              => $data,
        ];
    }

}

if (!function_exists('responseApi')) {

    /**
     * responseApi
     *
     * @param  mixed $statusCode
     * 0 = SUCCESS
     * 1 = ERROR
     * 2 = UNAUTHORIZED
     * 3 = NOT FOUND
     * 4 = BAD REQUEST
     * 5 = BALANCE NOT ENOUGH
     * @param  mixed $statusMessage
     * @param  mixed $data
     * @param  mixed $code
     * @return array
     */
    function responseApi($statusCode = 0, $statusMessage = 'success', $data = [], $code = 200): array
    {
        return [
            "response"              => [
                "statusCode"                => $statusCode,
                "statusMessage"             => $statusMessage,
                "data"                      => $data,
            ],
            "code"                  => $code
        ];
    }

}

if (!function_exists('navBarAttributes')) {

    function navBarAttributes(): array
    {
        $user               = auth()->user();
        $attributes         = [];

        if (!empty($user->roles->first())) {
            $attributes     = [
                "route_logo"            => "dashboard.index",
                "navbar_foto"           => asset(config('constants.dashboard.assets') . 'images/avatars/no_profile.png'),
            ];
        }

        if (!empty($user->biodataPendaftar)) {
            $attributes     = [
                "route_logo"            => "pmb.dashboard.index",
                "navbar_foto"           => $user->biodataPendaftar->foto_storage,
            ];
        }

        return $attributes;
    }

}

if (!function_exists("labelHelper")) {

    function labelHelper($title = "", $class = "", $icon = "", $toolTip = ""): String
    {
        $html       =  "<span class='" . $class . "' style='margin-bottom: 5px;'>";

        if (!empty($toolTip)) {
            $html   = "<span title='" . $toolTip . "' data-toggle='tooltip' data-placement='top' class='" . $class . "' style='margin-bottom: 5px;'>";
        }

        if (!empty($icon)) {
            $html   .= "<i data-feather='" . $icon . "'></i> ";
        }

        $html   .= $title;
        $html   .= "</span>";

        return $html;
    }

}

if (!function_exists("rootFolder")) {

    function rootFolder(): String
    {
        return str_replace("/public", "", $_SERVER['DOCUMENT_ROOT']);
    }

}

if (!function_exists("jsonFileToCollection")) {

    function jsonFileToCollection(String $path): Collection
    {
        $json       = file_get_contents(rootFolder() . $path);

        return collect(json_decode($json, true));
    }

}

if (!function_exists("rangeDateToArray")) {

    function rangeDateToArray(String $startDate, String $endDate, String $format): array
    {
        $endDate    = Carbon::parse($endDate)->addDay()->format("Y-m-d");

        $period     = new DatePeriod(
            new DateTime($startDate),
            new DateInterval('P1D'),
            new DateTime($endDate)
        );

        $dates   = [];

        foreach ($period as $key => $value) {
            $dates[]    = $value->format($format);
        }

        return $dates;
    }

}

if (!function_exists('saveFile')) {
    
    function saveFile($file, $lokasi)
    {
    	$file_name 	= null;

    	if (!empty($file)) {
    		$file       = $file;

            if (!File::isDirectory(storage_path().'/app/public/'.$lokasi)) {
                File::makeDirectory(storage_path().'/app/public/'.$lokasi, 0777, true);
            }

            if(substr($file->getMimeType(), 0, 5) == 'image') {
	            if (!empty($file)) {
	                $extension  = !empty($file->getClientOriginalExtension()) ? $file->getClientOriginalExtension() : $file->guessExtension();
                    // dd($extension);
	                $filename   = md5($file->getFilename()).'.'.$extension;
	                $location   = storage_path().'/app/public/'.$lokasi.'/'.$filename;
	                Image::make($file)->save($location);

	                $file_name = $filename;
	            }
            }else {
            	if (!empty($file)) {
	            	$extension  = !empty($file->getClientOriginalExtension()) ? $file->getClientOriginalExtension() : $file->guessExtension();
	            	$filename   = md5($file->getFilename()).'.'.$extension;

	            	$file->storeAs('public/'.$lokasi, $filename);

	            	$file_name = $filename;
	            }
            }
    	}

    	return $file_name;
    }

}

if (!function_exists("saveBase64ToImage")) {
    
    function saveBase64ToImage($base64_string, $dynamic_folder, $nip = "") 
	{
		$format			= "data:image/png;base64,";
		$string_format 	= $format.$base64_string; 
		$storage_path 	= storage_path().'/app/public/'.$dynamic_folder.'/';

		if (!File::isDirectory($storage_path)) {
		    File::makeDirectory($storage_path, 0777, true);
		}
		$ext 			= explode('/', mime_content_type($string_format))[1];
		$filename 		= time()."".md5(rand(1,100))."".$nip.".".$ext;
		$location   	= $storage_path.$filename;
	    Image::make(file_get_contents($string_format))->save($location);

	    return $filename;
	}

}

if (!function_exists("saveBase64ToFile")) {
    
    function saveBase64ToFile($base64_string, $dynamic_folder, $nip = "") 
	{ 
		$storage_path 	= storage_path().'/app/public/'.$dynamic_folder.'/';

		if (!File::isDirectory($storage_path)) {
		    File::makeDirectory($storage_path, 0777, true);
		}

        $decode     = base64_decode($base64_string);
        $fileName   = md5(uniqid(rand(), true)) . "." . getMimeTypeBase64($decode);
        
        Storage::disk('public')->put($dynamic_folder . "/" . $fileName, $decode);

	    return $fileName;
	}

}

if (!function_exists("getMimeTypeBase64")) {
    
    function getMimeTypeBase64($decode)
    {
        $f          = finfo_open();
        $mime_type  = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);

        return str_replace("application/", "", $mime_type);
    }

}

if (!function_exists('deleteFile')) {

    function deleteFile($filename, $lokasi)
    {
        $path = storage_path().'/app/public/'.$lokasi.'/';
        return File::delete($path.$filename);
    }


}


if (!function_exists('indonesianDayName')) {

	function indonesianDayName($name)
	{
		$indonesia_name 	= '';

		switch ($name) {
			case 'Sunday':
				$indonesia_name 	= 'Minggu';
				break;
			case 'Monday':
				$indonesia_name 	= 'Senin';
				break;
			case 'Tuesday':
				$indonesia_name 	= 'Selasa';
				break;
			case 'Wednesday':
				$indonesia_name 	= 'Rabu';
				break;
			case 'Thursday':
				$indonesia_name 	= 'Kamis';
				break;
			case 'Friday':
				$indonesia_name 	= 'Jumat';
				break;
			case 'Saturday':
				$indonesia_name 	= 'Sabtu';
				break;
		}

		return $indonesia_name;
	}

}

if (!function_exists("getTimeWord")) {

    function getTimeWord() : String
    {
		$now 			= Carbon::now()->format('H:i');

		$status 		= '';
		if (strtotime($now) >= strtotime('00:01') && strtotime($now) <= strtotime('11:00')) {
			$status 	= 'Pagi';
		}else if (strtotime($now) >= strtotime('11:01') && strtotime($now) <= strtotime('14:59')) {
			$status 	= 'Siang';
		}else if (strtotime($now) >= strtotime('15:00') && strtotime($now) <= strtotime('18:59')) {
			$status 	= 'Sore';
		}else if (strtotime($now) >= strtotime('19:00') && strtotime($now) <= strtotime('23:59')) {
			$status 	= 'Malam';
		}

		return $status;
    }

}

if (!function_exists("diffBetweenHour")) {

    function diffBetweenHour($firstDate, $secondDate)
    {
        if (empty($firstDate) || empty($secondDate)) {
            return null;
        }

        $start  = Carbon::parse($firstDate);
        $end    = Carbon::parse($secondDate);

        return $start->diff($end)->format("%H Jam %i Menit");
    }

}

if (!function_exists("diffBetweenHour")) {

    function diffBetweenHour($firstDate, $secondDate)
    {
        if (empty($firstDate) || empty($secondDate)) {
            return null;
        }

        $start  = Carbon::parse($firstDate);
        $end    = Carbon::parse($secondDate);

        return $start->diff($end)->format("%H Jam %i Menit");
    }

}

if (!function_exists("indonesianMonthName")) {

    function indonesianMonthName($monthNameEng)
    {
        $monthNameInd 	    = '';

		switch ($monthNameEng) {
			case 'January':
				$monthNameInd 	= 'Januari';
				break;
			case 'February':
				$monthNameInd 	= 'Februari';
				break;
			case 'March':
				$monthNameInd 	= 'Maret';
				break;
			case 'April':
				$monthNameInd 	= 'April';
				break;
			case 'May':
				$monthNameInd 	= 'Mei';
				break;
			case 'June':
				$monthNameInd 	= 'Juni';
				break;
			case 'July':
				$monthNameInd 	= 'Juli';
				break;
            case 'August':
                $monthNameInd 	= 'Agustus';
                break;
            case 'September':
                $monthNameInd 	= 'September';
                break;
            case 'October':
                $monthNameInd 	= 'Oktober';
                break;
            case 'November':
                $monthNameInd 	= 'November';
                break;
            case 'December':
                $monthNameInd 	= 'Desember';
                break;
		}

		return $monthNameInd;
    }

}

if (!function_exists('makeTag')) {

	function makeTag($model, $tag)
	{
		$arr_tag 			= [];
		foreach ($tag as $key => $item_tag) {
			if (is_object($model) && isset($model->{$item_tag})) {
				$arr_tag[$item_tag] 	=  $model->{$item_tag};
			}

			if (is_array($model) && isset($model[$item_tag])) {
				$arr_tag[$item_tag] 	=  $model[$item_tag];
			}
		}

		return $arr_tag;
	}

}

if (!function_exists('storeLog')) {

    function storeLog($type, $nomor, $tag, $menu)
    {
        $log_helper 	= new LogHelper;
        $item 	        = [
            'user_id' 		=> auth()->user()->id,
            'menu' 			=> $menu,
            'action' 		=> $log_helper->typeFormatted($type),
            'log' 			=> $log_helper->makeLogMessage($type , $nomor, $tag, $menu),
            'tag' 		    => $tag,
        ];

        UserLog::create($item);

        return true;
    }

}

if (!function_exists('counterIcon')) {
    
    function counterIcon($counterName)
    {
        $arr = [
            "A"             => "fa fa-circle-user",
            "B"             => "fa fa-circle-user",
            "C"             => "fa fa-circle-user",
            "D"             => "fa fa-circle-user",
        ];

        return $arr[$counterName];
    }

}

if (!function_exists("terbilang")) {
    
    function terbilang($x) {
        
        if (substr($x, 0, 1) === '0') {
            $x = substr($x, 1);
        }

        $angka = ["", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"];

        if ($x < 12)
          return array_key_exists($x, $angka) ? " " . $angka[$x] : "";
        elseif ($x < 20)
          return terbilang($x - 10) . " Belas";
        elseif ($x < 100)
          return terbilang($x / 10) . " Puluh" . terbilang($x % 10);
        elseif ($x < 200)
          return "Seratus" . terbilang($x - 100);
        elseif ($x < 1000)
          return terbilang($x / 100) . " Ratus" . terbilang($x % 100);
        elseif ($x < 2000)
          return "Seribu" . terbilang($x - 1000);
        elseif ($x < 1000000)
          return terbilang($x / 1000) . " Ribu" . terbilang($x % 1000);
        elseif ($x < 1000000000)
          return terbilang($x / 1000000) . " Juta" . terbilang($x % 1000000);
    }

}

if (!function_exists("saltedEncode")) {
    
    function saltedEncode($string)
    {
        $key        = env("SALT_ENC");
        $plaintext  = $string; 
        
        $ivlen          = openssl_cipher_iv_length($cipher="AES-128-CBC"); 
        $iv             = openssl_random_pseudo_bytes($ivlen); 
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv); 
        $hmac           = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true); 
        
        $ciphertext = base64_encode($iv.$hmac.$ciphertext_raw);

        return $ciphertext;
    }

}

if (!function_exists("saltedDecode")) {
    
    function saltedDecode($ciphertext)
    {
        $key                = env("SALT_ENC");
        $c                  = base64_decode($ciphertext); 
        $ivlen              = openssl_cipher_iv_length($cipher="AES-128-CBC"); 
        $iv                 = substr($c, 0, $ivlen); 
        $hmac               = substr($c, $ivlen, $sha2len=32); 
        $ciphertext_raw     = substr($c, $ivlen+$sha2len); 
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv); 
        $calcmac            = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true); 
        
        if (hash_equals($hmac, $calcmac)) {
            return $original_plaintext; 
        } else { 
            throw new Exception('Decryption failed!');
        }
    }

}