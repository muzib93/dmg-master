<?php

namespace Helpers;

use App\Models\User;
use GuzzleHttp\Client;
use App\Models\RoleRelation;
use Illuminate\Support\Facades\Log;
use App\Notifications\AlertNotification;
use GuzzleHttp\Exception\ClientException;

class NotificationHelper {

    public function __construct()
    {
        
    }

    public function notify(array $permissions, array $data, $perusahaanId = null, bool $isWithAdmin = true)
    {
        $roleRelationIds            = RoleRelation::where("perusahaan_id", $perusahaanId)->pluck("role_id");
        $users                      = $this->getUserByPermission($permissions, $roleRelationIds);

        foreach ($users as $key => $user) {
            $user->notify(new AlertNotification($data));
        }

        if ($isWithAdmin) {
            $this->notifiyToAdmin($data);
        }
    }

    protected static function getUserByPermission($permissions, $roleRelationIds)
    {
        $user   = User::whereHas("roles.permissions", function ($query) use ($permissions) {
            $query->whereIn("name", $permissions);
        });

        if ($roleRelationIds->isNotEmpty()) {
            $user       = $user->whereHas("roles", function ($query) use ($roleRelationIds)
            {
                $query->whereIn("id", $roleRelationIds);
            });
        }

        return $user->get();
    }

    protected function notifiyToAdmin($data)
    {
        $users      = User::whereHas("roles", function ($query)
        {
            $query->where("name", "admin");
        })->get();

        foreach ($users as $key => $user) {
            $user->notify(new AlertNotification($data));
        }
    }
}