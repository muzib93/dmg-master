<?php

namespace Helpers;

use App\Models\PerusahaanJenis;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SidebarHelper {

    protected $fileJson;
    
    /**
     * __construct
     *
     * @param  mixed $fileJson
     * @return void
     */
    public function __construct(String $fileJson)
    {
        $this->fileJson         = json_decode(file_get_contents(base_path("jsons/$fileJson")), true);
    }

    public function getMenuByRole()
    {
        $topBarArray            = $this->fileJson;
        $topBarArray            = collect($topBarArray);
        $role                   = $this->getRole();
        $roleName               = empty($role) ? "ADMIN" : $role->name;
        $menus                  = $topBarArray->where("role", $roleName)->first()["menu"];

        if ($roleName == "perusahaan") {
            $perusahaan         = auth()->user()->perusahaan;
            $jenisRequireds     = [PerusahaanJenis::JENIS_PEMILIK_TUGBOAT => "Tugboat", PerusahaanJenis::JENIS_PEMILIK_TONGKANG => "Tongkang", PerusahaanJenis::JENIS_PEMILIK_JETTY => "Jetty"]; 
            foreach ($jenisRequireds as $key => $jenisRequired) {
                if (empty($perusahaan)) {
                    $menus          = collect($menus)->filter(function ($menu) use ($jenisRequired) {
                        return $menu["sectionName"] != $jenisRequired;
                    })->values()->toArray();
                }

                if (!empty($perusahaan) && $perusahaan->perusahaanJenis->isNotEmpty() && $perusahaan->perusahaanJenis->where("jenis", $key)->isEmpty()) {
                    $menus          = collect($menus)->filter(function ($menu) use ($jenisRequired) {
                        return $menu["sectionName"] != $jenisRequired;
                    })->values()->toArray();
                }
            }
        }

        return $menus;

        // $arrayMenu              = [];

        // foreach ($topBarArray["menu"] as $keySection => $sectionItems) {
        //     $tempMenu           = [];
        //     foreach ($sectionItems["menu"] as $key => $menu) {
        //         if (!array_key_exists("has-child", $menu) && $role->permissions->pluck('route_name')->contains($menu["route"])) {
        //             $tempMenu[]     = $menu;
        //         }

        //         if (array_key_exists("has-child", $menu) && $menu["has-child"]) {
        //             $childArrayMenu     = [];
        //             foreach ($menu["child"] as $keyChild => $menu_child) {
        //                 if ($role->permissions->pluck("route_name")->contains($menu_child["route"])) {
        //                     $childArrayMenu[]   = $menu_child;
        //                 }
        //             }
        //             if (!empty($childArrayMenu)) {
        //                 $menu["child"]      = $childArrayMenu;
        //                 $tempMenu[]         = $menu;
        //             }
        //         }
        //     }
        //     if (!empty($tempMenu)) {
        //         $arrayMenu[]        = [
        //             "sectionName"           => $sectionItems["sectionName"],
        //             "menu"                  => $tempMenu,
        //         ];
        //     }
        // }

        // return $arrayMenu;
    }

    public function getRole()
    {
        return auth()->user()->roles->first();
    }
}