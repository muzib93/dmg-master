<?php 

namespace Helpers;


class FcmHelper
{
	function sendFcm($data, $destination)
	{
	  $url          = "https://fcm.googleapis.com/fcm/send";
	  $token        = $destination;
	  $serverKey    = 'AAAAWHEbFJE:APA91bHuG5tlS17LET2KQ-qKBMw0ea6X5SBRp2fjqek3cFHIZkBvNbO3ga56jSkNqzuAVTWN0RPjl4T_QEyhIzp5So7eCJ2pqQPl6A2Cyb7l0EzChvq3RPlc89zbboR_dCLiXDP7O6_Q';
	  $title        = $data['title'];
	  $body         = $data['body'];
	  $notification = array('title' => $title , 'body' => $body, 'android_channel_id' => 'yum_mobile');
	  $dataNotif 	= array('title' => $title , 'body' => $body);
	  $arrayToSend  = array('to' => $token, 'notification' => $notification, 'data' => $dataNotif);

	  $json         = json_encode($arrayToSend);
	  $headers      = array();
	  $headers[]    = 'Content-Type: application/json';
	  $headers[]    = 'Authorization: key='. $serverKey;
	  $ch           = curl_init();
	  curl_setopt($ch, CURLOPT_URL, $url);
	  curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
	  curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	  curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
	  //Send the request
	  $response = curl_exec($ch);

	    //Close request
	  if ($response === FALSE) {
	      die('FCM Send Error: ' . curl_error($ch));
	  }
	  curl_close($ch);
	}
}