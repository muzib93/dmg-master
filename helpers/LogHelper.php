<?php 

namespace Helpers;

use App\Models\UserLog;
use Carbon\Carbon;

class LogHelper
{
	public function makeLogArray($type, $nomor, $tag, $menu)
	{
		return [
			'user_id' 		=> auth()->user()->id,
			'menu' 			=> $menu,
			'action' 		=> $this->typeFormatted($type),
			'log' 			=> $this->makeLogMessage($type , $nomor, $tag, $menu),
			'tag' 		=> json_encode($tag),
		];
	}

	public function storeLog($type, $nomor, $tag, $menu)
	{
		$item 	= [
			'user_id' 		=> auth()->user()->id,
			'menu' 			=> $menu,
			'action' 		=> $this->typeFormatted($type),
			'log' 			=> $this->makeLogMessage($type , $nomor, $tag, $menu),
			'tag' 		=> $tag,
		];

		UserLog::create($item);

		return true;
	}

	public function makeLogMessage($type, $nomor, $tag, $menu)
	{
		$message 		= '<p>User <b>'.ucwords(auth()->user()->name).'</b> melakukan '.$this->typeFormatted($type);
		
		// LOGIN OR LOGOUT MESSAGE
		if ($type == 'login' || $type == 'logout') {
			$message .= $this->logInOrOutMessage();
			return $message;
		}
		// 

		$message 		.= $this->textMenuDanNomor($menu, $nomor, $tag);
		$message 		.= $this->textTanggal();

		return $message;
	}

	public function typeFormatted($type)
	{
		$str 	= $type;
		switch ($type) {
			case 'add':
				$str 		= 'Penambahan';
				break;
			case 'edit':
				$str 		= 'Perubahan';
				break;
			case 'delete':
				$str 		= 'Penghapusan';
				break;
			case 'login':
				$str 		= 'Login';
				break;
			case 'logout':
				$str 		= 'Logout';
				break;
		}

		return $str;
	}

	public function logInOrOutMessage()
	{
		return $this->textTanggal();
	}

	public function textTanggal()
	{
		return ' pada tanggal <b>'.Carbon::now()->format('d F Y H:i:s').'</b> </p>';
	}

	public function textMenuDanNomor($menu, $nomor, $tag)
	{
		return ' data di menu <b>'.ucwords($menu).'</b> dan id/nomor <b>'.$nomor.', '.$this->tagToStr($tag).'</b>';
	}

	public function tagToStr($tag)
	{
		$str 			= '';
		foreach ($tag as $key => $item_tag) {
			end($tag);
			if ($key == key($tag)) {
				$str 	.= strtoupper($key).' : '.strtoupper($item_tag);
			}else {
				$str 	.= strtoupper($key).' : '.strtoupper($item_tag).', ';
			}
		}

		return $str;
	}
}