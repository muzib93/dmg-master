<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTongkangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tongkang')) {
            Schema::create('tongkang', function (Blueprint $table) {
                $table->id();
                $table->string("nama");
                $table->string("nama_pemilik");
                $table->string("ukuran");
                $table->integer("tonase_kotor");
                $table->string("tanda_pendaftaran");
                $table->date("tanggal_surat_laut");
                $table->date("tanggal_ukur");
                $table->date("masa_berlaku_konstruksi");
                $table->date("masa_berlaku_lambung");
                $table->date("masa_berlaku_garis_muat");
                $table->text("surat_laut_file")->nullable();
                $table->text("surat_ukur_file")->nullable();
                $table->text("konstruksi_file")->nullable();
                $table->text("lambung_file")->nullable();
                $table->text("garis_muat_file")->nullable();
                $table->bigInteger("perusahaan_id")->unsigned();    
                $table->foreign("perusahaan_id")
                      ->on("perusahaan")
                      ->references("id");
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tongkang');
    }
}
