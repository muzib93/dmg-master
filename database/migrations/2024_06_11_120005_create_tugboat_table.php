<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTugboatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tugboat')) {
            Schema::create('tugboat', function (Blueprint $table) {
                $table->id();
                $table->string("nama_pemilik");
                $table->bigInteger("nib");
                $table->string("nama");
                $table->string("ukuran");
                $table->integer("tonase_kotor");
                $table->string("registrasi_pas");
                $table->date("masa_berlaku_skksd");
                $table->date("surat_ukur_berlaku");
                $table->date("tanggal_pas");
                $table->date("masa_berlaku_izin_operasi");
                $table->date("masa_berlaku_izin_trayek");
                $table->tinyInteger("jenis_kapal");
                $table->text("skksd_file")->nullable();
                $table->text("surat_ukur_file")->nullable();
                $table->text("pas_file")->nullable();
                $table->text("izin_operasi_file")->nullable();
                $table->text("izin_trayek_file")->nullable();
                $table->text("nib_file")->nullable();
                $table->bigInteger("perusahaan_id")->unsigned();    
                $table->foreign("perusahaan_id")
                      ->on("perusahaan")
                      ->references("id");
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tugboat');
    }
}
