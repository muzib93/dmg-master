<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerusahaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('perusahaan')) {
            Schema::create('perusahaan', function (Blueprint $table) {
                $table->id();
                $table->string('nama_perusahaan');
                $table->string('nama_pemilik');
                $table->bigInteger('nib');
                $table->bigInteger('iup');
                $table->text('nib_file')->nullable();
                $table->text('iup_file')->nullable();
                $table->bigInteger('user_id')->unsigned();
                $table->foreign('user_id')
                      ->on('users')
                      ->references('id');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaan');
    }
}
