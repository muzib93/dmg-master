<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJettyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('jetty')) {
            Schema::create('jetty', function (Blueprint $table) {
                $table->id();
                $table->string("nama");
                $table->string("lokasi");
                $table->string("titik_kordinat");
                $table->string("ijin_jetty");
                $table->string("ijin_jetty_file")->nullable();
                $table->bigInteger("perusahaan_id")->unsigned();    
                $table->foreign("perusahaan_id")
                      ->on("perusahaan")
                      ->references("id");
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jetty');
    }
}
