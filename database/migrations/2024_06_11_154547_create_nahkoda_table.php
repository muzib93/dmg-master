<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNahkodaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('nahkoda')) {
            Schema::create('nahkoda', function (Blueprint $table) {
                $table->id();
                $table->bigInteger("tugboat_id")->unsigned();
                $table->foreign("tugboat_id")
                      ->on("tugboat")
                      ->references("id");
                $table->string("nama");
                $table->date("tanggal_skak");
                $table->date("tanggal_aktif");
                $table->text("skak_file");
                $table->text("ijazah_laut_file");
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nahkoda');
    }
}
