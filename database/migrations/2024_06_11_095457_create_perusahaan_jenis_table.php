<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerusahaanJenisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('perusahaan_jenis')) {
            Schema::create('perusahaan_jenis', function (Blueprint $table) {
                $table->id();
                $table->tinyInteger("jenis");
                $table->bigInteger("perusahaan_id")->unsigned();    
                $table->foreign("perusahaan_id")
                      ->on("perusahaan")
                      ->references("id");
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaan_jenis');
    }
}
