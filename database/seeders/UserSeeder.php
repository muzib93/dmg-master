<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createPermission();

        $admin              = Role::create(["name" => "admin"]);

        $userAdmin          = User::create([
            "name"                  => 'admin',
            "email"                 => 'admin@gmail.com',
            "password"              => Hash::make("123123"),
        ]);

        $userAdmin->assignRole($admin);

        $roleUser               = Role::create(["name" => "perusahaan"]);

        $user                   = User::create([
            "name"                  => 'Pemilik',
            "email"                 => 'pemilik@gmail.com',
            "password"              => Hash::make("123123"),
        ]);

        $user->assignRole($roleUser);

        $user                   = User::create([
            "name"                  => 'Perusahaan 2',
            "email"                 => 'perusahaan2@gmail.com',
            "password"              => Hash::make("123123"),
        ]);

        $user->assignRole($roleUser);
    }

    public function createPermission(): void
    {
        $arr                = [];
        $permissionArr      = array_values($this->permissions());
        $permissions        = array_merge(...$permissionArr);

        foreach ($permissions as $key => $item) {
            $arr[]          = [
                "name"          => $item["name"],
                "route_name"    => $item["route_name"],
                "parent_type"   => 0,
                "guard_name"    => "web",
                "created_at"    => date("Y-m-d H:i:s"),    
                "updated_at"    => date("Y-m-d H:i:s"),    
            ];
        }

        Permission::insert($arr);
    }

    public function permissions(): array
    {
        return [
            "customer"      => [
                [
                    "name"          => "Read Dashboard",
                    "route_name"    => "dashboard.index",
                ],
                [
                    "name"          => "Read Permission",
                    "route_name"    => "permission.index",
                ],
                [
                    "name"          => "Read Role",
                    "route_name"    => "role.index",
                ],
                [
                    "name"          => "Create Role",
                    "route_name"    => "role.create",
                ],
                [
                    "name"          => "Edit Role",
                    "route_name"    => "role.edit",
                ],
                [
                    "name"          => "Delete Role",
                    "route_name"    => "role.destroy",
                ],
                [
                    "name"          => "Read User",
                    "route_name"    => "user.index",
                ],
                [
                    "name"          => "Create User",
                    "route_name"    => "user.create",
                ],
                [
                    "name"          => "Edit User",
                    "route_name"    => "user.edit",
                ],
                [
                    "name"          => "Delete User",
                    "route_name"    => "user.destroy",
                ],
            ]
        ];
    }
}
