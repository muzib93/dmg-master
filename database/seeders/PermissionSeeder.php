<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            
            DB::beginTransaction();

            $roles              = [
                "reviewer",
                "approval",
            ];
    
            $permissions        = [
                // LOGOUT KARYAWAN
                [
                    "name"          => "Read Logout Karyawan",
                    "route_name"    => "logout-karyawan.index",
                ],
                [
                    "name"          => "Proses Logout Karyawan",
                    "route_name"    => "logout-karyawan.single",
                ],
                // 
                // DIREKTORAT
                [
                    "name"          => "Read Direktorat",
                    "route_name"    => "direktorat.index",
                ],
                [
                    "name"          => "Create Direktorat",
                    "route_name"    => "direktorat.create",
                ],
                [
                    "name"          => "Edit Direktorat",
                    "route_name"    => "direktorat.edit",
                ],
                [
                    "name"          => "Delete Direktorat",
                    "route_name"    => "direktorat.destroy",
                ],
                [
                    "name"          => "Import Direktorat",
                    "route_name"    => "import.base.store",
                ],
                // 
                // KOMPARTEMEN
                [
                    "name"          => "Read Kompartemen",
                    "route_name"    => "kompartemen.index",
                ],
                [
                    "name"          => "Create Kompartemen",
                    "route_name"    => "kompartemen.create",
                ],
                [
                    "name"          => "Edit Kompartemen",
                    "route_name"    => "kompartemen.edit",
                ],
                [
                    "name"          => "Delete Kompartemen",
                    "route_name"    => "kompartemen.destroy",
                ],
                [
                    "name"          => "Import Kompartemen",
                    "route_name"    => "import.base.store",
                ],
                // 
                // DEPARTEMEN
                [
                    "name"          => "Read Departemen",
                    "route_name"    => "departemen.index",
                ],
                [
                    "name"          => "Create Departemen",
                    "route_name"    => "departemen.create",
                ],
                [
                    "name"          => "Edit Departemen",
                    "route_name"    => "departemen.edit",
                ],
                [
                    "name"          => "Delete Departemen",
                    "route_name"    => "departemen.destroy",
                ],
                [
                    "name"          => "Import Departemen",
                    "route_name"    => "import.base.store",
                ],
                // 
                // JABATAN
                [
                    "name"          => "Read Jabatan",
                    "route_name"    => "jabatan.index",
                ],
                [
                    "name"          => "Create Jabatan",
                    "route_name"    => "jabatan.create",
                ],
                [
                    "name"          => "Edit Jabatan",
                    "route_name"    => "jabatan.edit",
                ],
                [
                    "name"          => "Delete Jabatan",
                    "route_name"    => "jabatan.destroy",
                ],
                [
                    "name"          => "Import Jabatan",
                    "route_name"    => "import.base.store",
                ],
                // 
                // AREA KERJA
                [
                    "name"          => "Read Area Kerja",
                    "route_name"    => "area-kerja.index",
                ],
                [
                    "name"          => "Create Area Kerja",
                    "route_name"    => "area-kerja.create",
                ],
                [
                    "name"          => "Edit Area Kerja",
                    "route_name"    => "area-kerja.edit",
                ],
                [
                    "name"          => "Delete Area Kerja",
                    "route_name"    => "area-kerja.destroy",
                ],
                [
                    "name"          => "Import Area Kerja",
                    "route_name"    => "import.base.store",
                ],
                // 
                // PTKP
                [
                    "name"          => "Read PTKP",
                    "route_name"    => "ptkp.index",
                ],
                [
                    "name"          => "Create PTKP",
                    "route_name"    => "ptkp.create",
                ],
                [
                    "name"          => "Edit PTKP",
                    "route_name"    => "ptkp.edit",
                ],
                [
                    "name"          => "Delete PTKP",
                    "route_name"    => "ptkp.destroy",
                ],
                [
                    "name"          => "Import PTKP",
                    "route_name"    => "import.base.store",
                ],
                // 
                // JENJANG PENDIDIKAN
                [
                    "name"          => "Read Jenjang Pendidikan",
                    "route_name"    => "jenjang-pendidikan.index",
                ],
                [
                    "name"          => "Create Jenjang Pendidikan",
                    "route_name"    => "jenjang-pendidikan.create",
                ],
                [
                    "name"          => "Edit Jenjang Pendidikan",
                    "route_name"    => "jenjang-pendidikan.edit",
                ],
                [
                    "name"          => "Delete Jenjang Pendidikan",
                    "route_name"    => "jenjang-pendidikan.destroy",
                ],
                [
                    "name"          => "Import Jenjang Pendidikan",
                    "route_name"    => "import.base.store",
                ],
                // 
                // AGAMA
                [
                    "name"          => "Read Agama",
                    "route_name"    => "agama.index",
                ],
                [
                    "name"          => "Create Agama",
                    "route_name"    => "agama.create",
                ],
                [
                    "name"          => "Edit Agama",
                    "route_name"    => "agama.edit",
                ],
                [
                    "name"          => "Delete Agama",
                    "route_name"    => "agama.destroy",
                ],
                [
                    "name"          => "Import Agama",
                    "route_name"    => "import.base.store",
                ],
                // 
                // HUBUNGAN KELUARGA
                [
                    "name"          => "Read Hubungan Keluarga",
                    "route_name"    => "hubungan-keluarga.index",
                ],
                [
                    "name"          => "Create Hubungan Keluarga",
                    "route_name"    => "hubungan-keluarga.create",
                ],
                [
                    "name"          => "Edit Hubungan Keluarga",
                    "route_name"    => "hubungan-keluarga.edit",
                ],
                [
                    "name"          => "Delete Hubungan Keluarga",
                    "route_name"    => "hubungan-keluarga.destroy",
                ],
                [
                    "name"          => "Import Hubungan Keluarga",
                    "route_name"    => "import.base.store",
                ],
                // 
                // BANK
                [
                    "name"          => "Read Bank",
                    "route_name"    => "bank.index",
                ],
                [
                    "name"          => "Create Bank",
                    "route_name"    => "bank.create",
                ],
                [
                    "name"          => "Edit Bank",
                    "route_name"    => "bank.edit",
                ],
                [
                    "name"          => "Delete Bank",
                    "route_name"    => "bank.destroy",
                ],
                [
                    "name"          => "Import Bank",
                    "route_name"    => "import.base.store",
                ],
                // 
                // KONTRAK INDUK
                [
                    "name"          => "Read Kontrak Induk",
                    "route_name"    => "kontrak-induk.index",
                ],
                [
                    "name"          => "Create Kontrak Induk",
                    "route_name"    => "kontrak-induk.create",
                ],
                [
                    "name"          => "Edit Kontrak Induk",
                    "route_name"    => "kontrak-induk.edit",
                ],
                [
                    "name"          => "Delete Kontrak Induk",
                    "route_name"    => "kontrak-induk.destroy",
                ],
                [
                    "name"          => "Import Kontrak Induk",
                    "route_name"    => "import.base.store",
                ],
                // 
                // TBK
                [
                    "name"          => "Read TBK",
                    "route_name"    => "tbk.index",
                ],
                [
                    "name"          => "Create TBK",
                    "route_name"    => "tbk.create",
                ],
                [
                    "name"          => "Edit TBK",
                    "route_name"    => "tbk.edit",
                ],
                [
                    "name"          => "Delete TBK",
                    "route_name"    => "tbk.destroy",
                ],
                [
                    "name"          => "Import TBK",
                    "route_name"    => "import.base.store",
                ],
                // 
                // KLASIFIKASI PEKERJAAN
                [
                    "name"          => "Read Klasifikasi Pekerjaan",
                    "route_name"    => "klasifikasi-pekerjaan.index",
                ],
                [
                    "name"          => "Create Klasifikasi Pekerjaan",
                    "route_name"    => "klasifikasi-pekerjaan.create",
                ],
                [
                    "name"          => "Edit Klasifikasi Pekerjaan",
                    "route_name"    => "klasifikasi-pekerjaan.edit",
                ],
                [
                    "name"          => "Delete Klasifikasi Pekerjaan",
                    "route_name"    => "klasifikasi-pekerjaan.destroy",
                ],
                [
                    "name"          => "Import Klasifikasi Pekerjaan",
                    "route_name"    => "import.base.store",
                ],
                // 
                // LOKASI KERJA
                [
                    "name"          => "Read Lokasi Kerja",
                    "route_name"    => "lokasi-kerja.index",
                ],
                [
                    "name"          => "Create Lokasi Kerja",
                    "route_name"    => "lokasi-kerja.create",
                ],
                [
                    "name"          => "Edit Lokasi Kerja",
                    "route_name"    => "lokasi-kerja.edit",
                ],
                [
                    "name"          => "Delete Lokasi Kerja",
                    "route_name"    => "lokasi-kerja.destroy",
                ],
                [
                    "name"          => "Import Lokasi Kerja",
                    "route_name"    => "import.base.store",
                ],
                // 
                // STATUS KARYAWAN
                [
                    "name"          => "Read Status Karyawan",
                    "route_name"    => "status-karyawan.index",
                ],
                [
                    "name"          => "Create Status Karyawan",
                    "route_name"    => "status-karyawan.create",
                ],
                [
                    "name"          => "Edit Status Karyawan",
                    "route_name"    => "status-karyawan.edit",
                ],
                [
                    "name"          => "Delete Status Karyawan",
                    "route_name"    => "status-karyawan.destroy",
                ],
                [
                    "name"          => "Import Status Karyawan",
                    "route_name"    => "import.base.store",
                ],
                // 
                // COST CENTER
                [
                    "name"          => "Read Cost Center",
                    "route_name"    => "cost-center.index",
                ],
                [
                    "name"          => "Create Cost Center",
                    "route_name"    => "cost-center.create",
                ],
                [
                    "name"          => "Edit Cost Center",
                    "route_name"    => "cost-center.edit",
                ],
                [
                    "name"          => "Delete Cost Center",
                    "route_name"    => "cost-center.destroy",
                ],
                [
                    "name"          => "Import Cost Center",
                    "route_name"    => "import.base.store",
                ],
                // 
                // HARI LIBUR
                [
                    "name"          => "Read Hari Libur",
                    "route_name"    => "hari-libur.index",
                ],
                [
                    "name"          => "Create Hari Libur",
                    "route_name"    => "hari-libur.create",
                ],
                [
                    "name"          => "Edit Hari Libur",
                    "route_name"    => "hari-libur.edit",
                ],
                [
                    "name"          => "Delete Hari Libur",
                    "route_name"    => "hari-libur.destroy",
                ],
                // 
                // WAKTU KERJA
                [
                    "name"          => "Read Waktu Kerja",
                    "route_name"    => "waktu-kerja.index",
                ],
                [
                    "name"          => "Create Waktu Kerja",
                    "route_name"    => "waktu-kerja.create",
                ],
                [
                    "name"          => "Edit Waktu Kerja",
                    "route_name"    => "waktu-kerja.edit",
                ],
                [
                    "name"          => "Delete Waktu Kerja",
                    "route_name"    => "waktu-kerja.destroy",
                ],
                // 
                // HARI KERJA
                [
                    "name"          => "Read Hari Kerja",
                    "route_name"    => "hari-kerja.index",
                ],
                [
                    "name"          => "Create Hari Kerja",
                    "route_name"    => "hari-kerja.create",
                ],
                [
                    "name"          => "Edit Hari Kerja",
                    "route_name"    => "hari-kerja.edit",
                ],
                [
                    "name"          => "Delete Hari Kerja",
                    "route_name"    => "hari-kerja.destroy",
                ],
                // 
                // SHIFT
                [
                    "name"          => "Read Shift",
                    "route_name"    => "shift.index",
                ],
                [
                    "name"          => "Create Shift",
                    "route_name"    => "shift.create",
                ],
                [
                    "name"          => "Edit Shift",
                    "route_name"    => "shift.edit",
                ],
                [
                    "name"          => "Delete Shift",
                    "route_name"    => "shift.destroy",
                ],
                // 
                // GENERATE SHIFT
                [
                    "name"          => "Read Generate Shift",
                    "route_name"    => "shift.index",
                ],
                [
                    "name"          => "Create Generate Shift",
                    "route_name"    => "shift.create",
                ],
                [
                    "name"          => "Generate Shift Multiple",
                    "route_name"    => "generate-shift.mass.index",
                ],
                [
                    "name"          => "Edit Generate Shift",
                    "route_name"    => "shift.edit",
                ],
                [
                    "name"          => "Delete Generate Shift",
                    "route_name"    => "shift.destroy",
                ],
                // 
                // TIPE IZIN KHUSUS
                [
                    "name"          => "Read Tipe Izin Khusus",
                    "route_name"    => "tipe-cuti-khusus.index",
                ],
                [
                    "name"          => "Create Tipe Izin Khusus",
                    "route_name"    => "tipe-cuti-khusus.create",
                ],
                [
                    "name"          => "Edit Tipe Izin Khusus",
                    "route_name"    => "tipe-cuti-khusus.edit",
                ],
                [
                    "name"          => "Delete Tipe Izin Khusus",
                    "route_name"    => "tipe-cuti-khusus.destroy",
                ],
                // 
                // REFERENSI BPJS
                [
                    "name"          => "Read Referensi BPJS",
                    "route_name"    => "referensi-bpjs.index",
                ],
                [
                    "name"          => "Tambah Periode Referensi BPJS",
                    "route_name"    => "referensi-bpjs.create",
                ],
                [
                    "name"          => "Show Referensi BPJS",
                    "route_name"    => "referensi-bpjs.show",
                ],
                [
                    "name"          => "Edit Referensi BPJS",
                    "route_name"    => "referensi-bpjs.edit",
                ],
                // 
                // PERUSAHAAN
                [
                    "name"          => "Read Perusahaan",
                    "route_name"    => "perusahaan.index",
                ],
                [
                    "name"          => "Create Perusahaan",
                    "route_name"    => "perusahaan.create",
                ],
                [
                    "name"          => "Edit Perusahaan",
                    "route_name"    => "perusahaan.edit",
                ],
                [
                    "name"          => "Delete Perusahaan",
                    "route_name"    => "perusahaan.destroy",
                ],
                [
                    "name"          => "Show Referensi Perusahaan",
                    "route_name"    => "perusahaan.referensi.index",
                ],
                [
                    "name"          => "Manage Referensi Perusahaan",
                    "route_name"    => "perusahaan.referensi.store",
                ],
                //
                // KONTRAK PERUSAHAAN
                [
                    "name"          => "Read Kontrak Perusahaan",
                    "route_name"    => "kontrak-perusahaan.index",
                ],
                [
                    "name"          => "Create Kontrak Perusahaan",
                    "route_name"    => "kontrak-perusahaan.create",
                ],
                [
                    "name"          => "Edit Kontrak Perusahaan",
                    "route_name"    => "kontrak-perusahaan.edit",
                ],
                [
                    "name"          => "Delete Kontrak Perusahaan",
                    "route_name"    => "kontrak-perusahaan.destroy",
                ],
                [
                    "name"          => "Show Kontrak Perusahaan",
                    "route_name"    => "kontrak-perusahaan.show",
                ],
                [
                    "name"          => "Import Kontrak Perusahaan",
                    "route_name"    => "import.kontrak-perusahaan.store",
                ],
                //
                // TEMPLATE KONTRAK
                [
                    "name"          => "Read Template Kontrak",
                    "route_name"    => "template-kontrak.index",
                ],
                [
                    "name"          => "Create Template Kontrak",
                    "route_name"    => "template-kontrak.create",
                ],
                [
                    "name"          => "Edit Template Kontrak",
                    "route_name"    => "template-kontrak.edit",
                ],
                [
                    "name"          => "Delete Template Kontrak",
                    "route_name"    => "template-kontrak.destroy",
                ],
                //
                // KARYAWAN
                [
                    "name"          => "Read Karyawan",
                    "route_name"    => "karyawan.index",
                ],
                [
                    "name"          => "Create Karyawan",
                    "route_name"    => "karyawan.create",
                ],
                [
                    "name"          => "Edit Karyawan",
                    "route_name"    => "karyawan.edit",
                ],
                [
                    "name"          => "Show Karyawan",
                    "route_name"    => "karyawan.show",
                ],
                [
                    "name"          => "Manage Karyawan",
                    "route_name"    => "",
                ],
                [
                    "name"          => "Export Import Karyawan",
                    "route_name"    => "",
                ],
                [
                    "name"          => "Pengajuan Karyawan",
                    "route_name"    => "karyawan-pengajuan.index",
                ],
                [
                    "name"          => "Show Pengajuan Karyawan",
                    "route_name"    => "karyawan-pengajuan.show",
                ],
                [
                    "name"          => "Approve Karyawan",
                    "route_name"    => "",
                ],
                [
                    "name"          => "Review Karyawan", 
                    "route_name"    => ""
                ],
                [
                    "name"          => "Create Approval Karyawan",
                    "route_name"    => "karyawan.approve.store"
                ],
                [
                    "name"          => "Pengajuan Ulang Karyawan",
                    "route_name"    => "karyawan.pengajuan-ulang"
                ],
                //
                // PENEMPATAN
                [
                    "name"          => "Read Penempatan",
                    "route_name"    => "penempatan.index",
                ],
                [
                    "name"          => "Create Penempatan",
                    "route_name"    => "penempatan.create",
                ],
                [
                    "name"          => "Multi Penempatan Penempatan",
                    "route_name"    => "penempatan.multi.index",
                ],
                [
                    "name"          => "Delete Penempatan",
                    "route_name"    => "penempatan.destroy",
                ],
                //
                // KARYAWAN RESIGN
                [
                    "name"          => "Read Karyawan Resign",
                    "route_name"    => "karyawan-resign.index",
                ],
                [
                    "name"          => "Show Karyawan Resign",
                    "route_name"    => "karyawan.show",
                ],
                //
                // DATA TANGGUNGAN
                [
                    "name"          => "Read Data Tanggungan",
                    "route_name"    => "pengajuan-data-tanggungan.index",
                ],
                [
                    "name"          => "Approval Data Tanggungan",
                    "route_name"    => "pengajuan-data-tanggungan.approval",
                ],
                //
                // SHIFT KARYAWAN
                [
                    "name"          => "Read Shift Karyawan",
                    "route_name"    => "shift-karyawan.index",
                ],
                [
                    "name"          => "Create Shift Karyawan",
                    "route_name"    => "shift-karyawan.create",
                ],
                [
                    "name"          => "Multi Shift Karyawan",
                    "route_name"    => "shift-karyawan.multi.index",
                ],
                [
                    "name"          => "Delete Shift Karyawan",
                    "route_name"    => "shift-karyawan.destroy",
                ],
                //
                // ABSEN
                [
                    "name"          => "Read Absen",
                    "route_name"    => "absen.index",
                ],
                [
                    "name"          => "Edit Absen",
                    "route_name"    => "absen.edit",
                ],
                [
                    "name"          => "Delete Absen",
                    "route_name"    => "absen.destroy",
                ],
                //
                // ABSEN MANUAL
                [
                    "name"          => "Create Absen Manual",
                    "route_name"    => "absen-manual.create",
                ],
                [
                    "name"          => "Import Absen Manual",
                    "route_name"    => "absen-manual.import",
                ],
                //
                // IZIN
                [
                    "name"          => "Read Izin",
                    "route_name"    => "izin.index",
                ],
                [
                    "name"          => "Create Izin",
                    "route_name"    => "izin.create",
                ],
                [
                    "name"          => "Edit Izin",
                    "route_name"    => "izin.edit",
                ],
                [
                    "name"          => "Delete Izin",
                    "route_name"    => "izin.destroy",
                ],
                [
                    "name"          => "Approval Izin 1",
                    "route_name"    => "izin.approval",
                ],
                [
                    "name"          => "Approval Izin 2",
                    "route_name"    => "izin.approval",
                ],
                //
                // CUTI
                [
                    "name"          => "Read Cuti",
                    "route_name"    => "cuti.index",
                ],
                [
                    "name"          => "Create Cuti",
                    "route_name"    => "cuti.create",
                ],
                [
                    "name"          => "Edit Cuti",
                    "route_name"    => "cuti.edit",
                ],
                [
                    "name"          => "Delete Cuti",
                    "route_name"    => "cuti.destroy",
                ],
                [
                    "name"          => "Show Cuti",
                    "route_name"    => "cuti.show",
                ],
                [
                    "name"          => "Approval Cuti 1",
                    "route_name"    => "cuti.approval",
                ],
                [
                    "name"          => "Approval Cuti 2",
                    "route_name"    => "cuti.approval",
                ],
                //
                // SPL
                [
                    "name"          => "Read Spl",
                    "route_name"    => "spl.index",
                ],
                [
                    "name"          => "Create Spl",
                    "route_name"    => "spl.create",
                ],
                [
                    "name"          => "Edit Spl",
                    "route_name"    => "spl.edit",
                ],
                [
                    "name"          => "Delete Spl",
                    "route_name"    => "spl.destroy",
                ],
                [
                    "name"          => "Show Spl",
                    "route_name"    => "spl.show",
                ],
                [
                    "name"          => "Approval Spl 1",
                    "route_name"    => "spl.approval",
                ],
                [
                    "name"          => "Approval Spl 2",
                    "route_name"    => "spl.approval",
                ],
                [
                    "name"          => "Approval Multi Spl 1",
                    "route_name"    => "spl.approval-mass",
                ],
                [
                    "name"          => "Approval Multi Spl 2",
                    "route_name"    => "spl.approval-mass",
                ],
                //
                // LAPORAN
                [
                    "name"          => "Laporan Rekap Karyawan",
                    "route_name"    => "laporan.rekap-karyawan.index",
                ],
                [
                    "name"          => "Laporan Kontrak Perusahaan",
                    "route_name"    => "laporan.kontrak-perusahaan.index",
                ],
                [
                    "name"          => "Laporan Absen Departemen",
                    "route_name"    => "laporan.absen-departemen.index",
                ],
                [
                    "name"          => "Laporab Absen Karyawan",
                    "route_name"    => "laporan.absen-karyawan.index",
                ],
                [
                    "name"          => "Laporan Rekap Cuti",
                    "route_name"    => "laporan.rekap-cuti.index",
                ],
                //
                // IMPORT
                [
                    "name"          => "Import Karyawan",
                    "route_name"    => "import.karyawan.index",
                ],
                [
                    "name"          => "Import Penempatan",
                    "route_name"    => "import.penempatan.index",
                ],
                //
            ];
    
            foreach ($roles as $key => $role) {
                $role               = Role::create([
                    "name"          => $role,
                ]);
            }
    
            foreach ($permissions as $key => $permission) {
                $permission         = Permission::create([
                    "name"          => $permission["name"],
                    "route_name"    => $permission["route_name"],
                    "guard_name"    => "web",
                ]);
            }
    
            $allPermissions         = Permission::get();
            $allRoles               = Role::whereIn("name", ["reviewer","approval"])->get();

            foreach ($allRoles as $key => $allRole) {
                $allRole->givePermissionTo($allPermissions);
                $userRole           = User::create([
                    "name"          => $allRole->name,
                    "email"         => $allRole->name . "@gmail.com",
                    "is_active"     => 1,
                    "status_logout" => 1,
                    "password"      => Hash::make("123123"),
                ]);
                $userRole->assignRole($allRole);
            }

            DB::commit();

            app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();

        } catch (\Throwable $th) {
            DB::rollback();

            throw $th;
        }
    }
}
