const SwalPopUp         = (title, message, icon) => {
    return Swal.fire({
        title: title,
        text: message,
        icon: icon,
        customClass: {
            confirmButton: 'btn btn-primary'
        },
        buttonsStyling: false
    });
}

const SwalDelete       = (formSubmit) => {
    Swal.fire({
        title: 'Are you sure?',
        text: "Are you sure to delete this data?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        customClass: {
            confirmButton: 'btn btn-primary',
            cancelButton: 'btn btn-outline-danger ms-1'
        },
        buttonsStyling: false
    }).then(function (result) {
        if (result.value) {
            formSubmit.submit();
        }
    });
}