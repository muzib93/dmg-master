const StateChange   = (url_type = null, el = null, to_element = null, addition_text = null, addition_value = null) => {
    if (url_type !== null) {
        $.ajax({
            type: "GET",
            url: baseUrl+"/"+url_type,
            data: {
                id: $(el).val(),
                _token: _csrf_token
            },
            dataType: "json",
            success: function (response) {
                RemoveElement(to_element);
                if ($(el).val() != "" && $(el).val() != 'all') {
                    $('.'+to_element).append($(`<option value=${addition_value}>${addition_text}</option>`).attr('selected','selected'));
                }
                $.each(response.data, function(key, value) {
                     $("."+to_element).append($(`<option value=${key}>${value}</option>`));
                });
            },
            fail: function (errMsg) {
                alert(errMsg);
            }
        });
    }   
}

const RemoveElement     = (to_element)  => {
    if (to_element == 'kabupaten') {
        $('.kabupaten').find('option').remove().end();
        $('.kecamatan').find('option').remove().end();
        $('.kelurahan').find('option').remove().end();
    }

    if (to_element == 'kecamatan') {
        $('.'+to_element).find('option').remove().end();
        $('.kelurahan').find('option').remove().end();
    }

    if (to_element == 'kelurahan') {
        $('.'+to_element).find('option').remove().end();
    }

    if (to_element == 'kabupaten-ktp') {
        $('.kabupaten-ktp').find('option').remove().end();
        $('.kecamatan-ktp').find('option').remove().end();
        $('.kelurahan-ktp').find('option').remove().end();
    }

    if (to_element == 'kecamatan-ktp') {
        $('.'+to_element).find('option').remove().end();
        $('.kelurahan-ktp').find('option').remove().end();
    }

    if (to_element == 'kelurahan-ktp') {
        $('.'+to_element).find('option').remove().end();
    }
}
