const PrintPage 	= (title) => {
	var contents 	= $("#hot_scroll").html();
	var frame1 		= $('<iframe />');

    frame1[0].name 	= "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" , "padding":"8px"});
    
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;

    frameDoc.document.open();
    //Create a new HTML document.
    frameDoc.document.write(`<html><head><title>TrilliunMaps - ${title}</title>`);
     //Append the external CSS file.
    frameDoc.document.write(`<link href="${base_url}/admin_assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>`);
    frameDoc.document.write(`<link href="${base_url}/admin_assets/css/core.css" rel="stylesheet" type="text/css"/>`);
    frameDoc.document.write(`<link href="${base_url}/admin_assets/css/fontnya.css" rel="stylesheet" type="text/css"/>`);
    frameDoc.document.write(`<link href="${base_url}/admin_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css"/>`);

    frameDoc.document.write('<style>*{font-size:11px !important;}</style>');
	frameDoc.document.write('</he');
	frameDoc.document.write('ad><bo');
	frameDoc.document.write('dy>');
    //Append the DIV contents.
    frameDoc.document.write(contents);
    frameDoc.document.write('</bo');
	frameDoc.document.write('dy></ht');
	frameDoc.document.write('ml>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        frame1.remove();
    }, 500);
}

const PrintExcel    = (title, e) => {
    
    e.preventDefault();

    var tT = new XMLSerializer().serializeToString(document.querySelector('div#report-content')); //Serialised table
    var tF = `${title}.xls`; //Filename
    var tB = new Blob([tT]); //Blub

    if(window.navigator.msSaveOrOpenBlob){
        //Store Blob in IE
        window.navigator.msSaveOrOpenBlob(tB, tF)
    }
    else{
        //Store Blob in others
        var tA = document.body.appendChild(document.createElement('a'));
        tA.href = URL.createObjectURL(tB);
        tA.download = tF;
        tA.style.display = 'none';
        tA.click();
        tA.parentNode.removeChild(tA)
    }
}