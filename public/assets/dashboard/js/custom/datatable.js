const InitDatatable         = (url, fieldArr, defsArr = [], filterArr = [], rowCallback = () => {}) => {
    let dTableConfig    = $('.dt-general').DataTable(
        {
            ajax: AjaxConfig(url, filterArr),
            order: [],
            columns: MakeColumnsDatatable(fieldArr),
            columnDefs: MakeColumnDefsDatatable(defsArr, fieldArr),
            orderCellsTop: true,
            scrollX: true,
            serverSide: true,
            processing: true,
            language: {
                paginate: {
                    previous:'&nbsp;',
                    next: '&nbsp;'
                },
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>'
            },
            "rowCallback": function( row, data ) {
                rowCallback(row, data);
            }
        }
    );

    $("body").find("form.dt_adv_search").find("select").on("change", () => {
        dTableConfig.draw();
    });

    $("body").find("form.dt_adv_search").find(".flatpicker").on("change", () => {
        dTableConfig.draw();
    });

    $('.dataTables_filter .form-control').removeClass('form-control-sm');
    $('.dataTables_length .form-select').removeClass('form-select-sm').removeClass('form-control-sm');
    $("#DataTables_Table_0_wrapper").find(".row").eq(0).addClass("d-flex justify-content-between align-items-center mx-0");
    $("#DataTables_Table_0_wrapper").find(".row").last().addClass("d-flex justify-content-between align-items-center mx-0");

    return dTableConfig;
}

const AjaxConfig        = (url, filterArr) => {
    return {
        url: url,
        data: (d) => {
            GetFilter(d, filterArr)
        }
    }
}

const GetFilter         = (d, filterArr) => {
    $.each(filterArr, function (index, val) { 
        let filterValue = $("form.dt_adv_search").find('[name="' + val + '"]').val();
        d[val]       = filterValue === undefined ? "" : filterValue;
    });
    // console.log(d);
    return d;
}

const ClearFilterInput  = (el, e) => {
    e.preventDefault();
    $(el).closest("form").find("select").each((index, element) => {
        $(element).val("").trigger("change");
    });

    $(el).closest("form").find("input").each((index, element) => {
        $(element).val("");
    });

    $("body").find(".dt-general").DataTable().draw();
}

const MakeColumnsDatatable  = (fieldArr) => {
    let arr = [
        { 
            data: 'DT_RowIndex', 
            name: 'DT_RowIndex', 
            orderable: false, 
            searchable: false
        }
    ];

    $.each(fieldArr, function(index, val) {
        arr.push({ data: val });
    });

    return arr;
}

const MakeColumnDefsDatatable   = (defsArr, fieldArr) => {
    let arr     = [
        {
            "targets": 0,
            "className": "text-center",
            "width": "2%"
        }
    ];

    $.each(defsArr, function(index, val) {
        arr.push(val);
    });

    if (fieldArr.slice(-1).pop() == "options") {
        arr.push({
            "targets": -1,
            "className": "text-center",
            // "width": "2%"
            orderable: false, 
            searchable: false
        });   
    }

    return arr;
}

$('.date-range-picker-filter').daterangepicker({
    locale: {
        format: 'DD-MM-YYYY',
        cancelLabel: 'Clear'
    },
    autoUpdateInput: false,
});

$('.date-range-picker-filter').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' to ' + picker.endDate.format('DD-MM-YYYY'));
    $("body").find(".dt-general").DataTable().draw();
});

$('.date-range-picker-filter').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
    $("body").find(".dt-general").DataTable().draw();
});