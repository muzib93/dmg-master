let templateContent     = "";

const FormatDate                     = (date, format) => {
    const map = {
        mm: date.getMonth() + 1,
        dd: date.getDate(),
        yyyy: date.getFullYear(),
        h: date.getHours(),
        i: date.getMinutes(),
    }

    return format.replace(/mm|dd|yyyy|h|i/gi, matched => map[matched])
}

const GetScheduleDate               = () => {
    if ($(".schedule-date").val() != "") {
        return $(".schedule-date").val();
    }else {
        return FormatDate(new Date(), "dd-mm-yyyy h:i");
    }
}

const ChangeDate                    = () => {
    KeyUpMessage($('#message-content'));
}

const ChangeTemplate                = (el, url) => {
    $.ajax({
        type: "GET",
        url: url,
        data: {
            message_template_id: $(el).val(),
        },
        dataType: "JSON",
        success: function (response) {
            console.log("dawdwadwa")
            if (response.status) {
                templateContent     = response.data;
            }else {
                templateContent     = "";
            }
            KeyUpMessage($('#message-content'));
        }
    });
}

const FormatMessage                 = (templateFormatted, messageBreak) => {
    if (templateFormatted != "" && messageBreak != "") {
        return templateFormatted.includes("{message}") ? 
            templateFormatted.replaceAll("{message}", messageBreak) : 
            templateFormatted + " " + messageBreak;
    }else {
        return templateFormatted + " " + messageBreak;
    }
}

const KeyUpMessage                  = (el) => {
    const messageContent    = $(".docs-preview-pane").find(".message-content");
    const scheduleDate      = GetScheduleDate();
    let template             = templateContent.replaceAll('\n', '<br/>');

    if ($(el).val().length == 0 && template == "") {
        $(messageContent).addClass("d-none");
        $(messageContent).find(".phone-message").text(" ");   
    }else {
        let messageBreak    = $(el).val().replaceAll('\n', '<br/>');
        $(messageContent).removeClass("d-none");
        $(messageContent).find(".time").text(scheduleDate);
        $(messageContent).find(".phone-message").html(FormatMessage(template, messageBreak));
    }
}

$('#message-content').keyup(function() {
    KeyUpMessage(this);
});