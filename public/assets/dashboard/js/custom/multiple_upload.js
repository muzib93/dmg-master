function readURL(input , tar) {  
    if (input.files && input.files[0]) { 
        $.each(input.files , function(index,ff) 
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                tar.attr('src', e.target.result);
            }
            reader.readAsDataURL(ff);
        });
    }
}

$('body').on('change', '.picupload', function() {
    var div         = document.createElement("li");
    div.innerHTML   = DataHtml('remove-pic');
    var thisHtml    = $(this).clone();
    $(thisHtml).attr('name','image_gallery[]');
    $("#media-list").prepend(div);
    $('.media-list li:first').find('.file-place').html(thisHtml);
    readURL(this, $('.media-list li:first').find('img.img-responsive'));
});

$('body').on('click', '.remove-pic', function() {
    $(this).parent().parent().parent().remove();
});

$('body').on('change', '.single-picupload',function () {
    var div         = document.createElement("li");
    div.innerHTML   = DataHtml('single-remove-pic');
    var thisHtml    = $(this).clone();
    $(thisHtml).attr('name','thumbnail');
    $("#single-list").prepend(div);
    $('.single-list li:first').find('.file-place').html(thisHtml);
    readURL(this, $('.single-list li:first').find('img.img-responsive'));
    $(this).closest('li').remove();
});

$('body').on('click', '.single-remove-pic', function() {
    $(this).parent().parent().parent().remove();
    $("#single-list").prepend(SingleUploadHtml());
});

const DataHtml  = (classRemove) => {
  return "<img style='height: 100%; width: 100%' class='img-responsive img-rounded' src='' title=''/>"+
         "<div class='file-place display-none'>"+
         "</div>"+
          "<div  class='post-thumb'>"+
            "<div class='inner-post-thumb'>"+
              "<a href='javascript:void(0);' data-id='' class="+classRemove+">"+
                "<i style='color: red;' class='fa fa-times' aria-hidden='true'></i>"+
              "</a>"+
              "<div>"+
            "</div>";
}

const SingleUploadHtml  = () => {
  return '<li class="single-upload">'+
          '<span>'+
            '<i class="fa fa-plus text-indigo-400" aria-hidden="true"></i>'+
            '<input type="file" name="thumbnail" id="single-picupload" class="single-picupload" value="">'+
          '</span>'+
        '</li>';
}