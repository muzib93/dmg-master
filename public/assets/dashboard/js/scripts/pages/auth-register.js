const baseUrl                 = window.location.origin;
var captchaState              = false;

$(function () {
  ('use strict');

  var registerMultiStepsWizard  = document.querySelector('.register-multi-steps-wizard');
  if (typeof registerMultiStepsWizard !== undefined && registerMultiStepsWizard !== null) {
    var numberedStepper = new Stepper(registerMultiStepsWizard);

    $(registerMultiStepsWizard)
      .find('.btn-next')
      .each(function () {
        $(this).on('click', function (e) {
          var isValid = true;
          isValid     = CheckPeriodePendaftaranId(this);
          if (isValid) {
            numberedStepper.next();
          } else {
            SwalPopUp("Peringatan","Jalur Pendaftaran Harus Dipilih","error");
            e.preventDefault();
          }
        });
      });

    $(registerMultiStepsWizard)
      .find('.btn-prev')
      .on('click', function () {
        numberedStepper.previous();
      });

    $(registerMultiStepsWizard)
      .find('.btn-submit')
      .on('click', function () {
        var isValid = $(this).closest("#formulir").find("form").valid();
        CheckSumberRequired();
        
        if (!captchaState) {
          ValidateCaptcha(isValid);
        }

        ValidateEmail(captchaState, isValid);
      });
  }
});

const PilihJalur    = (e, el, urlFindPeriodePendaftaran) => {
  e.preventDefault();
  $(el).closest(".pricing-card").find(".basic-pricing").removeClass("popular");
  $(el).closest(".pricing-card").find("button").removeClass("btn-primary").addClass("btn-outline-primary");
  $(el).closest(".basic-pricing").addClass("popular");
  $(el).removeClass("btn-outline-primary").addClass("btn-primary");
  $("#input-periode-pendaftaran-id").val(urlFindPeriodePendaftaran);
  GetInformasiJalur(urlFindPeriodePendaftaran);
}

const CheckPeriodePendaftaranId = () => {
  if (!$(".basic-pricing").hasClass("popular")) {
    return false;
  }

  return true;
}

const SwalPopUp         = (title, message, icon) => {
  return Swal.fire({
      title: title,
      text: message,
      icon: icon,
      customClass: {
          confirmButton: 'btn btn-primary'
      },
      buttonsStyling: false
  });
}

const GetInformasiJalur   = (urlFindPeriodePendaftaran) => {
  $.ajax({
    type: "GET",
    url: urlFindPeriodePendaftaran,
    data: {},
    dataType: "JSON",
    success: function (response) {
      if (response.status) {
        $("#formulir").find("#form-pendaftaran").html(response.data.form_view);
        $("#informasi-jalur").find("#konten-informasi").html(response.data.konten_informasi);
        InstancePackage();
      }
    }
  });  
}

const ClickSumber     = (sumber_id, el) => {
  if (sumber_id == "9" && $(el).prop("checked") == true) {
    $(".input-sumber-lainnya").removeClass("d-none").removeAttr("disabled").attr("required","required");
  }else if (sumber_id == "9" && $(el).prop("checked") == false) {
    $(".input-sumber-lainnya").addClass("d-none").attr("disabled","disabled").removeAttr("required").removeClass("error");
    $("#sumber_lainnya-error").remove();
  }
}

const InstancePackage   = () => {
  $(".select2").each(function () {
    var $this = $(this);
    $this.wrap('<div class="position-relative"></div>');
    $this.select2({
        dropdownAutoWidth: true,
        width: '100%',
        dropdownParent: $this.parent()
    });
  });
  $('.select2').on('select2:open', function (e) {
    const evt = "scroll.select2";
    $(e.target).parents().off(evt);
    $(window).off(evt);
  });
  $(".flatpicker").flatpickr({
    dateFormat: 'd-m-Y'
  });
}

const CheckSumberRequired   = () => {
  if ($(".form-check-input:checked").length == 0) {
    $(".form-check-input").closest(".col-md-6").find(".form-label").append('<span id="sumber_informasi-error" class="error"> Sumber informasi harus dipilih salah satu.</span>');
    throw new Error("Sumber informasi harus dipilih salah satu"); 
  }else {
    $("#sumber_informasi-error").remove();
  }
}

const ReloadCaptcha   = (urlCaptcha) => {
  $.ajax({
    type: "GET",
    url: urlCaptcha,
    data: {},
    dataType: "JSON",
    success: function (response) {
      if (response.data) {
        $(".captcha span").html(response.data.captcha);
      }
    }
  });
}

const ValidateCaptcha   = (isValid) => {
  $.ajax({
    type: "POST",
    url: baseUrl + "/ajax/validate-captcha",
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') 
    },
    data: {
      "captcha_value": $(".input-captcha").val()
    },
    dataType: "JSON",
    success: function (response) {
      if (!response.status) {
        $(".input-captcha").addClass("error");
        $(".input-captcha").closest(".col-md-6").append('<span id="captcha-error" class="error"> ' + response.message + '.</span>');
        ReloadCaptcha(baseUrl + "/ajax/reload-captcha");
      }else {
        $(".input-captcha").removeClass("error").addClass("success");
        $('#captcha-error').remove();
        $(".input-captcha").closest(".col-md-6").append('<span id="captcha-success" class="text-success"> Captcha is correct.</span>');
        $(".input-captcha").attr("disabled","disabled");
        $(".input-captcha").closest(".col-md-6").find(".captcha button").addClass("disabled");
        captchaState  = true;
        ValidateEmail(captchaState, isValid);
      }
    }
  });
}

const ValidateEmail     = (captchaState, isValid) => {
  $.ajax({
    type: "GET",
    url: baseUrl + "/ajax/validate-email",
    data: {
      email: $("#input-email").val()
    },
    dataType: "JSON",
    success: function (response) {
      if (response.status) {
        SwalPopUp("Peringatan", response.message, "error");
      }else {
        if (captchaState && isValid) {
          $("#form-pendaftaran-store").submit();
        }
      }
    }
  }); 
}