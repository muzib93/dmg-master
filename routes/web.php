<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\Dashboard\RoleController;
use App\Http\Controllers\Dashboard\UserController;
use App\Http\Controllers\Dashboard\TugboatController;
use App\Http\Controllers\Dashboard\UserLogController;
use App\Http\Controllers\Dashboard\TongkangController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Dashboard\JettyController;
use App\Http\Controllers\Dashboard\PermissionController;
use App\Http\Controllers\Perusahaan\PerusahaanController;

Auth::routes();

Route::group(["middleware" => ["auth","dashboard"]], function ()
{
    Route::get('/', [DashboardController::class, "index"])->name("dashboard.index");

    // DATATABLES
        Route::get('permission/data', [PermissionController::class, 'data'])->name('permission.data');
        Route::get('user/data', [UserController::class, 'data'])->name('user.data');
        Route::get('user-log/data', [UserLogController::class, 'data'])->name('user-log.data');
        Route::get('tugboat/data', [TugboatController::class, 'data'])->name('tugboat.data');
        Route::get('tongkang/data', [TongkangController::class, 'data'])->name('tongkang.data');
        Route::get('jetty/data', [JettyController::class, 'data'])->name('jetty.data');
    // 

    // AUTH
        Route::resource("user", UserController::class);
        Route::get("notifikasi", [UserController::class, "notifikasiIndex"])->name("notifikasi.index");
        
        Route::resource("role", RoleController::class);
        Route::get("role-add-user/{roleId}", [RoleController::class, "addUserIndex"])->name("role.add-user.index");
        Route::put("role-add-user/{roleId}", [RoleController::class, "addUserStore"])->name("role.add-user.store");
        Route::resource("permission", PermissionController::class);
        Route::resource("user-log", UserLogController::class);
        Route::post("user-log-upload/{userLogId}", [UserLogController::class, "userLogUpload"])->name("user-log.upload");
    // 

    // TUGBOAT
        Route::resource("tugboat", TugboatController::class);
        Route::post("tugboat-upload/{tugboatId}", [TugboatController::class, "upload"])->name("tugboat.upload");
        Route::get("tugboat-remove-file/{tugboatId}/{field}", [TugboatController::class, "removeFile"])->name("tugboat.remove-file");
        Route::post("tugboat-nahkoda/{tugboatId}", [TugboatController::class, "nahkodaStore"])->name("tugboat.nahkoda.store");
        Route::delete("tugboat-nahkoda/{nahkodaId}", [TugboatController::class, "nahkodaDelete"])->name("tugboat.nahkoda.delete");
    // 

    // TONGKANG
        Route::resource("tongkang", TongkangController::class);
        Route::post("tongkang-upload/{tongkangId}", [TongkangController::class, "upload"])->name("tongkang.upload");
        Route::get("tongkang-remove-file/{tongkangId}/{field}", [TongkangController::class, "removeFile"])->name("tongkang.remove-file");
    // 

    // JETTY
        Route::resource("jetty", JettyController::class);
        Route::post("jetty-upload/{jettyId}", [JettyController::class, "upload"])->name("jetty.upload");
        Route::get("jetty-remove-file/{jettyId}/{field}", [JettyController::class, "removeFile"])->name("jetty.remove-file");
    // 
    
    // PROFILE
        Route::get("profile", [UserController::class, "editProfile"])->name("profile.edit");
        Route::post("profile", [UserController::class, "updateProfile"])->name("profile.update");
    // 

    // AJAX
        Route::group(["prefix" => "ajax/", "as" => "ajax."], function ()
        {
            Route::get("mark-all-as-read", [AjaxController::class, "markAllAsRead"])->name("mark-all-as-read");
            Route::get("get-kabupaten", [AjaxController::class, "getKabupaten"])->name("get-kabupaten");
            Route::get("get-kecamatan", [AjaxController::class, "getKecamatan"])->name("get-kecamatan");
            Route::get("get-kelurahan", [AjaxController::class, "getKelurahan"])->name("get-kelurahan");
        });
    // 

    // PERUSAHAAN
        Route::group(["prefix" => "user-perusahaan/", "as" => "user-perusahaan."], function ()
        {
            Route::get("perusahaan", [PerusahaanController::class, "index"])->name("perusahaan.index");
            Route::post("perusahaan", [PerusahaanController::class, "store"])->name("perusahaan.store");
            Route::post("perusahaan-upload/{perusahaanId}", [PerusahaanController::class, "upload"])->name("perusahaan.upload");
            Route::get("perusahaan-remove-file/{perusahaanId}/{field}", [PerusahaanController::class, "removeFile"])->name("perusahaan.remove-file");
        });
    // 
});